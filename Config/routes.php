<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('plugin'=>'acl_management','controller' => 'users', 'action' => 'companyPerformance'));

//dashboard
Router::connect('/company_performance', array('plugin'=>'acl_management','controller' => 'users', 'action' => 'companyPerformance'));
Router::connect('/personal_performance/*', array('plugin'=>'acl_management','controller' => 'users', 'action' => 'personalPerformance'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

//system settings
Router::connect('/settings/config', array('plugin'=>false,'controller'          => 'systemSettings', 'action'          => 'add'));
Router::connect('/settings/pipeline', array('plugin'=>false,'controller'          => 'systemSettings', 'action'          => 'pipelines'));
Router::connect('/settings/google_connect', array('plugin'=>false,'controller'          => 'system_settings', 'action'          => 'googleConnect'));
///3rd parties connection
Router::connect('/users/socialSign', array('plugin'=>'acl_management','controller' => 'users', 'action' => 'socialSign'));

//message route
Router::connect('/messages/inbox', array('plugin'=>false,'controller' => 'messages', 'action' => 'index'));
Router::connect('/view', array('plugin'=>false,'controller' => 'messages', 'action' => 'view'));

//Router::connect('/leads', array('controller'               => 'contacts', 'action'               => 'leads', 'index'));
Router::connect('/leads/opportunities', array('plugin'=>false,'controller' => 'contacts', 'action' => 'leads', 'opportunities'));
Router::connect('/leads/new', array('plugin'=>false,'controller'           => 'leads', 'action'           => 'add'));
Router::connect('/leads/edit', array('plugin'=>false,'controller'          => 'leads', 'action'          => 'edit'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */

Router::mapResources(array('deals','activities'));
Router::mapResources('activities');
Router::mapResources('dealStages');
Router::parseExtensions('json');
require CAKE.'Config'.DS.'routes.php';
