<?php 
set_time_limit(0);
App::uses('CakeEmail', 'Network/Email');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');


class GoogleSyncShell extends AppShell{
	public $uses = array('GoogleUser','Contact','User','Activity');

	public function test(){
			$users = $this->User->find('all',array());
	
        $email = new CakeEmail('smtp');

        $email->from('malikperang@gmail.com');

        $email->to('fariz@dcodedigital.org');

        $email->subject('Test Email from Cron');

        $email->send('Hello from Cron');
	}

	public function syncCalendar(){
		// $users = $this->User->find('all',array());
	
  //       $email = new CakeEmail('smtp');

  //       $email->from('malikperang@gmail.com');

  //       $email->to('fariz@dcodedigital.org');

  //       $email->subject('Test Email from Cron');

  //       $email->send('Hello from Cron');
	}

	public function syncContact(){
		$gUser = $this->GoogleUser->find('all');
		foreach ($gUser as $user) {
				// debug($user);
			// google api credentials
			$client_id = Configure::read('Google.clientId');
			$client_secret = Configure::read('Google.clientSecret');

			//create new google object
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			
			//get new contacts
		    if (!empty($user)) {
		    	// debug($client);
		    	// exit();
		    	// debug($client);
			// debug($client_secret);
		    	$client->setAccessToken($user['GoogleUser']['access_token']);
		    	$access_token = json_decode($user['GoogleUser']['access_token'])->access_token;
				$refreshToken = json_decode($user['GoogleUser']['access_token'])->refresh_token;
			
		    	if ($client->isAccessTokenExpired() == TRUE) {
		    		$client->refreshtoken($refreshToken);
				  	$newtoken= $client->getAccessToken();
				  	$this->GoogleUser->id = $user['GoogleUser']['id'];
				  	$data = array(
				  		'access_token'=>$newtoken,
				  		);
				  	$this->GoogleUser->save($data);
					// return json_encode('Failed');
					// echo 'failed';

		    	}else{
		    		// debug('connection ok');
		    		// exit;
		    		//add data
		    		debug($access_token);
		    		debug($refreshToken);
	    			$newContacts = $this->Contact->find('all',array(
	    				'conditions'=>array(
	    					'Contact.company_id'=>$user['User']['company_id'],
	    					'Contact.created_by'=>$user['User']['id'],
	    					),
	    				));
	    			foreach ($newContacts as $value) {
	    				
					  	$contactXML = '<atom:entry xmlns:atom="http://www.w3.org/2005/Atom"
								    xmlns:gd="http://schemas.google.com/g/2005">
								  	<atom:category scheme="http://schemas.google.com/g/2005#kind"
								    term="http://schemas.google.com/contact/2008#contact"/>
								  <gd:name>
								     <gd:fullName>'.$value['Contact']['name'].'</gd:fullName>
								  </gd:name>
								  <atom:content type="text">'.$value['Contact']['remarks'].'</atom:content>
								  <gd:email rel="http://schemas.google.com/g/2005#work"
								    primary="true"
								    address="'.$value['Contact']['email'].'" displayName="'.$value['Contact']['name'].'"/>
								  <gd:phoneNumber rel="http://schemas.google.com/g/2005#work"
								    primary="true">
								   '.$value['Contact']['phone'].'
								  </gd:phoneNumber>
								  <gd:structuredPostalAddress
								      rel="http://schemas.google.com/g/2005#work"
								      primary="true">
								    <gd:city>'.$value['Contact']['city'].'</gd:city>
								    <gd:street>'.$value['Contact']['street_address'].'</gd:street>
								    <gd:region>'.$value['Contact']['state'].'</gd:region>
								    <gd:postcode>'.$value['Contact']['postcode'].'</gd:postcode>
								    <gd:country>'.$value['Contact']['country'].'</gd:country>
								  </gd:structuredPostalAddress>
					  				<gContact:groupMembershipInfo deleted="false"
					    					href="'.htmlentities($user['GoogleUser']['google_contact_group_id']).'"/>
								</atom:entry>';

						$headers = array(
							'Host: www.google.com',
							'Gdata-version: 3.0',
							'Content-length: '.strlen($contactXML),
							'Content-type: application/atom+xml',
							'Authorization: OAuth '.$access_token
						);

						$contactQuery = 'https://www.google.com/m8/feeds/contacts/default/full/';

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $contactQuery );
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $contactXML);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
						curl_setopt($ch, CURLOPT_TIMEOUT, 10);
						curl_setopt($ch, CURLOPT_FAILONERROR, true);
						curl_exec($ch);
	    			}
	    			
		    		//update data
		    		$curl = curl_init('https://www.google.com/m8/feeds/contacts/default/full?group='.htmlentities($user['GoogleUser']['google_contact_group_id']).'&max-results=10000000&start-index=1&alt=json&v=3&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
					// curl_setopt($curl, CURLOPT_TIMEOUT, 4000);
					// debug($curl);
					// exit;
					$contacts_json = curl_exec($curl);

					curl_close($curl);
			
					$contacts = json_decode($contacts_json,true);
					debug($curl);
					//if isset contact in feed
					if (isset($contacts['feed']['entry'])) {

						//save all new google contacts to database first
						foreach ($contacts['feed']['entry'] as $contact){
							$etag = '';
							$id = '';
							$name = '';
							$email = '';
							$phone = '';

							if (isset($contact['id']['$t'])) {
								$id = substr($contact['id']['$t'], strpos($contact['id']['$t'], "/base/") + 6);
							}

							if (isset($contact['gd$etag'])) {
								$etag = $contact['gd$etag'];
							}
							// return json_encode($etag);
							// exit();
							if (isset($contact['title']['$t'])) {
								$name = $contact['title']['$t'];
							}

							if (isset($contact['gd$email'][0]['address'])) {
								$email = $contact['gd$email'][0]['address'];
							}

							if (isset($contact['gd$phoneNumber'][0]['$t'])) {
								$phone = preg_replace('/\s+/', '', $contact['gd$phoneNumber'][0]['$t']);
							}
							
							$checkContact = $this->Contact->find('first',array(
								'conditions'=>array(
									'Contact.name'=>$name,
									'Contact.email'=>$email,
									'Contact.phone'=>$phone,
									)
								));


							//if contact not exist in database
							//create new one
							if (empty($checkContact)) {
									$this->Contact->create();
									$data = array(
										'company_id'=>$user['User']['company_id'],
										'google_contact_id'=>$id,
										'google_contact_etag'=> $etag,
										'name'=>$name,
										'email'=>$email,
										'phone'=>$phone,
										'source'=>'Google',
										'created_by'=>$user['User']['id']
										);
									if($this->Contact->save($data)){
										echo 'contact saved';
									}else{
										echo 'contact not saved';
									};
								
							}else{
								//just updated
								$this->Contact->id = $checkContact['Contact']['id'];
								$data = array(
									'company_id'=>$user['User']['company_id'],
									'google_contact_id'=>$id,
									'google_contact_etag'=> $etag,
									'name'=>$name,
									'email'=>$email,
									'phone'=>$phone,
									'source'=>'Google',
									'created_by'=>$user['User']['id']
									);
									if($this->Contact->save($data)){
										echo 'contact saved';
									}else{
										echo 'contact not saved';
									};
							}
			    		}
					}
						
		    		// return json_encode('Success');
		    		// echo 'success';
		    	}	
			}else{
				// return json_encode('not connected');
				// echo 'not connected';
			}
		}

		//google api credentials
		// $client_id = Configure::read('Google.clientId');
		// $client_secret = Configure::read('Google.clientSecret');

		// //create new google object
		// $client = new Google_Client();
		// $client->setClientId($client_id);
		// $client->setClientSecret($client_secret);

		// //get google user id
		// $googleUser = $this->GoogleUser->find('first',array(
		// 			'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		// ));
		
		// //get new contacts
	 //    if (!empty($googleUser)) {
	    	
	 //    	$client->setAccessToken($googleUser['GoogleUser']['access_token']);
	 //    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
		// 	$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
		
	 //    	if ($client->isAccessTokenExpired() == TRUE) {
	 //    		$client->refreshtoken($refreshToken);
		// 	  	$newtoken= $client->getAccessToken();
		// 	  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
		// 	  	$data = array(
		// 	  		'access_token'=>$newtoken,
		// 	  		);
		// 	  	$this->GoogleUser->save($data);

		//   		$googleUser = $this->GoogleUser->find('first',array(
		// 			'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		// 		));
				
		// 		// return json_encode('Failed');

	 //    	}else{
	    		
	 //    		//add data
  //   			$newContacts = $this->Contact->find('all',array(
  //   				'conditions'=>array(
  //   					'Contact.company_id'=>$userDetails['company_id'],
  //   					'Contact.created_by'=>$userDetails['id'],
  //   					),
  //   				));
  //   			foreach ($newContacts as $value) {
    				
		// 		  	$contactXML = '<atom:entry xmlns:atom="http://www.w3.org/2005/Atom"
		// 					    xmlns:gd="http://schemas.google.com/g/2005">
		// 					  	<atom:category scheme="http://schemas.google.com/g/2005#kind"
		// 					    term="http://schemas.google.com/contact/2008#contact"/>
		// 					  <gd:name>
		// 					     <gd:fullName>'.$value['Contact']['name'].'</gd:fullName>
		// 					  </gd:name>
		// 					  <atom:content type="text">'.$value['Contact']['remarks'].'</atom:content>
		// 					  <gd:email rel="http://schemas.google.com/g/2005#work"
		// 					    primary="true"
		// 					    address="'.$value['Contact']['email'].'" displayName="'.$value['Contact']['name'].'"/>
		// 					  <gd:phoneNumber rel="http://schemas.google.com/g/2005#work"
		// 					    primary="true">
		// 					   '.$value['Contact']['phone'].'
		// 					  </gd:phoneNumber>
		// 					  <gd:structuredPostalAddress
		// 					      rel="http://schemas.google.com/g/2005#work"
		// 					      primary="true">
		// 					    <gd:city>'.$value['Contact']['city'].'</gd:city>
		// 					    <gd:street>'.$value['Contact']['street_address'].'</gd:street>
		// 					    <gd:region>'.$value['Contact']['state'].'</gd:region>
		// 					    <gd:postcode>'.$value['Contact']['postcode'].'</gd:postcode>
		// 					    <gd:country>'.$value['Contact']['country'].'</gd:country>
		// 					  </gd:structuredPostalAddress>
		// 		  				<gContact:groupMembershipInfo deleted="false"
		// 		    					href="'.htmlentities($googleUser['GoogleUser']['google_contact_group_id']).'"/>
		// 					</atom:entry>';

		// 			$headers = array(
		// 				'Host: www.google.com',
		// 				'Gdata-version: 3.0',
		// 				'Content-length: '.strlen($contactXML),
		// 				'Content-type: application/atom+xml',
		// 				'Authorization: OAuth '.$access_token
		// 			);

		// 			$contactQuery = 'https://www.google.com/m8/feeds/contacts/default/full/';

		// 			$ch = curl_init();
		// 			curl_setopt($ch, CURLOPT_URL, $contactQuery );
		// 			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// 			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 			curl_setopt($ch, CURLOPT_POSTFIELDS, $contactXML);
		// 			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		// 			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		// 			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		// 			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		// 			curl_setopt($ch, CURLOPT_FAILONERROR, true);
		// 			curl_exec($ch);
  //   			}
    			
	 //    		//update data
	 //    		$curl = curl_init('https://www.google.com/m8/feeds/contacts/default/full?group='.htmlentities($googleUser['GoogleUser']['google_contact_group_id']).'&max-results=10000000&start-index=1&alt=json&v=3&access_token='.$access_token);
		// 		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// 		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		// 		curl_setopt($curl, CURLOPT_TIMEOUT, 10);

		// 		$contacts_json = curl_exec($curl);

		// 		curl_close($curl);
		
		// 		$contacts = json_decode($contacts_json,true);
				
		// 		//if isset contact in feed
		// 		if (isset($contacts['feed']['entry'])) {
		// 			//save all new google contacts to database first
		// 			foreach ($contacts['feed']['entry'] as $contact){
		// 				$etag = '';
		// 				$id = '';
		// 				$name = '';
		// 				$email = '';
		// 				$phone = '';

		// 				if (isset($contact['id']['$t'])) {
		// 					$id = substr($contact['id']['$t'], strpos($contact['id']['$t'], "/base/") + 6);
		// 				}

		// 				if (isset($contact['gd$etag'])) {
		// 					$etag = $contact['gd$etag'];
		// 				}
		// 				// return json_encode($etag);
		// 				// exit();
		// 				if (isset($contact['title']['$t'])) {
		// 					$name = $contact['title']['$t'];
		// 				}

		// 				if (isset($contact['gd$email'][0]['address'])) {
		// 					$email = $contact['gd$email'][0]['address'];
		// 				}

		// 				if (isset($contact['gd$phoneNumber'][0]['$t'])) {
		// 					$phone = preg_replace('/\s+/', '', $contact['gd$phoneNumber'][0]['$t']);
		// 				}
						
		// 				$checkContact = $this->Contact->find('first',array(
		// 					'conditions'=>array(
		// 						'Contact.name'=>$name,
		// 						'Contact.email'=>$email,
		// 						'Contact.phone'=>$phone,
		// 						)
		// 					));


		// 				//if contact not exist in database
		// 				//create new one
		// 				if (empty($checkContact)) {
		// 						$this->Contact->create();
		// 						$data = array(
		// 							'company_id'=>$userDetails['company_id'],
		// 							'google_contact_id'=>$id,
		// 							'google_contact_etag'=> $etag,
		// 							'name'=>$name,
		// 							'email'=>$email,
		// 							'phone'=>$phone,
		// 							'source'=>'Google',
		// 							'created_by'=>$userDetails['id']
		// 							);
		// 						$this->Contact->save($data);
							
		// 				}else{
		// 					//just updated
		// 					$this->Contact->id = $checkContact['Contact']['id'];
		// 					$data = array(
		// 						'company_id'=>$userDetails['company_id'],
		// 						'google_contact_id'=>$id,
		// 						'google_contact_etag'=> $etag,
		// 						'name'=>$name,
		// 						'email'=>$email,
		// 						'phone'=>$phone,
		// 						'source'=>'Google',
		// 						'created_by'=>$userDetails['id']
		// 						);
		// 					$this->Contact->save($data);
		// 				}
		//     		}
		// 		}
					
	 //    		// return json_encode('Success');
	 //    	}	
		// }else{
		// 	// return json_encode('not connected');
		// }
	}
}