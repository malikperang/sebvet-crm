<?php 

class SubscriptionShell extends AppShell{
	public $uses = array('Subscription','User');

	public function checkSubStatus(){

		//Get latest subscription detail
		$findSubDetail = $this->Subscription->find('all',array(
			'conditions'=>array(''),
			'order'=>array('Subscription.created'=>'DESC'),
			'limit' => 1,
			));

		//set this month name.
		$thisMonth = date('M',time());
		
		//
		foreach ($findSubDetail as $sub) :
			//debug($sub);

			if($sub['Subscription']['month'] != $thisMonth):
			//if subdetail for month not equal to this month means they
			//did not pay for this month. set the status to false.
				$this->User->id = $sub['User']['id'];
				$this->User->saveField('status',false);
				//$this->User->saveField('status',4);
				//debug to log file
				
				else:
					//else
					//update for double check
						$this->User->id = $sub['User']['id'];
						$this->User->saveField('status',true);
			endif;
		endforeach;

	}

}
?>