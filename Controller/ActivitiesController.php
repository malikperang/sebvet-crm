<?php
App::uses('AppController', 'Controller');

//require google api PHP Client to integrate Google
//reference link : https://github.com/google/google-api-php-client
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');

/**
 * Activities Controller
 *
 * @property Activity $Activity
 * @property PaginatorComponent $Paginator
 */
class ActivitiesController extends AppController {

	/**
	 * Used model
	 */

	public $uses = array('Activity','User','Deal','GoogleUser','Attachment');

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator','RequestHandler');

	/**
	 * index method
	 * This function will display all activities
	 * @return void
	 */
	public function index() {
		$userDetails = $this->Session->read('Auth.User');
		// $this->Activity->recursive = 0;
		// $this->set('activities', $this->Paginator->paginate());
		if ($userDetails['group_id'] == 1) {
			$this->Paginator->settings = array(
					'conditions'=>array('Activity.company_id'=>$userDetails['company_id']),
					'order'=>array('Activity.created'=>'DESC'),
					'limit'=>50
				);
			$activities = $this->Paginator->paginate('Activity');
			$deals = $this->Deal->find('list',array(
				'conditions'=>array('Deal.company_id'=>$userDetails['company_id'])
				));
		}else{
			$this->Paginator->settings = array(
				'conditions'=>array(
					'Activity.created_by'=>$userDetails['id'],
					'Activity.company_id'=>$userDetails['company_id']),
				'order'=>array('Activity.created'=>'DESC'),
				'limit'=>50
				);

			$activities = $this->Paginator->paginate('Activity');
			$deals = $this->Deal->find('list',array(
				'conditions'=>array(
					'Deal.company_id'=>$userDetails['company_id'],
					'Deal.created_by'=>$userDetails['id']
					)
				));
		}

		$this->set(compact('activities','deals'));
	}

	/**
	 * view Activity method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->Activity->exists($id)) {
			throw new NotFoundException(__('Invalid activity'));
		}
		$options = array('conditions' => array('Activity.'.$this->Activity->primaryKey => $id));
		$this->set('activity', $this->Activity->find('first', $options));
	}

	/**
	 * add Activity method
	 *
	 * @return void
	 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');
		if ($this->request->is('post')) {
			$this->request->data['Activity']['company_id'] = $userDetails['company_id'];
		
			//google api credentials
			$client_id = Configure::read('Google.clientId');
			$client_secret = Configure::read('Google.clientSecret');

			//create new google object
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);


			$googleUser = $this->GoogleUser->find('first',array(
		  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
			));

		  	if (!empty($googleUser) && !empty($this->request->data['Activity']['start_date'])) {
		    	
	    		$client->setAccessToken($googleUser['GoogleUser']['access_token']);
		    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
				$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
			
		    	if ($client->isAccessTokenExpired() == TRUE) {
		    		$client->refreshtoken($refreshToken);
				  	$newtoken= $client->getAccessToken();
				  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
				  	$data = array(
				  		'access_token'=>$newtoken,
				  		);
				  	$this->GoogleUser->save($data);
					
		    	}else{
		    		$service = new Google_Service_Calendar($client); 
					$event = new Google_Service_Calendar_Event();
			
					$event->setSummary($this->request->data['Activity']['notes']);
					$event->setLocation($this->request->data['Activity']['location']);

					$start = new Google_Service_Calendar_EventDateTime();
					$start->setTimeZone('Asia/Kuala_Lumpur');
					$start->setDateTime($this->request->data['Activity']['start_date'].'T'.$this->request->data['Activity']['event_time']);
					
					$event->setStart($start);

					$end = new Google_Service_Calendar_EventDateTime();
					$end->setTimeZone('Asia/Kuala_Lumpur');
					$end->setDateTime($this->request->data['Activity']['due'].'T'.$this->request->data['Activity']['event_time']);
					$event->setEnd($end);
		
					$createdEvent = $service->events->insert($googleUser['GoogleUser']['google_calendar_id'], $event);



				    $this->Activity->create();
				    //check if user uploading file
					if (!empty($this->request->data['Activity']['attachment']['name'])) {
						$file                                               = $this->addAttachment($this->request->data['Activity']['attachment']);
						//get file link
						$fileLink      = $file['link'];
						//get file type
						$fileType = $file['fileType'];
						//filename
						$fileName = $file['name'];
						//size
						$fileSize = $file['size'];
					}else{
						$this->request->data['Activity']['attachment'] = NULL;
					}
				
					$this->Activity->create();


					$this->request->data['Activity']['google_event_id']	= $createdEvent->getId();
					//check if new activity has been saved
					if ($this->Activity->save($this->request->data)) {
						$this->Attachment->create();
						$activityID = $this->Activity->getLastInsertId();
						if (!empty($this->request->data['Activity']['attachment']['name'])){
							$data = array(
								'activity_id'=>$activityID,
								'name'=>$fileName,
								'type'=>$fileType,
								'size'=>$fileSize,
								'url'=>$fileLink,
								'created_by'=>$userDetails['id']
							);
							$this->Attachment->save($data);
						}
					

			
						return json_encode('success');
					} else {
						return json_encode('could not saved');
					}
					

					return json_encode('success calendar');
		    	}
		    }

		    $this->Activity->create();
		    //check if user uploading file
			if (!empty($this->request->data['Activity']['attachment']['name'])) {
				// debug($this->request->data['Activity']['attachment']);
			
				$file                                               = $this->addAttachment($this->request->data['Activity']['attachment']);
				//get file link
				$fileLink      = $file['link'];
				//get file type
				$fileType = $file['fileType'];
				//filename
				$fileName = $file['name'];
				//size
				$fileSize = $file['size'];
			}else{
				$this->request->data['Activity']['attachment'] = NULL;
			}
		
			$this->Activity->create();

			//check if new activity has been saved
			if ($this->Activity->save($this->request->data)) {
				$this->Attachment->create();
				$activityID = $this->Activity->getLastInsertId();
				if (!empty($this->request->data['Activity']['attachment']['name'])){
					$data = array(
						'activity_id'=>$activityID,
						'name'=>$fileName,
						'type'=>$fileType,
						'size'=>$fileSize,
						'url'=>$fileLink,
						'created_by'=>$userDetails['id']
					);
					$this->Attachment->save($data);
				}
			
				return json_encode('success');
			} else {
				return json_encode('could not saved');
			}
			

		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->Activity->exists($id)) {
			throw new NotFoundException(__('Invalid activity'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Activity->save($this->request->data)) {
				$this->Session->setFlash(__('The activity has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The activity could not be saved. Please, try again.'));
			}
		} else {
			$options             = array('conditions' => array('Activity.'.$this->Activity->primaryKey => $id));
			$this->request->data = $this->Activity->find('first', $options);
		}
	}

	/**
	 * delete Activity method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {

		//delete google event first


		$this->Activity->id = $id;
		if (!$this->Activity->exists()) {
			throw new NotFoundException(__('Invalid activity'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Activity->delete()) {
			// $this->Session->setFlash(__('The activity has been deleted.'));
		} else {
			$this->Session->setFlash(__('The activity could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function deleteMany(){
		if ($this->request->is('post')) {
			if (!isset($this->request->data['Activity']['id'])) {
				$this->Session->setFlash('Something when wrong.Please make sure you have selected at least one activity to be deleted.', 'alert/error');
				return $this->redirect(array('action'=>'index'));
			}

			if (!empty($this->request->data)) {
				foreach ($this->request->data['Activity']['id'] as $activity => $value) {
					$this->Activity->delete($value);
				}
				$this->redirect(array('action'=>'index'));
			}
		}
	}

	/**
	 * addAttachment method
	 * This function will upload file,
	 * The location for the uploaded file is under
	 * folder /webroot/attachments/DATE/file.jpg
	 */

	public function addAttachment($file) {
		
		$current_date = date('Y-m-d');

		//type of files that are allowed
		$fileTypes = array(
			'image/gif', 
			'image/jpeg',
			'image/png', 
			'application/pdf', 
			'application/msword', 
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.ms-excel',
			'application/vnd.ms-powerpoint'
			);

		$uploadFolder = 'attachments'.DS.$current_date.DS;

		//check availability of a folder
		//return create new folder with current date if not exist

		if (file_exists($uploadFolder)) {
			$uploadFolder = $uploadFolder;
		} else {
			$old = umask(0);
			//give read permission
			mkdir(WWW_ROOT.'attachments'.DS.$current_date.DS, 0755, true);
			umask($old);
			// Checking
			if ($old != umask()) {
				die('An error occurred while changing back the umask');
			}
		}

		$uploadPath = WWW_ROOT.$uploadFolder;

		if (!empty($file)) {
			$ext = substr(strtolower(strrchr($file['name'], '.')), 1);//get the extension

			//check file type
			//if file type is not lists in $fileTypes array,flash an error
			if (in_array($file['type'], $fileTypes)) {
				$date     = date_create();
				$rand     = rand(100000, 999999);
				$fileName = 'attachments'.date_format($date, 'U').'_'.$rand.'.'.$ext;
				move_uploaded_file($file['tmp_name'], $uploadPath.$fileName);

				$fileLink = $current_date.DS.$fileName;
				return array('fileType' => $file['type'], 'link' => $fileLink,'name'=>$file['name'],'size'=>$file['size']);

			} else {
				$this->Session->setFlash(__('Please make sure the file you tried to upload are in correct format. For more info about the allowed file type,please click here.'), 'alert/error');
				return false;
			}
		}
	}

	public function calendar(){
		$userDetails = $this->Session->read('Auth.User');
		$googleUser = $this->GoogleUser->find('first',array(
	  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		));

		$this->set(compact('googleUser'));
	}

	public function sync(){
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');

		// $this->autoRender = false;
		$userDetails = $this->Session->read('Auth.User');
		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');

		//create new google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);


		$googleUser = $this->GoogleUser->find('first',array(
	  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		));

	  	if (!empty($googleUser)) {

    		$client->setAccessToken($googleUser['GoogleUser']['access_token']);
	    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
			$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;

	  		if (!empty($googleUser['GoogleUser']['google_calendar_id'])) {
	  			if ($client->isAccessTokenExpired() == TRUE) {
		    		$client->refreshtoken($refreshToken);
				  	$newtoken= $client->getAccessToken();
				  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
				  	$data = array(
				  		'access_token'=>$newtoken,
				  		);
				  	$this->GoogleUser->save($data);
				
		    	}else{
		    		
		    		$service = new Google_Service_Calendar($client); 
			
					$events = $service->events->listEvents($googleUser['GoogleUser']['google_calendar_id']);

					//get activities on database first
				    $activities = $this->Activity->find('all',array(
				    	'conditions'=>array('Activity.created_by'=>$userDetails['id'])

				    	));
				      

						 // foreach ($events->getItems() as $event) {
						 //    return json_encode($event);
						 //  }


				    //add & update to google calendar
				    foreach ($activities as $activity) {
				    	if ($activity['Activity']['google_event_id'] == NULL && !empty($activity['Activity']['start_date'])) {
				    	
				 
				    		$event = new Google_Service_Calendar_Event();
		
							$event->setSummary($activity['Activity']['notes']);
							$event->setLocation($activity['Activity']['location']);
							$start = new Google_Service_Calendar_EventDateTime();
							$start->setTimeZone('Asia/Kuala_Lumpur');
							$start->setDateTime($activity['Activity']['start_date'].'T'.$activity['Activity']['event_time']);
							
							$event->setStart($start);

							$end = new Google_Service_Calendar_EventDateTime();
							$end->setTimeZone('Asia/Kuala_Lumpur');
							$end->setDateTime($activity['Activity']['due'].'T'.$activity['Activity']['event_time']);
							$event->setEnd($end);
				
							$createdEvent = $service->events->insert($googleUser['GoogleUser']['google_calendar_id'], $event);

							$this->Activity->id = $activity['Activity']['id'];
							$data = array(
								'notes'=>$createdEvent->getSummary(),
								// 'start_date'=>
								'google_event_id'=> $createdEvent->getId()
								);
							$this->Activity->save($data);
					
				    	}


				    }
				   
			
					foreach ($events->getItems() as $event) {
						$optParams = array('showDeleted' => true);
						$events = $service->events->listEvents($googleUser['GoogleUser']['google_calendar_id'], $optParams);
						// debug($events['modelData']['items']);

						//delete event from database if not exist on google calendar
						foreach ($events['modelData']['items'] as $model) {
							if ($model['status'] == 'cancelled') {
								$activityToDeleted = $this->Activity->find('first',array(
																'conditions'=>array('Activity.google_event_id'=>$model['id']),
														));
								if (!empty($activityToDeleted)) {
									$this->Activity->id = $activityToDeleted['Activity']['id'];
									$this->Activity->delete();
								}
						
							}
						}

						//check if activity exist
						$checkExistActivity = $this->Activity->find('all',array(
							'conditions'=>array('Activity.google_event_id'=>$event->getId())
							));
						// return json_encode($event->getId());
						// exit();

						//add new activity(google calendar event) to database
						if (empty($checkExistActivity)) {
							$start_date = (new DateTime($event->getStart()->getDateTime()))->format('Y-m-d');
							$due = (new DateTime($event->getEnd()->getDateTime()))->format('Y-m-d');
							$time = (new DateTime($event->getEnd()->getDateTime()))->format('H:i:s');
							// debug($)
							$data = array(
								'google_event_id'=>$event->getId(),
								'company_id'=>$userDetails['company_id'],
								'location'=>$event->getLocation(),
								'notes'=>$event->getSummary(),
								'start_date'=>$start_date,
								'due'=>$due,
								'time' => $time,
								'created_by'=>$userDetails['id'],
							);
							$this->Activity->create();
							$this->Activity->save($data);		
						}
					}
					return json_encode('sync success');
			   	}
	  		}else{
	  			return json_encode('google calendarID not found');
	  		}

	    	
		}else{
			return json_encode('google account not connect');
		}

	}


	public function liveFeed(){
		$userDetails = $this->Session->read('Auth.User');
		$activities = $this->Activity->find('all',array(
			'conditions'=>array('Activity.company_id'=>$userDetails['company_id']),
			'limit'=>5
			));
		$this->set(
			array(
				'activities'  => $activities,
				'_serialize' => 'activities')
		);
	}

}
