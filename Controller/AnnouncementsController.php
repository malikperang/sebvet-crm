<?php
App::uses('AppController', 'Controller');
/**
 * Announcements Controller
 *
 * @property Announcement $Announcement
 * @property PaginatorComponent $Paginator
 */
class AnnouncementsController extends AppController {

	public $uses = array('Announcement','Attachment');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Announcement->recursive = 0;
		// $this->set('announcements', $this->Paginator->paginate());
		$this->Paginator->settings = array(
			'order'=>array('Announcement.created'=>'DESC')
			);
		$announcements = $this->Paginator->paginate('Announcement');
		$this->set(compact('announcements'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Announcement->exists($id)) {
			throw new NotFoundException(__('Invalid announcement'));
		}
		$options = array('conditions' => array('Announcement.' . $this->Announcement->primaryKey => $id));
		$this->set('announcement', $this->Announcement->find('first', $options));

		$attachments = $this->Attachment->findAllByAnnouncementId($id);

		$this->set(compact('attachments'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			$this->request->data['Announcement']['created_by'] = $userDetails['id'];
			$this->Announcement->create();
			if ($this->Announcement->save($this->request->data)) {
				$announcementId = $this->Announcement->getLastInsertId();
				if(!empty($this->request->data['Announcement']['file'][0]['name'])){
					foreach ($this->request->data['Announcement']['file'] as $value) {
							$file                 = $this->addAttachment($value);
							//get file link
							$fileLink      = $file['link'];
							//get file type
							$fileType = $file['fileType'];
							//filename
							$fileName = $file['name'];
							//size
							$fileSize = $file['size'];

							$data = array(
								'created_by'=>$userDetails['id'],
								'announcement_id'=>$announcementId,
								'name'=>$fileName,
								'type'=>$fileType,
								'url'=>$fileLink,
								'size'=>$fileSize,
							);
							$this->Attachment->create();
							$this->Attachment->save($data);
						
					}
				}
				$this->redirect(array('action'=>'index'));
			}else{
				$this->Session->setFlash(__('The announcement could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$userDetails = $this->Session->read('Auth.User');

		if (!$this->Announcement->exists($id)) {
			throw new NotFoundException(__('Invalid announcement'));
		}
		if ($this->request->is(array('post', 'put'))) {
			debug($this->request->data);
			$this->request->data['Announcement']['file'] = array_filter($this->request->data['Announcement']['file']);
			
			$this->Announcement->id = $id;
			$this->request->data['Announcement']['modified_by'] = $userDetails['id'];
			if ($this->Announcement->save($this->request->data)) {
				$ident = $this->Announcement->getLastInsertId();

				if(!empty($this->request->data['Announcement']['file'][0]['name'])){
					foreach ($this->request->data['Announcement']['file'] as $key => $value) {
						
						$file = $this->addAttachment($value);
						// debug($file);
						// exit();
						if($file !== null){
							$data = array(
							'announcement_id'=>$ident,
							'name'=>$file['name'],
							'url'=>$file['link'],
							'type'=>$file['fileType'],
							'modified_by'=>$userDetails['id']
							);
							//save all attachments
							$this->Attachment->save($data);
							$this->Session->setFlash(__('The announcement has been updated.'),'alert/success');
							return $this->redirect(array('action' => 'index'));
						}else{
							$this->Session->setFlash(__('The attachments could not been uploaded.'),'alert/error');
						}
					
					}
				}else{
					$this->Session->setFlash(__('The announcement has been saved.'),'alert/success');
					return $this->redirect(array('action' => 'index'));
				}
				
				
			} else {
				$this->Session->setFlash(__('The announcement could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Announcement.' . $this->Announcement->primaryKey => $id));
			$this->request->data = $this->Announcement->find('first', $options);
		}
		$announcement = $this->Announcement->findById($id);
		$this->set(compact('announcement'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Announcement->id = $id;
		if (!$this->Announcement->exists()) {
			throw new NotFoundException(__('Invalid announcement'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Announcement->delete()) {
			$this->Session->setFlash(__('The announcement has been deleted.'));
		} else {
			$this->Session->setFlash(__('The announcement could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * addAttachment method
	 * This function will upload file,
	 * The location for the uploaded file is under
	 * folder /webroot/attachments/DATE/
	 */

	public function addAttachment($file) {

		/**
		 * TO JOMOS TEAM
		 * Google drive API integration should be done here
		 * When user uploading any file,
		 * upload the file immediately to their google account too
		 */

		
		$current_date = date('Y-m-d');

		//type of files that are allowed
		$fileTypes = array(
			'image/gif', 
			'image/jpeg',
			'image/png', 
			'application/pdf', 
			'application/msword', 
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.ms-excel',
			'application/vnd.ms-powerpoint'
			);

		$uploadFolder = 'attachments'.DS.$current_date.DS;

		//check availability of a folder
		//return create new folder with current date if not exist

		if (file_exists($uploadFolder)) {
			$uploadFolder = $uploadFolder;
		} else {
			$old = umask(0);
			//create & give read permission to the folder
			mkdir(WWW_ROOT.'attachments'.DS.$current_date.DS, 0755, true);
			umask($old);
			// Checking
			if ($old != umask()) {
				die('An error occurred while changing back the umask');
			}
		}

		$uploadPath = WWW_ROOT.$uploadFolder;

		if (!empty($file)) {
			$ext = substr(strtolower(strrchr($file['name'], '.')), 1);//get the extension

			//check file type
			//if file type is not lists in $fileTypes array,flash an error
			if (in_array($file['type'], $fileTypes)) {
				$date     = date_create();
				$rand     = rand(100000, 999999);
				$fileName = 'attachments_'.date_format($date, 'U').'_'.$rand.'.'.$ext;
				move_uploaded_file($file['tmp_name'], $uploadPath.$fileName);

				$fileLink = $current_date.DS.$fileName;
				return array('fileType' => $file['type'], 'link' => $fileLink,'name'=>$file['name'],'size'=>$file['size']);

			} else {
				return $this->Session->setFlash(__('Please make sure the file you tried to upload are in correct format.'), 'alert/error');
				
			}
		}
	}
}
