<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $theme  = 'SebvetCRM';
	public $layout = 'default';
	public $helpers =  array('Ordinal');

	
	public $components = array(
		'Acl',
		'Auth'          => array(
			'authenticate' => array(
				'Form'        => array(
					'fields'     => array('username'     => 'email'),
					'scope'      => array('User.status'      => 1)
				)
			),
			'authorize' => array(
				'Actions'  => array('actionPath'  => 'controllers')
			)
		),
		'Session',
		'DebugKit.Toolbar'
	);

	public function beforeFilter() {
		parent::beforeFilter();
		// $tinyMCEconfigs = array(
		//     'simple' => array(
		//         'mode' => 'textareas',
		//         'theme' => 'simple',
		//         'editor_selector' => 'mceSimple'
  //   	));


		// $this->TinyMCE->configs = $tinyMCEconfigs;

		// $this->Auth->allow();//must comment after generate action

		//Configure AuthComponent
		$this->Auth->autoRedirect = true; 
		$this->Auth->loginAction    = '/users/login';
		$this->Auth->logoutRedirect = '/users/login';

		//after login redirect to index
		$this->Auth->loginRedirect = array('plugin' => 'acl_management',
			'controller'                               => 'users', 'action'=>'companyPerformance');
	}

	public function beforeRender() {
		parent::beforeRender();
		if($this->name == 'CakeError'){
		      $this->layout = 'error';
		}      
		//$userDetails variable will avaible at all pages
		$userDetails = $this->Session->read('Auth.User');
		//$sysSetting variable will avaible at all pages
		$sysSetting  = $this->getCurrentSetting();
		//get new message in inbox
		$this->loadModel('Message');
		$newMessage = $this->Message->countNewMessage($userDetails['id']);
		$this->set(compact('userDetails', 'sysSetting','newMessage'));

		//refresh google access token if user connected the google account
		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');

		//create google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);

		$this->loadModel('GoogleUser');
		$googleUser = $this->GoogleUser->find('first',array(
					'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'])
					));
		// debug($google_user);
	 	if (!empty($google_user)) {
			$client->setAccessToken($googleUser['GoogleUser']['access_token']);
	    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
			$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
		
	    	if ($client->isAccessTokenExpired() == TRUE) {
	    		$client->refreshtoken($refreshToken);
			  	$newtoken= $client->getAccessToken();
			  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
			  	$data = array(
			  		'access_token'=>$newtoken,
			  		);
			  	$this->GoogleUser->save($data);
				
	    	}
			
	    }



	}

	/**
	 * Get current system setting
	 * 
	 */
	public function getCurrentSetting() {
		$this->loadModel('SystemSetting');
		$result = $this->SystemSetting->find('first', array(
				'conditions' => array(''),
				'order'      => array('SystemSetting.created'      => 'DESC'),
				// 'limit'      => 1

			));
		return $result;
	}

	// /**
	//  * Image Upload function
	//  * can be used on all controller
	//  * Only uploading image file.
	//  * @return string image link
	//  */
	// public function imageUpload($image) {
	
	// 	$current_date = date('Y-m-d');

	// 	//image upload
	// 	$imageTypes   = array('image/gif', 'image/jpeg', 'image/png');
	// 	$uploadFolder = 'img'.DS.'uploads'.DS.$current_date.DS;

	// 	//check availability of a folder
	// 	//return create new folder with current date if not exist

	// 	if (file_exists($uploadFolder)) {
	// 		$uploadFolder = $uploadFolder;
	// 	} else {
	// 		$old = umask(0);
	// 		//give read permission
	// 		mkdir('img'.DS.'uploads'.DS.$current_date.DS, 0755, true);
	// 		umask($old);
	// 		// Checking
	// 		if ($old != umask()) {
	// 			die('An error occurred while changing back the umask');
	// 		}
	// 	}

	// 	$uploadPath = WWW_ROOT.$uploadFolder;

	// 	if (!empty($image)) {
	// 		$ext = substr(strtolower(strrchr($image['name'], '.')), 1);//get the extension

	// 		//check file type
	// 		//if not an image type,flash an error
	// 		if (in_array($image['type'], $imageTypes)) {
	// 			$date      = date_create();
	// 			$rand      = rand(100000, 999999);
	// 			$file_name = 'pic_'.date_format($date, 'U').'_'.$rand.'.'.$ext;
	// 			move_uploaded_file($image['tmp_name'], $uploadPath.$file_name);

	// 			$imagelink = $current_date.DS.$file_name;
	// 			return $imagelink;

	// 		} else {
	// 			$this->Session->setFlash(__('Please make sure the image you tried to upload are in correct format : jpeg/gif/png'), 'alert/error');
	// 			return false;
	// 		}
	// 	}

	// }

	

}
