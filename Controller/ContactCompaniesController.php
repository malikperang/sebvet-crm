<?php
App::uses('AppController', 'Controller');
/**
 * ContactCompanies Controller
 *
 * @property ContactCompany $ContactCompany
 * @property PaginatorComponent $Paginator
 */
class ContactCompaniesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ContactCompany->recursive = 0;
		$this->set('contactCompanies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ContactCompany->exists($id)) {
			throw new NotFoundException(__('Invalid contact company'));
		}
		$options = array('conditions' => array('ContactCompany.' . $this->ContactCompany->primaryKey => $id));
		$this->set('contactCompany', $this->ContactCompany->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ContactCompany->create();
			if ($this->ContactCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The contact company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact company could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ContactCompany->exists($id)) {
			throw new NotFoundException(__('Invalid contact company'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ContactCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The contact company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact company could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ContactCompany.' . $this->ContactCompany->primaryKey => $id));
			$this->request->data = $this->ContactCompany->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ContactCompany->id = $id;
		if (!$this->ContactCompany->exists()) {
			throw new NotFoundException(__('Invalid contact company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ContactCompany->delete()) {
			$this->Session->setFlash(__('The contact company has been deleted.'));
		} else {
			$this->Session->setFlash(__('The contact company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
