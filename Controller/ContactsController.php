<?php
App::uses('AppController', 'Controller');
//require google api PHP Client to integrate Google
//reference link : https://github.com/google/google-api-php-client
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 */
class ContactsController extends AppController {

	//used model
	public $uses = array('Contact', 'Company','ContactCompany','GoogleUser','Deal');

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	/**
	 * index method
	 * This function will display all available Contacts
	 * @return void
	 */
	public function index() {
		$userDetails = $this->Session->read('Auth.User');

		if ($userDetails['group_id'] == 1) {
		     $this->Paginator->settings = array(
					        'conditions' => array('Contact.company_id' => $userDetails['company_id']),
					        'limit' => 50,
					        'cache'=>true,
					   );
			//output from database
			 $contacts = $this->Paginator->paginate('Contact');
			 $totalContact = $this->Contact->countAll();
			 $displayContacts = $this->Contact->find('list');
		}else{
		     $this->Paginator->settings = array(
					        'conditions' => array('Contact.company_id' => $userDetails['company_id'],'Contact.created_by'=>$userDetails['id']),
					        'limit' => 50,
					        'cache'=>true,
					   );
			//output from database
			$contacts = $this->Paginator->paginate('Contact');
			$totalContact = $this->Contact->countAllByUser($userDetails['id']);
			$displayContacts = $this->Contact->find('list',array(
				'conditions'=>array(
					'Contact.company_id'=>$userDetails['company_id'],
					'Contact.created_by'=>$userDetails['id']
					)
				));
		}
		
		
		$this->set(compact('contacts','displayContacts','totalContact'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null,$userID = null) {
		$userDetails = $this->Session->read('Auth.User');


		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
		//for security purpose
		if($userID == $userDetails['id']){
			$options = array('conditions' => array('Contact.'.$this->Contact->primaryKey => $id));
			$this->set('contact', $this->Contact->find('first', $options));
		}else{
			throw new NotFoundException(__('Invalid contact'));
		}

		$ongoing = $this->Deal->find('all',array(
			'conditions'=>array('Deal.created_by'=>$userDetails['id'],'Deal.contact_id'=>$id,'Deal.status'=>NULL),
			));
		$won = $this->Deal->find('all',array(
			'conditions'=>array('Deal.created_by'=>$userDetails['id'],'Deal.contact_id'=>$id,'Deal.status'=>'won'),
			));
		$lost = $this->Deal->find('all',array(
			'conditions'=>array('Deal.created_by'=>$userDetails['id'],'Deal.contact_id'=>$id,'Deal.status'=>'lost'),
			));
		$this->set(compact('ongoing','won','lost'));
	
	}

	/**
	 * add New Contact method
	 *
	 * @return void
	 */
	public function add() {
		//get logged on user details
		$userDetails = $this->Session->read('Auth.User');

		if ($this->request->is('post')) {
		
			// //if user uploading profile picture
			if (empty($this->request->data['Contact']['profile_picture']['name'])) {
				$this->request->data['Contact']['profile_picture'] = NULL;
			}else{
				$this->request->data['Contact']['profile_picture'] = $this->addProfilePicture($this->request->data['Contact']['profile_picture']);
			}

	
			//assign company id
			$this->request->data['Contact']['company_id'] = $userDetails['company_id']; 
			//created by,get logged on user id
			$this->request->data['Contact']['created_by'] = $userDetails['id'];

			$this->ContactCompany->create();
			$data = array(
				'name'=> $this->request->data['Contact']['company_name'],
				'created_by'=>$userDetails['id'],
				'modified_by'=>$userDetails['id'],
				);

			//save contact company first
			if ($this->ContactCompany->save($data)) {
				$this->Contact->create();
		
				$this->request->data['Contact']['contact_company_id'] = $this->ContactCompany->getLastInsertID();
				//then save company data
				if ($this->Contact->save($this->request->data)) {
					return $this->redirect(array('action'=>'index'));
				}else{
					$this->Session->setFlash(__('Something when wrong.Please try again'),'alert/error');
				}
			}
			

		}
	
		$companies = $this->Contact->Company->find('list');
		$this->set(compact( 'companies'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		
		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
		//get logged on user details
		$userDetails = $this->Session->read('Auth.User');

		if ($this->request->is(array('put','post'))) {
			$this->Contact->id = $id;
		
			//if user uploading profile picture
			if (empty($this->request->data['Contact']['profile_picture']['name'])) {
				$this->request->data['Contact']['profile_picture'] = NULL;
			}else{
				$this->request->data['Contact']['profile_picture'] = $this->addProfilePicture($this->request->data['Contact']['profile_picture']);
			}

			
			$this->request->data['Contact']['company_id'] = $userDetails['company_id']; 

			$this->request->data['Contact']['modified_by'] = $userDetails['id'];
			
			$data = array(
				'name'=> $this->request->data['Contact']['company_name'],
				'modified_by'=>$userDetails['id'],
			);

			$this->ContactCompany->create();
			//save contact company first
			if ($this->ContactCompany->save($data)) {
		
				$this->request->data['Contact']['contact_company_id'] = $this->ContactCompany->getLastInsertID();
				//then save company data
				if ($this->Contact->save($this->request->data)) {
					return $this->redirect(array('action'=>'index'));
				}else{
					$this->Session->setFlash(__('Something when wrong.Please try again'),'alert/error');
				}
			}
			
		}else{
			$options             = array('conditions' => array('Contact.'.$this->Contact->primaryKey => $id));
			$this->request->data = $this->Contact->find('first', $options);
		}

	
		$companies = $this->Contact->Company->find('list');
		$this->set(compact( 'companies'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$userDetails = $this->Session->read('Auth.User');
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		$this->request->allowMethod('post', 'delete');
	
		$this->autoRender = false;
		if ($this->Contact->delete()) {
			$this->Session->setFlash(__('The contact has been deleted.'),'alert/success');
		} else {
			$this->Session->setFlash(__('The contact could not be deleted. Please, try again.'),'alert/error');
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function deleteMany(){
		// $this->request->onlyAllow('ajax');
		if ($this->request->is('post')) {
		
			if (!isset($this->request->data['Contact']['id'])) {
				$this->Session->setFlash('Something when wrong.Please make sure you have selected at least one contact to be deleted.', 'alert/error');
				return $this->redirect(array('action'=>'index'));
			}

			if (!empty($this->request->data)) {
				foreach ($this->request->data['Contact']['id'] as $contact => $value) {
					$this->Contact->delete($value);
				}
				$this->redirect(array('action'=>'index'));
			}
			// 	if (!isset($this->request->data['Stock']['id'])) {
			// 	$this->Session->setFlash('<i class="cus-cross-octagon-fram"></i> <b>Error!</b> No stock selected. please select at least 1 or more stock transaction to be deleted.', 'alert/error');
			// 	$this->redirect($this->referer());
			// } elseif (!empty($this->request->data)) {
			// 	foreach ($this->request->data['Stock']['id'] as $key => $value) {
			// 		$this->Stock->delete($value);
			// 	}
			// 	$this->Session->setFlash(__(count($this->request->data['Stock']['id']).' '.'Stock has been deleted.'), 'alert/success');

			// 	$this->redirect($this->referer());
			// } else {
			// 	$this->Session->setFlash('Please make sure you have any data to delete!', 'alert/error');
			// 	$this->redirect($this->referer());
			// 	return false;
			// }
		}
	}

	
	public function sync(){
		$userDetails = $this->Session->read('Auth.User');
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');
		// return json_encode('Berjaya');

		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');

		//create new google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);

		//get google user id
		$googleUser = $this->GoogleUser->find('first',array(
					'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		));
		
		//get new contacts
	    if (!empty($googleUser)) {
	    	
	    	$client->setAccessToken($googleUser['GoogleUser']['access_token']);
	    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
			$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
		
	    	if ($client->isAccessTokenExpired() == TRUE) {
	    		$client->refreshtoken($refreshToken);
			  	$newtoken= $client->getAccessToken();
			  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
			  	$data = array(
			  		'access_token'=>$newtoken,
			  		);
			  	$this->GoogleUser->save($data);

		  		$googleUser = $this->GoogleUser->find('first',array(
					'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
				));
				
				return json_encode('Failed');

	    	}else{
	    		
	    		//add data
    			$newContacts = $this->Contact->find('all',array(
    				'conditions'=>array(
    					'Contact.company_id'=>$userDetails['company_id'],
    					'Contact.created_by'=>$userDetails['id'],
    					),
    				));
    			foreach ($newContacts as $value) {
    				
				  	$contactXML = '<atom:entry xmlns:atom="http://www.w3.org/2005/Atom"
							    xmlns:gd="http://schemas.google.com/g/2005">
							  	<atom:category scheme="http://schemas.google.com/g/2005#kind"
							    term="http://schemas.google.com/contact/2008#contact"/>
							  <gd:name>
							     <gd:fullName>'.$value['Contact']['name'].'</gd:fullName>
							  </gd:name>
							  <atom:content type="text">'.$value['Contact']['remarks'].'</atom:content>
							  <gd:email rel="http://schemas.google.com/g/2005#work"
							    primary="true"
							    address="'.$value['Contact']['email'].'" displayName="'.$value['Contact']['name'].'"/>
							  <gd:phoneNumber rel="http://schemas.google.com/g/2005#work"
							    primary="true">
							   '.$value['Contact']['phone'].'
							  </gd:phoneNumber>
							  <gd:structuredPostalAddress
							      rel="http://schemas.google.com/g/2005#work"
							      primary="true">
							    <gd:city>'.$value['Contact']['city'].'</gd:city>
							    <gd:street>'.$value['Contact']['street_address'].'</gd:street>
							    <gd:region>'.$value['Contact']['state'].'</gd:region>
							    <gd:postcode>'.$value['Contact']['postcode'].'</gd:postcode>
							    <gd:country>'.$value['Contact']['country'].'</gd:country>
							  </gd:structuredPostalAddress>
				  				<gContact:groupMembershipInfo deleted="false"
				    					href="'.htmlentities($googleUser['GoogleUser']['google_contact_group_id']).'"/>
							</atom:entry>';

					$headers = array(
						'Host: www.google.com',
						'Gdata-version: 3.0',
						'Content-length: '.strlen($contactXML),
						'Content-type: application/atom+xml',
						'Authorization: OAuth '.$access_token
					);

					$contactQuery = 'https://www.google.com/m8/feeds/contacts/default/full/';

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $contactQuery );
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $contactXML);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_FAILONERROR, true);
					curl_exec($ch);
    			}
    			
	    		//update data
	    		$curl = curl_init('https://www.google.com/m8/feeds/contacts/default/full?group='.htmlentities($googleUser['GoogleUser']['google_contact_group_id']).'&max-results=10000000&start-index=1&alt=json&v=3&access_token='.$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);

				$contacts_json = curl_exec($curl);

				curl_close($curl);
		
				$contacts = json_decode($contacts_json,true);
				
				//if isset contact in feed
				if (isset($contacts['feed']['entry'])) {
					//save all new google contacts to database first
					foreach ($contacts['feed']['entry'] as $contact){
						$etag = '';
						$id = '';
						$name = '';
						$email = '';
						$phone = '';

						if (isset($contact['id']['$t'])) {
							$id = substr($contact['id']['$t'], strpos($contact['id']['$t'], "/base/") + 6);
						}

						if (isset($contact['gd$etag'])) {
							$etag = $contact['gd$etag'];
						}
						// return json_encode($etag);
						// exit();
						if (isset($contact['title']['$t'])) {
							$name = $contact['title']['$t'];
						}

						if (isset($contact['gd$email'][0]['address'])) {
							$email = $contact['gd$email'][0]['address'];
						}

						if (isset($contact['gd$phoneNumber'][0]['$t'])) {
							$phone = preg_replace('/\s+/', '', $contact['gd$phoneNumber'][0]['$t']);
						}
						
						$checkContact = $this->Contact->find('first',array(
							'conditions'=>array(
								'Contact.name'=>$name,
								'Contact.email'=>$email,
								'Contact.phone'=>$phone,
								)
							));


						//if contact not exist in database
						//create new one
						if (empty($checkContact)) {
								$this->Contact->create();
								$data = array(
									'company_id'=>$userDetails['company_id'],
									'google_contact_id'=>$id,
									'google_contact_etag'=> $etag,
									'name'=>$name,
									'email'=>$email,
									'phone'=>$phone,
									'source'=>'Google',
									'created_by'=>$userDetails['id']
									);
								$this->Contact->save($data);
							
						}else{
							//just updated
							$this->Contact->id = $checkContact['Contact']['id'];
							$data = array(
								'company_id'=>$userDetails['company_id'],
								'google_contact_id'=>$id,
								'google_contact_etag'=> $etag,
								'name'=>$name,
								'email'=>$email,
								'phone'=>$phone,
								'source'=>'Google',
								'created_by'=>$userDetails['id']
								);
							$this->Contact->save($data);
						}
		    		}
				}
					
	    		return json_encode('Success');
	    	}	
		}else{
			return json_encode('not connected');
		}
	}

	public function search(){
		$userDetails = $this->Session->read('Auth.User');
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$this->redirect(array('action'=>'view',$this->request->data['Contact']['contact_id'],$userDetails['id']));
		}
	}

	/**
	 * Image Upload function
	 * can be used on all controller
	 * Only uploading image file.
	 * @return string image link
	 */
	public function addProfilePicture($image) {
	
		$current_date = date('Y-m-d');

		//image upload
		$imageTypes   = array('image/gif', 'image/jpeg', 'image/png');
		$uploadFolder = 'img'.DS.'uploads'.DS.$current_date.DS;

		//check availability of a folder
		//return create new folder with current date if not exist

		if (file_exists($uploadFolder)) {
			$uploadFolder = $uploadFolder;
		} else {
			$old = umask(0);
			//give read permission
			mkdir('img'.DS.'uploads'.DS.$current_date.DS, 0755, true);
			umask($old);
			// Checking
			if ($old != umask()) {
				die('An error occurred while changing back the umask');
			}
		}

		$uploadPath = WWW_ROOT.$uploadFolder;

		if (!empty($image)) {
			$ext = substr(strtolower(strrchr($image['name'], '.')), 1);//get the extension

			//check file type
			//if not an image type,flash an error
			if (in_array($image['type'], $imageTypes)) {
				$date      = date_create();
				$rand      = rand(100000, 999999);
				$file_name = 'pic_'.date_format($date, 'U').'_'.$rand.'.'.$ext;
				move_uploaded_file($image['tmp_name'], $uploadPath.$file_name);

				$imagelink = $current_date.DS.$file_name;
				return $imagelink;

			} else {
				$this->Session->setFlash(__('Please make sure the image you tried to upload are in correct format : jpeg/gif/png'), 'alert/error');
				return false;
			}
		}

	}
}
