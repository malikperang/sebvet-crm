<?php

App::uses('CakeEmail', 'Network/Email');


class CronController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout=null;
    }

    public function test() {
        // Check the action is being invoked by the cron dispatcher 
        if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); } 

        //no view
        $this->autoRender = false;

        //do stuff...

        $email = new CakeEmail('smtp');

        $email->from('malikperang@gmail.com');

        $email->to('fariz@dcodedigital.org');

        $email->subject('Test Email from Cron');

        $result = $email->send('Hello from Cron');


        return;
    }
}