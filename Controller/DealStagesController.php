<?php
App::uses('AppController', 'Controller');
/**
 * DealStages Controller
 *
 * @property DealStage $DealStage
 * @property PaginatorComponent $Paginator
 */
class DealStagesController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'RequestHandler');

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->DealStage->recursive = 0;
		$this->set('dealStages', $this->Paginator->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->DealStage->exists($id)) {
			throw new NotFoundException(__('Invalid deal stage'));
		}
		$options = array('conditions' => array('DealStage.'.$this->DealStage->primaryKey => $id));
		$this->set('dealStage', $this->DealStage->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');

		if ($this->request->is('post')) {
			//slug for jquery tab id
			$this->request->data['DealStage']['slug'] = str_replace(' ', '-', $this->request->data['DealStage']['name']);
			$this->request->data['DealStage']['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', $this->request->data['DealStage']['slug']);
			$this->DealStage->create();
			if ($this->DealStage->save($this->request->data)) {
				// $this->Session->setFlash(__('The deal stage has been saved.'),'alert/success');
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The deal stage could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit() {
		
		if ($this->request->is(array('put','post'))) {

			$id = $this->request->data['DealStage']['id'];
			$this->DealStage->id = $id;
			$this->request->data['DealStage']['slug'] = str_replace(' ', '-', $this->request->data['DealStage']['name']);
			$this->request->data['DealStage']['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', $this->request->data['DealStage']['slug']);
			if (!$this->DealStage->exists($id)) {
				throw new NotFoundException(__('Invalid deal stage'));
			}
			if ($this->DealStage->save($this->request->data)) {
				// $this->Session->setFlash(__('The deal stage has been saved.'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The deal stage could not be saved. Please, try again.'));
			}
		}

		
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete() {
		$userDetails = $this->Session->read('Auth.User');
		$this->request->allowMethod('post', 'delete');
		if ($this->request->is(array('post','delete'))) {
			$this->DealStage->id = $this->request->data['DealStage']['id'];
			if (!$this->DealStage->exists($this->DealStage->id)) {
				throw new NotFoundException(__('Invalid deal stage'));
			}

			
			if ($this->DealStage->delete()) {
				$this->Session->setFlash(__('The deal stage and its associated deals has been deleted.'),'alert/success');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The deal stage could not be deleted. Please, try again.'),'alert/error');
			}

		}
		
		return $this->redirect(array('action' => 'index'));
	}

	public function api() {
		$userDetails = $this->Session->read('Auth.User');
		$dealStage = $this->DealStage->find('list',array(
			'conditions'=>array('DealStage.company_id'=>$userDetails['company_id'])
			));

		$this->set(array(
				'dealStage'  => $dealStage,
				'_serialize' => 'dealStage'));

	}
}
