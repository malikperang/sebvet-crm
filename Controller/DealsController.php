<?php
App::uses('AppController', 'Controller');
/**
 * Deals Controller
 *
 * @property Deal $Deal
 * @property PaginatorComponent $Paginator
 */
class DealsController extends AppController {

	public $uses = array('Deal', 'Activity','User','DealStage','Contact','SalePerformance');

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'RequestHandler','Security');

	public function beforeFilter(){
		$this->Security->unlockedActions = array('changeStage','changeDealName','changeDealValue','won','lost');
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$userDetails = $this->Session->read('Auth.User');


		//if logged on user is admin/super user
		if ($userDetails['group_id'] == 1) {
			//filter dealstage by company id
			$dealStages = $this->Deal->DealStage->find('all',array(
				'conditions'=>array('DealStage.company_id'=>$userDetails['company_id']),
				));

			//get contact list by company ID
			$contacts   = $this->Contact->find('list',array(
				'conditions'=>array(
					'Contact.company_id'=>$userDetails['company_id'],
					)
				));

			//find stage related to company id
			//this will return dealstage list when adding new deal
			$dealStagesList = $this->Deal->DealStage->find('list',array(
				'conditions'=>array(
					'DealStage.company_id'=>$userDetails['company_id'],
					)
				));

			$deals = $this->Deal->find('list',array(
				'Deal.company_id'=>$userDetails['company_id']
				));
		}else{
			//filter dealstage by company id
			$dealStages = $this->Deal->DealStage->find('all',array(
				'conditions'=>array('DealStage.company_id'=>$userDetails['company_id']),
				));

			//get contact list by company ID and just retrieve the contact
			//that user made
			$contacts   = $this->Contact->find('list',array(
				'conditions'=>array(
					'Contact.company_id'=>$userDetails['company_id'],
					'Contact.created_by'=>$userDetails['id']
					)
				));

			//find stage related to company id
			//use this to return dealstage list when adding new deal
			$dealStagesList = $this->Deal->DealStage->find('list',array(
				'conditions'=>array(
					'DealStage.company_id'=>$userDetails['company_id'],
					)
				));
			$deals = $this->Deal->find('list',array(
				'conditions'=>array(
					'Deal.company_id'=>$userDetails['company_id'],
					'Deal.created_by'=>$userDetails['id']
					)
				));
		}
		

		$this->set(compact('dealStages','dealStagesList','contacts','deals'));
		
	
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null,$userID = null) {
		$userDetails = $this->Session->read('Auth.User');
		if (!$this->Deal->exists($id)) {
			throw new NotFoundException(__('Invalid deal'));
		}
		
		//for security purpose
		if($userID == $userDetails['id'] || $userDetails['id'] == 1){
			$options = array('conditions' => array('Deal.'.$this->Deal->primaryKey => $id));
			$this->set('deal', $this->Deal->find('first', $options));

			$activities = $this->Activity->find('all', array(
				'conditions' => array('Activity.deal_id' => $id), 
				'order' => array('Activity.created' => 'DESC')));
			
			$users = $this->User->find('list',array(
				'conditions'=>array('User.group_id'=>2),

				));

			$dealStages = $this->Deal->DealStage->find('list');
	
			$this->set(compact('activities','users'));
		}else{
			throw new NotFoundException(__('Invalid deal'));
		}
		
		$dealStages = $this->DealStage->find('all',array(
			'conditions'=>array('DealStage.company_id'=>$userDetails['company_id'])
			));
		$this->set(compact('dealStages'));
	  
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		//get logged on user details
		$userDetails = $this->Session->read('Auth.User');

		if ($this->request->is('post')) {

			//if empty schedule activity date
			if (!empty($this->request->data['Deal']['estimate_close_date'])) {
				$this->request->data['Deal']['estimate_close_date'] = date('Y-m-d H:i:s',strtotime($this->request->data['Deal']['estimate_close_date']));
			}

			$this->request->data['Deal']['company_id'] = $userDetails['company_id'];

			//created by,get logged on user id,
			$this->request->data['Deal']['created_by'] = $userDetails['id'];
			$this->Deal->create();
			if ($this->Deal->save($this->request->data)) {
				$dealID = $this->Deal->getLastInsertID();
				// $this->Session->setFlash(__('The deal has been saved'));
				return $this->redirect(array('action' => 'view', $dealID,$userDetails['id']));
			} else {
				$this->Session->setFlash(__('The Deal could not be created. Please, fix the error & try again.'),'alert/error');
			}
		}

		//find contact related to company id
		$contacts   = $this->Deal->Contact->find('list',array(
			'conditions'=>array(
				'Contact.company_id'=>$userDetails['company_id'],
				'Contact.created_by'=>$userDetails['id'],
				),
			));

		//find stage related to company id
		$dealStages = $this->Deal->DealStage->find('list',array(
			'conditions'=>array(
				'DealStage.company_id'=>$userDetails['company_id']
				)
			));

		$this->set(compact('contacts', 'dealStages'));

	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$userDetails = $this->Session->read('Auth.User');
		if (!$this->Deal->exists($id)) {
			throw new NotFoundException(__('Invalid deal'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->Deal->id = $id;
			if ($this->Deal->save($this->request->data)) {
				$this->Session->setFlash(__('The deal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The deal could not be saved. Please, try again.'));
			}
		} else {
			$options             = array('conditions' => array('Deal.'.$this->Deal->primaryKey => $id));
			$this->request->data = $this->Deal->find('first', $options);
		}

	
		//find contact related to company id & logged on user id
		$contacts   = $this->Deal->Contact->find('list',array(
			'conditions'=>array(
				'Contact.company_id'=>$userDetails['company_id'],
				'Contact.created_by'=>$userDetails['id'],
				),
			));

		//find stage related to company id
		$dealStages = $this->Deal->DealStage->find('list',array(
			'conditions'=>array(
				'DealStage.company_id'=>$userDetails['company_id']
				)
			));
		$this->set(compact('contacts', 'dealStages'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->Deal->id = $id;
		if (!$this->Deal->exists()) {
			throw new NotFoundException(__('Invalid deal'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Deal->delete()) {
			$this->Session->setFlash(__('The deal has been deleted.'));
		} else {
			$this->Session->setFlash(__('The deal could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function funnel() {
		$userDetails = $this->Session->read('Auth.User');
		$this->Deal->DealStage->recursive = 1;
		$stats = $this->Deal->DealStage->find('all',array(
			'conditions' => array('DealStage.company_id'=>$userDetails['company_id']),
			));
		// $stats = $this->Deal->findAllByCompanyId($userDetails['company_id']);
		// // debug($stats);
		// foreach ($stats as $key => $value) {
		// 	// debug(count($value['Deal']));
		// 	return $value;
		// }
		// $this->Deal->find('all')

		$this->set(array(
			'stats' => $stats,
			'_serialize'=>array('stats'),
			));
		// exit;
		// $this->set(array(
		// 	'stats'=>$stats,
		// 	'_serialize'=>array('stats'),
		// 	));
		// $this->set(array(
		// 		'value'      => $value,
		// 		'_serialize' => $value,
		// 	));
		// $deals = $this->Deal->find('count', array(
		// // 		'conditions' => array('Deal.deal_stage_id' => 4),
		// // 		'order'      => array(),
		// // 	));
		// // // $this->set(array(
		// // // 		'deals'      => $deals,
		// // // 		'_serialize' => array('deals')
		// // // 	));
		// $perWeeks = $this->Deal->findTotalSalesPerWeek();

		// // foreach ($perWeeks as &$perWeek) {

		// // }
		// // echo json_encode(compact('perWeeks'));
		// // exit();

		// $this->set(array(
		// 		'perWeek'    => $perWeeks,
		// 		'_serialize' => 'perWeek',
		// 	));
		// // $this->set(array(
		// // 		'deals'      => $deals,
		// // 		'perWeeks'   => $perWeek,
		// // 		'perMonth'   => $perMonth,
		// // 		'_serialize' => 'perWeeks',
		// // 	));
	}

	public function getSaleStatsMonth() {
		$perMonth = $this->Deal->findTotalSalesPerMonth();

		$this->set(array(
				'perMonth'   => $perMonth,
				'_serialize' => 'perMonth'));

	}
	/**
	 * changeStage method
	 * @return json
	 */
	public function changeStage() {
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');
		$this->Deal->create();
		$this->Deal->id = $this->request->data['pk'];
		$data           = array(
			'deal_stage_id'   => $this->request->data['value'],
			'status' => NULL
		);
		$this->Deal->save($data, array('validate' => false));
		return json_encode($this->request->data['value']);

	}

	// /**
	//  * @return json
	//  */
	// public function dealStats() {
	// 	$totalDeals     = $this->Deal->countAllDeal();
	// 	$totalIncoming  = $this->Deal->countIncomingDeal();
	// 	$totalContacted = $this->Deal->countContactedDeal();
	// 	$totalQuote     = $this->Deal->countQuoteDeal();
	// 	$totalWon       = $this->Deal->countWonDeal();
	// 	$totalLost      = $this->Deal->countLostDeal();

	// 	$this->set(array(
	// 			'incoming'   => $totalIncoming,
	// 			'contacted'  => $totalContacted,
	// 			'quote'      => $totalQuote,
	// 			'won'        => $totalWon,
	// 			'lost'       => $totalLost,
	// 			'_serialize' => array('incoming', 'contacted', 'quote', 'won', 'lost'),
	// 		));
	// }

	public function won($dealID = null,$userID = null){
		$this->autoRender = false;
		$userDetails = $this->Session->read('Auth.User');
		if ($userID == $userDetails['id']) {
			$this->Deal->id = $dealID;
			$data = array(
				'deal_stage_id' => null,
				'status'=>'won'
			);
			$this->Deal->save($data);
			$this->redirect(array('action'=>'index'));
		}
	
	}


	public function lost($dealID = null,$userID = null){
		$this->autoRender = false;
		$userDetails = $this->Session->read('Auth.User');
		if ($userID == $userDetails['id']) {
			$this->Deal->id = $dealID;
			$data = array(
				'deal_stage_id' => null,
				'status'=>'lost'
			);
			$this->Deal->save($data);
			$this->redirect(array('action'=>'index'));
		}
	
	}

	public function changeDealName(){
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');
		$this->Deal->create();
		$this->Deal->id = $this->request->data['pk'];
		$data           = array('name'   => $this->request->data['value']);
		$this->Deal->save($data, array('validate' => false));
		return json_encode($this->request->data['value']);
	}

	public function changeDealValue(){
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');
		$this->Deal->create();
		$this->Deal->id = $this->request->data['pk'];
		$data           = array('value'   => $this->request->data['value']);
		$this->Deal->save($data, array('validate' => false));
		return json_encode($this->request->data['value']);
	}

	public function getDealJson(){
		$deals = $this->Deal->find('all');

		$this->set(array(
				'deals'  => $deals,
				'_serialize' => 'deals'));
	}

	public function viewWon(){
		$userDetails = $this->Session->read('Auth.User');

		if ($userDetails['group_id'] == 1) {
			$this->Paginator->settings = array(
					'conditions'=>array(
						'Deal.status'=>'won',
						'Deal.company_id'=>$userDetails['company_id']),
					'limit'=>20
			);
			$lists = $this->Deal->find('list',array(
			'conditions'=>array(
				'Deal.company_id'=>$userDetails['company_id'],
				)
			));
			$deals = $this->Paginator->paginate('Deal');

		}else{
			$this->Paginator->settings = array(
					'conditions'=>array(
						'Deal.status'=>'won',
						'Deal.company_id'=>$userDetails['company_id'],
						'Deal.created_by'=>$userDetails['id']),
					'limit'=>20
			);
			$lists = $this->Deal->find('list',array(
			'conditions'=>array(
				'Deal.company_id'=>$userDetails['company_id'],
				'Deal.created_by'=>$userDetails['id']
				)
			));

			$deals = $this->Paginator->paginate('Deal');
		}
		$totalDeals = $this->Deal->countWonDeals();
		
		$this->set(compact('deals','lists','totalDeals'));
	}

	public function viewLost(){
		$userDetails = $this->Session->read('Auth.User');

		if ($userDetails['group_id'] == 1) {
			$this->Paginator->settings = array(
					'conditions'=>array(
						'Deal.status'=>'lost',
						'Deal.company_id'=>$userDetails['company_id']),
					'limit'=>20
			);
			$lists = $this->Deal->find('list',array(
			'conditions'=>array(
				'Deal.company_id'=>$userDetails['company_id'],
				
				)
			));

			$deals = $this->Paginator->paginate('Deal');

		}else{
			$this->Paginator->settings = array(
					'conditions'=>array(
						'Deal.status'=>'lost',
						'Deal.company_id'=>$userDetails['company_id'],
						'Deal.created_by'=>$userDetails['id']),
					'limit'=>20
			);
			$lists = $this->Deal->find('list',array(
			'conditions'=>array(
				'Deal.company_id'=>$userDetails['company_id'],
				'Deal.created_by'=>$userDetails['id']
				)
			));
			$deals = $this->Paginator->paginate('Deal');
		}
		
		$this->set(compact('deals','lists'));
	}

	public function search(){
		$userDetails = $this->Session->read('Auth.User');
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$this->redirect(array('action'=>'view',$this->request->data['Deal']['deal_id'],$userDetails['id']));
		}
	}

	public function getSalesLeaderBoard(){
		$leaderBoards = $this->Deal->getSalesLeaderBoard();

		$this->set(
			array(
				'leaderBoards'  => $leaderBoards,
				'_serialize' => 'leaderBoards')
			);
	}

	public function getMtdSales(){
		$month = date('m');
		$year = date('Y');
		$target = $this->SalePerformance->getCurrentMonthTarget($month,$year);

		if (empty($target)) {
			$target['SalePerformance']['year'] = 0;
			$target['SalePerformance']['month'] = 0;
		}
		$mtd = $this->Deal->getMtdSales($target['SalePerformance']['year'],$target['SalePerformance']['month']);
		// debug($mtd);
		if (empty($mtd)) {
			$mtd = array('Deal'=>array('totalDeal'=>"0"));
		}
		$this->set(
			array(
				'mtd'  => $mtd,
				'_serialize' => 'mtd')
			);
	}
	public function currentMonthTarget(){
		$month = date('m');
		$year = date('Y');
		$target = $this->SalePerformance->getCurrentMonthTarget($month,$year);
		if (empty($target)) {
			$target = array('SalePerformance'=>array('target'=>"0"));
		}
		$this->set(
			array(
				'target'  => $target,
				'_serialize' => 'target')
		);
	}
}
