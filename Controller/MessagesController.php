<?php
App::uses('AppController', 'Controller');
/**
 * Messages Controller
 *
 * @property Message $Message
 * @property PaginatorComponent $Paginator
 */
class MessagesController extends AppController {

	public $layout = 'messages';
	public $uses = array('Message','User','Attachment');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$userDetails = $this->Session->read('Auth.User');
		$this->Message->recursive = 0;
		$messages = $this->Message->findMessageDesc($userDetails['id']);
		$this->set(compact('messages'));
		// $this->set('messages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null,$userID,$status = null) {
		$userDetails = $this->Session->read('Auth.User');
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Invalid message'));
		}

		//for security purposes
		if($userID == $userDetails['id']){
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->set('message', $this->Message->find('first', $options));
			$attachments = $this->Attachment->findAllByMessageId($id);
			$this->set(compact('attachments'));

		}else{
			throw new NotFoundException(__('Invalid message'));
		}
	
		//change status
		if ($status == 'read') {
			$this->Message->create();
			$this->Message->id = $id;
			$this->Message->saveField('status',$status);

		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			// debug($this->request->data);
			// exit();
			//change selected user id to recipient
			$this->request->data['Message']['recipient'] = $this->request->data['Message']['user_id'];

			//set who owned the message
			$this->request->data['Message']['user_id'] = $userDetails['id'];

			//set who created the message
			$this->request->data['Message']['sender'] = $userDetails['id'];
			//add status to new
			$this->request->data['Message']['status'] = 'new';

			//set delivery_status to sent
			$this->request->data['Message']['delivery_status'] = 'sent';
			if ($this->Message->save($this->request->data)) {
				$messageId = $this->Message->getLastInsertId();
				if(!empty($this->request->data['Message']['file'][0]['name'])){
					foreach ($this->request->data['Message']['file'] as $value) {
							$file                 = $this->addAttachment($value);
							//get file link
							$fileLink      = $file['link'];
							//get file type
							$fileType = $file['fileType'];
							//filename
							$fileName = $file['name'];
							//size
							$fileSize = $file['size'];

							$data = array(
								'created_by'=>$userDetails['id'],
								'message_id'=>$messageId,
								'name'=>$fileName,
								'type'=>$fileType,
								'url'=>$fileLink,
								'size'=>$fileSize,
							);
							$this->Attachment->create();
							$this->Attachment->save($data);
						
					}
				}
				$this->redirect(array('action'=>'index'));
			}else{
				$this->Session->setFlash(__('The message could not be sent. Please, try again.'));
			}
		}

		//for recipient
		$users = $this->User->find('list',array(
			'conditions'=>array('User.company_id'=>$userDetails['company_id'])
			));
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Invalid message'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('The message has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The message could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->request->data = $this->Message->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Message->id = $id;
		if (!$this->Message->exists()) {
			throw new NotFoundException(__('Invalid message'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Message->delete()) {
			$this->Session->setFlash(__('The message has been deleted.'));
		} else {
			$this->Session->setFlash(__('The message could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * addAttachment method
	 * This function will upload file,
	 * The location for the uploaded file is under
	 * folder /webroot/attachments/DATE/
	 */

	public function addAttachment($file) {

		/**
		 * TO JOMOS TEAM
		 * Google drive API integration should be done here
		 * When user uploading any file,
		 * upload the file immediately to their google account too
		 */

		
		$current_date = date('Y-m-d');

		//type of files that are allowed
		$fileTypes = array(
			'image/gif', 
			'image/jpeg',
			'image/png', 
			'application/pdf', 
			'application/msword', 
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.ms-excel',
			'application/vnd.ms-powerpoint');

		$uploadFolder = 'attachments'.DS.$current_date.DS;

		//check availability of a folder
		//return create new folder with current date if not exist

		if (file_exists($uploadFolder)) {
			$uploadFolder = $uploadFolder;
		} else {
			$old = umask(0);
			//create & give read permission to the folder
			mkdir(WWW_ROOT.'attachments'.DS.$current_date.DS, 0755, true);
			umask($old);
			// Checking
			if ($old != umask()) {
				die('An error occurred while changing back the umask');
			}
		}

		$uploadPath = WWW_ROOT.$uploadFolder;

		if (!empty($file)) {
			$ext = substr(strtolower(strrchr($file['name'], '.')), 1);//get the extension

			//check file type
			//if file type is not lists in $fileTypes array,flash an error
			if (in_array($file['type'], $fileTypes)) {
				$date     = date_create();
				$rand     = rand(100000, 999999);
				$fileName = 'attachments_'.date_format($date, 'U').'_'.$rand.'.'.$ext;
				move_uploaded_file($file['tmp_name'], $uploadPath.$fileName);

				$fileLink = $current_date.DS.$fileName;
				return array('fileType' => $file['type'], 'link' => $fileLink,'name'=>$file['name'],'size'=>$file['size']);


			} else {
				return $this->Session->setFlash(__('Please make sure the file you tried to upload are in correct format.'), 'alert/error');
				
			}
		}
	}

	public function reply($recipientID){
		//data to display
		//recipient
		$users = $this->User->find('list',array(
			'conditions'=>array('User.id'=>$recipientID)
			));
		$this->set(compact('users'));
	}

	public function viewSent(){
		$userDetails = $this->Session->read('Auth.User');
		$messages = $this->Message->find('all',array(
			'conditions'=>array('Message.user_id'=>$userDetails['id'],'Message.delivery_status'=>'sent'),
			'order'=>array('Message.created'=>'DESC')
			));
		$this->set(compact('messages'));
	}
}
