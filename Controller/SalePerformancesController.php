<?php
App::uses('AppController', 'Controller');
/**
 * SalePerformances Controller
 *
 * @property SalePerformance $SalePerformance
 * @property PaginatorComponent $Paginator
 */
class SalePerformancesController extends AppController {

	public $layout = 'setting';

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SalePerformance->recursive = 0;
		$this->set('salePerformances', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SalePerformance->exists($id)) {
			throw new NotFoundException(__('Invalid sale performance'));
		}
		$options = array('conditions' => array('SalePerformance.' . $this->SalePerformance->primaryKey => $id));
		$this->set('salePerformance', $this->SalePerformance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			$this->request->data['SalePerformance']['month'] = date('m',strtotime($this->request->data['SalePerformance']['datetime']));
			$this->request->data['SalePerformance']['year'] = date('Y',strtotime($this->request->data['SalePerformance']['datetime']));
			$this->request->data['SalePerformance']['created_by'] = $userDetails['id'];
			$this->request->data['SalePerformance']['modified_by'] = $userDetails['id'];
			$this->SalePerformance->create();
			if ($this->SalePerformance->save($this->request->data)) {
				$this->Session->setFlash(__('The sale performance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale performance could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SalePerformance->exists($id)) {
			throw new NotFoundException(__('Invalid sale performance'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->SalePerformance->id = $id;
			if ($this->SalePerformance->save($this->request->data)) {
				$this->Session->setFlash(__('The sale performance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale performance could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SalePerformance.' . $this->SalePerformance->primaryKey => $id));
			$this->request->data = $this->SalePerformance->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SalePerformance->id = $id;
		if (!$this->SalePerformance->exists()) {
			throw new NotFoundException(__('Invalid sale performance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SalePerformance->delete()) {
			$this->Session->setFlash(__('The sale performance has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale performance could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	
}
