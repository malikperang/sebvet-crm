<?php
App::uses('AppController', 'Controller');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
/**
 * SystemSettings Controller
 *
 * @property SystemSetting $SystemSetting
 * @property PaginatorComponent $Paginator
 */
class SystemSettingsController extends AppController {

	//used model
	public $uses = array('SystemSetting','DealStage','GoogleUser');

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');
	public $layout     = 'setting';
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->SystemSetting->recursive = 0;
		$this->set('systemSettings', $this->Paginator->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->SystemSetting->exists($id)) {
			throw new NotFoundException(__('Invalid system setting'));
		}
		$options = array('conditions' => array('SystemSetting.'.$this->SystemSetting->primaryKey => $id));
		$this->set('systemSetting', $this->SystemSetting->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
	
		if ($this->request->is('post')) {
			if(empty($this->request->data['SystemSetting']['name']) || empty($this->request->data['SystemSetting']['login_logo'])){
				$currentSetting = $this->SystemSetting->getLastSetting();
				$this->request->data['SystemSetting']['login_logo'] = $currentSetting['SystemSetting']['login_logo'];
				$this->request->data['SystemSetting']['name'] = $currentSetting['SystemSetting']['name'];
			}

			//upload logo image
			$logo                                               = $this->imageUpload($this->request->data['SystemSetting']['logo']);
			$login_logo                                         = $this->imageUpload($this->request->data['SystemSetting']['login_logo']);
			$this->request->data['SystemSetting']['logo']       = $logo;
			$this->request->data['SystemSetting']['login_logo'] = $login_logo;
			
			$this->SystemSetting->create();
			if ($this->SystemSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The system setting has been updated.'),'alert/success');
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The system setting could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->SystemSetting->exists($id)) {
			throw new NotFoundException(__('Invalid system setting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SystemSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The system setting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The system setting could not be saved. Please, try again.'));
			}
		} else {
			$options             = array('conditions' => array('SystemSetting.'.$this->SystemSetting->primaryKey => $id));
			$this->request->data = $this->SystemSetting->find('first', $options);
		}
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->SystemSetting->id = $id;
		if (!$this->SystemSetting->exists()) {
			throw new NotFoundException(__('Invalid system setting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SystemSetting->delete()) {
			$this->Session->setFlash(__('The system setting has been deleted.'));
		} else {
			$this->Session->setFlash(__('The system setting could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function pipelines(){
		$userDetails = $this->Session->read('Auth.User');

		$dealStages = $this->DealStage->find('all',array(
			'conditions'=>array('DealStage.company_id'=>$userDetails['company_id']),
			));
		$this->set(compact('dealStages'));
	}


	public function googleConnect(){
		//get logged on user details
		$userDetails = $this->Session->read('Auth.User');
		
		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');
		$redirect_uri =Configure::read('Google.returnUrl');

		//create google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		$client->setRedirectUri($redirect_uri);
		$client->setAccessType('offline');   // Gets us our refreshtoken
		$client->setApprovalPrompt('force');
		$client->addScope('https://www.google.com/m8/feeds ');
		$client->addScope('https://www.googleapis.com/auth/calendar');
		$client->addScope('https://www.googleapis.com/auth/plus.login');
		$client->addScope('https://www.googleapis.com/auth/plus.me');
		$client->addScope('https://www.googleapis.com/auth/userinfo.email');
		$client->addScope('https://www.googleapis.com/auth/userinfo.profile');
		$client->addScope('https://mail.google.com');
		$client->addScope('email');
		$client->setApplicationName("Sebvet S.R.S");
		
		//user perform an action connect/disconnect
		if ($this->request->is('post')) {
			 if (isset($this->request->data['connect'])) {
			 	$authUrl = $client->createAuthUrl();
			 	$this->response->location($authUrl);
			 }

			 if (isset($this->request->data['disconnect'])) {
			 	//get existing connected google account on database
			 	  $google_user = $google_user = $this->GoogleUser->find('first',array(
				  	'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
				  	));
			 	  $this->GoogleUser->id = $google_user['GoogleUser']['id'];
			 	  $data = array(
			 	  	'isConnected'=>0,
			 	  	);
			 	  $this->GoogleUser->save($data);

			 }
		}



		//oauth2 callback
		if (isset($_GET['code'])) {
			// debug('anjing');
			// exit(1);

		  	$client->authenticate($_GET['code']);
		  	$token  = $client->getAccessToken();
			
		  	//check existing account on database
		 	$googleUser = $this->GoogleUser->find('first',array(
		  	'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'])
		  	));

		  	if (!empty($googleUser)) {
		  		//update data
		  		$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
		  		$data = array(
		  			'access_token' =>  $token,
		  			'isConnected'=> 1,
		  			);
		  		$this->GoogleUser->save($data);
		  	
		 	}else{
		 	  $client->setAccessToken($token);
		  	  $access_token = json_decode($token)->access_token;
			  $curl = curl_init('https://www.googleapis.com/plus/v1/people/me?alt=json&access_token='.$access_token);
			  	  //first time user connected
		  	  $this->GoogleUser->create();
			  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
              curl_setopt($curl, CURLOPT_TIMEOUT, 10000);

			  $google_user_json = curl_exec($curl);
			  curl_close($curl);
			  $google_user = json_decode($google_user_json,true);
			
			  $this->GoogleUser->create();
			  $data = array(
			  	'user_id'=>$userDetails['id'],
			  	'access_token'=>  $token,
			  	'isConnected'=>1,
			  	'google_name'=>$google_user['displayName'],
			  	'google_email'=>$google_user['emails'][0]['value'],
			  	'google_link'=>$google_user['url'],
			  	'google_picture_link'=>$google_user['image']['url'],
			  	);
			  if ($this->GoogleUser->save($data)) {
		
			  }	else{
			  	echo $this->Session->setFlash('Something when wrong.Please try again.');
			  }


				if($this->GoogleUser->save($data)){
					$this->redirect(array('action'=>'googleConnect'));
				}else{
					echo $this->Session->setFlash('Something when wrong.Please try again.');
				};
				
			
		  	}
			
			unset($_GET['code']);

		}


		
		//check if user already connect
		$google_user = $this->GoogleUser->find('first',array(
			'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
			));
		$this->set(compact('google_user'));
	}

	public function googleCalendar(){
			//get logged on user details
			$userDetails = $this->Session->read('Auth.User');

			//google api credentials
			$client_id = Configure::read('Google.clientId');
			$client_secret = Configure::read('Google.clientSecret');

			//create new google object
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);


			$googleUser = $this->GoogleUser->find('first',array(
		  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
			));

		  	if (!empty($googleUser)) {
		    	
	    		$client->setAccessToken($googleUser['GoogleUser']['access_token']);
		    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
				$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
			
		    	if ($client->isAccessTokenExpired() == TRUE) {
		    		$client->refreshtoken($refreshToken);
				  	$newtoken= $client->getAccessToken();
				  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
				  	$data = array(
				  		'access_token'=>$newtoken,
				  		);
				  	$this->GoogleUser->save($data);
					
		    	}else{
		    		$service = new Google_Service_Calendar($client); 
		    		$list = $service->calendarList->listCalendarList();
		    		$this->set(compact('list'));
	

					// while(true) {
					//   foreach ($events->getItems() as $event) {
					//     echo $event->getSummary();
					//   }
					//   $pageToken = $events->getNextPageToken();
					//   if ($pageToken) {
					//     $optParams = array('pageToken' => $pageToken);
					//     $events = $service->events->listEvents('primary', $optParams);
					//   } else {
					//     break;
					//   }
					// }
		    	}
		    }else{
		    	echo $this->Session->setFlash('Please complete your google connection first!');
		    	$this->redirect(array('action'=>'googleConnect'));
		    }

		    //if user submitting form
		    if ($this->request->is('post')) {
		    	$googleUser = $this->GoogleUser->find('first',array(
			  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'])
			  	));

			  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
			  	$data = array(
			  		'google_calendar_id' => $this->request->data['SystemSetting']['calendar_id']
			  		);
			  	if ($this->GoogleUser->save($data)) {
			  		$this->Session->setFlash('Google calendar updated','alert/success');
			  	}else{
			  		$this->Session->setFlash('Something when wrong.Please try again');
		    		$this->redirect($this->referer());
			  	}
		    }

		    //if user already set google calendar
		     if (!empty($googleUser['GoogleUser']['google_calendar_id'])) {
	    		$usedCalendar = $googleUser['GoogleUser']['google_calendar_id'];
	   		 }
		    $this->set(compact('usedCalendar'));
		    return true;

	}

	public function googleContact(){
		//get logged on user details
		$userDetails = $this->Session->read('Auth.User');

		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');

		//create new google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);


		$googleUser = $this->GoogleUser->find('first',array(
	  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
		));

	  	if (!empty($googleUser)) {
	    	
    		$client->setAccessToken($googleUser['GoogleUser']['access_token']);
	    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
			$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;
		
	    	if ($client->isAccessTokenExpired() == TRUE) {
	    		$client->refreshtoken($refreshToken);
			  	$newtoken= $client->getAccessToken();
			  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
			  	$data = array(
			  		'access_token'=>$newtoken,
			  		);
			  	$this->GoogleUser->save($data);
				
	    	}else{
	    		
	    		$curl = curl_init('https://www.google.com/m8/feeds/groups/default/full?alt=json&v=3&access_token='.
				$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);

				$contacts_groups_json = curl_exec($curl);

				curl_close($curl);
				$contactsGroup = json_decode($contacts_groups_json,true);
				$this->set(compact('contactsGroup'));
	    	}
	    }else{
	    	echo $this->Session->setFlash('Please complete your google connection first!');
	    	$this->redirect(array('action'=>'googleConnect'));
	    }

	    //if user submitting form
	    if ($this->request->is('post')) {
	    	$googleUser = $this->GoogleUser->find('first',array(
		  		'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'])
		  	));

		  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
		  	$data = array(
		  		'google_contact_group_id' => $this->request->data['SystemSetting']['contact_id']
		  		);
		  	if ($this->GoogleUser->save($data)) {
		  		$this->Session->setFlash('Google contact group updated','alert/success');
		  	}else{
		  		$this->Session->setFlash('Something when wrong.Please try again');
	    		$this->redirect($this->referer());
		  	}
	    }

	    //if user already set google contacts group
	  
	    if (!empty($googleUser['GoogleUser']['google_contact_group_id'])) {
	    	$usedGroup = $googleUser['GoogleUser']['google_contact_group_id'];
	    }
	    $this->set(compact('usedGroup'));
	    return true;
	}

	public function googleDrive(){
		
	}
}
