<?php
App::uses('AppController', 'Controller');
/**
 * UserGroups Controller
 *
 * @property UserGroup $UserGroup
 * @property PaginatorComponent $Paginator
 */
class UserGroupsController extends AppController {

	public $layout = 'setting';

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

// /**
//  * index method
//  *
//  * @return void
//  */
// 	public function index() {
// 		$this->UserGroup->recursive = 0;
// 		$this->set('userGroups', $this->Paginator->paginate());
// 	}

// /**
//  * view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function view($id = null) {
// 		if (!$this->UserGroup->exists($id)) {
// 			throw new NotFoundException(__('Invalid user group'));
// 		}
// 		$options = array('conditions' => array('UserGroup.' . $this->UserGroup->primaryKey => $id));
// 		$this->set('userGroup', $this->UserGroup->find('first', $options));
// 	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			$this->request->data['UserGroup']['created_by'] = $userDetails['id'];
			$this->request->data['UserGroup']['company_id'] = $userDetails['company_id'];
			$this->UserGroup->create();
			if ($this->UserGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The user group has been saved.'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The user group could not be saved. Please, try again.'));
			}
		}
		$this->Paginator->settings = array(
					        'conditions' => array('UserGroup.company_id' => $userDetails['company_id']),
					        'limit' => 50,
					        'cache'=>true,
					   );
		$userGroups = $this->Paginator->paginate('UserGroup');
		$this->set(compact('userGroups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->UserGroup->id = $id;
		if (!$this->UserGroup->exists($id)) {
			throw new NotFoundException(__('Invalid user group'));
		}
		$userDetails = $this->Session->read('Auth.User');
			
		if ($this->request->is(array('put','post'))) {
			if ($this->UserGroup->save($this->request->data)) {
				return $this->redirect(array('action'=>'add'));
			} else {
				$this->Session->setFlash(__('The user group could not be saved. Please, try again.'));
			}
		}else{
			$options = array('conditions' => array('UserGroup.' . $this->UserGroup->primaryKey => $id));
			$this->request->data = $this->UserGroup->find('first', $options);
		}
		// $this->Paginator->settings = array(
		// 			        'conditions' => array('UserGroup.company_id' => $userDetails['company_id']),
		// 			        'limit' => 50,
		// 			        'cache'=>true,
		// 			   );
		// $userGroups = $this->Paginator->paginate('UserGroup');
		// $this->set(compact('userGroups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserGroup->id = $id;
		if (!$this->UserGroup->exists()) {
			throw new NotFoundException(__('Invalid user group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserGroup->delete()) {
			$this->Session->setFlash(__('The user group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
