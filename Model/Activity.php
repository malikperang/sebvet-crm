<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Deal $Deal
 */
class Activity extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Deal'        => array(
			'className'  => 'Deal',
			'foreignKey' => 'deal_id',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'User'        => array(
			'className'  => 'User',
			'foreignKey' => 'created_by',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		// 'User'        => array(
		// 	'className'  => 'User',
		// 	'foreignKey' => 'assigned_to',
		// 	'conditions' => '',
		// 	'fields'     => '',
		// 	'order'      => '',
		// )
	);

	public $hasMany = array(
		'Attachment'      => array(
			'className'    => 'Attachment',
			'foreignKey'   => 'activity_id',
			'dependent'    => true,
			'conditions'   => '',
			'fields'       => '',
			'order'        => '',
			'limit'        => '',
			'offset'       => '',
			'exclusive'    => '',
			'finderQuery'  => '',
			'counterQuery' => '',
		),
		
	);

}
