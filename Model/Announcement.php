<?php
App::uses('AppModel', 'Model');
/**
 * Announcement Model
 *
 */
class Announcement extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'created_by' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'modified_by' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public $belongsTo = array(
			'Created'        => array(
				'className'  => 'User',
				'foreignKey' => 'created_by',
				'conditions' => '',
				'fields'     => '',
				'order'      => '',
			),
			'Modified'        => array(
				'className'  => 'User',
				'foreignKey' => 'modified_by',
				'conditions' => '',
				'fields'     => '',
				'order'      => '',
			)
		);

	/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany= array(
		'Attachment' => array(
			'className' => 'Attachment',
			'foreignKey' => 'announcement_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
