<?php
App::uses('AppModel', 'Model');
/**
 * Attachment Model
 *
 * @property Announcement $Announcement
 */
class Attachment extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Activity'        => array(
			'className'  => 'Activity',
			'foreignKey' => 'activity_id',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		// 'User'        => array(
		// 	'className'  => 'User',
		// 	'foreignKey' => 'created_by',
		// 	'conditions' => '',
		// 	'fields'     => '',
		// 	'order'      => '',
		// ),
		// 'User'        => array(
		// 	'className'  => 'User',
		// 	'foreignKey' => 'assigned_to',
		// 	'conditions' => '',
		// 	'fields'     => '',
		// 	'order'      => '',
		// )
	);


}
