<?php
App::uses('AppModel', 'Model');
/**
 * Contact Model
 *
 * @property ContactStatus $ContactStatus
 * @property Company $Company
 * @property Deal $Deal
 * @property Lead $Lead
 */
class Contact extends AppModel {

	public $validate = array(
		// 'title' => array(
  //       	'rule' => 'notEmpty',
  //       	'message' => 'This field cannot be left blank'
  //   	)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed


	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Company'     => array(
			'className'  => 'Company',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'ContactCompany'     => array(
			'className'  => 'ContactCompany',
			'foreignKey' => 'contact_company_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'Owner'        => array(
			'className'  => 'User',
			'foreignKey' => 'created_by',
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Deal'          => array(
			'className'    => 'Deal',
			'foreignKey'   => 'contact_id',
			'dependent'    => false,
			'conditions'   => '',
			'fields'       => '',
			'order'        => '',
			'limit'        => '',
			'offset'       => '',
			'exclusive'    => '',
			'finderQuery'  => '',
			'counterQuery' => '',
		),

	);

	public function countAll(){
		$result = $this->find('count',array(
			));
		return $result;
	}

	public function countAllByUser($user_id = null){
		$result = $this->find('count',array(
			'conditions'=>array('Contact.created_by'=>$user_id)
			));
		return $result;
	}

	public function isExistContact($name, $email) {
		$result = $this->findByNameOrEmail($name, $email);
		return $result;
	}

}
