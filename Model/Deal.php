<?php
App::uses('AppModel', 'Model');
/**
 * Deal Model
 *
 * @property Contact $Contact
 * @property DealStage $DealStage
 */
class Deal extends AppModel {
	public $recursive = 3;

	public $validate = array(
		'contact_id' => array(
				// or: array('ruleName', 'param1', 'param2' ...)
		        'rule' => 'notEmpty',
		        'required' => true,
		        'allowEmpty' => false,
		        // or: 'update'
		        'on' => 'create',
		        'message' => 'Please select a contact!'
			),
		'deal_stage_id' => array(
				// or: array('ruleName', 'param1', 'param2' ...)
		        'rule' => 'notEmpty',
		        'required' => true,
		        'allowEmpty' => false,
		        // or: 'update'
		        'on' => 'create',
		        'message' => 'Please select a Deal Stage!'
			)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed




	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Contact'     => array(
			'className'  => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'DealStage'   => array(
			'className'  => 'DealStage',
			'foreignKey' => 'deal_stage_id',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'Owner'        => array(
			'className'  => 'User',
			'foreignKey' => 'created_by',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		)
	);

	public $hasMany = array(
		'Activity'      => array(
			'className'    => 'Activity',
			'foreignKey'   => 'deal_id',
			'dependent'    => true,
			'conditions'   => '',
			'fields'       => '',
			'order'        => '',
			'limit'        => '',
			'offset'       => '',
			'exclusive'    => '',
			'finderQuery'  => '',
			'counterQuery' => '',
		),
	);

	// /**
	//  * @return int
	//  */

	// public function getTotalWorth() {
	// 	$won_deal = $this->find('first', array(
	// 			'conditions' => array('Deal.deal_stage_id' => 4),
	// 			'fields'     => array('SUM(Deal.value) AS total'),
	// 		));
	// 	foreach ($won_deal as $result) {
	// 		# code...
	// 		return $result;
	// 	}

	// 	// debug($won_deal);
	// 	// exit();
	// }

	// /**
	//  * @return int
	//  */

	// public function getTotalActiveDeal() {
	// 	$conditions  = array('Deal.deal_stage_id BETWEEN ? AND ?' => array(1, 3));
	// 	$active_deal = $this->find('count', array(
	// 			'conditions' => $conditions,
	// 		));
	// 	return $active_deal;
	// }

	// public function getNewDeal() {
	// 	$conditions  = array('Deal.deal_stage_id BETWEEN ? AND ?' => array(1, 3));
	// 	$active_deal = $this->find('count', array(
	// 			'conditions' => $conditions,
	// 		));
	// 	return $active_deal;
	// }

	/**
	 * @return int
	 */

	// public function getTotalWonDeal() {
	// 	$conditions = array('Deal.deal_stage_id'              => 4);
	// 	$result     = $this->find('count', array('conditions' => $conditions));
	// 	return $result;
	// }

	// public function findTotalSalesPerWeek() {
	// 	$this->virtualFields['total'] = 'SUM(Deal.value)';
	// 	$this->virtualFields['day']   = 'DAYNAME(Deal.modified)';
	// 	$this->virtualFields['date']  = 'Deal.modified';
	// 	$result                       = $this->find('all', array(
	// 			'fields' => array(
	// 				// 'DISTINCT(Deal.created) AS testing',
	// 				'day',
	// 				'date',
	// 				// 'DISTINCT(Deal.name)'
	// 				'total',

	// 			),

	// 			'conditions' => array(
	// 				'YEARWEEK(Deal.modified) = YEARWEEK(CURRENT_DATE)',
	// 				'Deal.deal_stage_id' => 4,
	// 			),
	// 			'group' => array('Deal.created'),
	// 			'order' => array('Deal.created' => 'ASC'),
	// 		));

		// 		$result = $this->query("SELECT DAYNAME(date(`created`)) AS thedate, count(`value`) AS count
		// FROM deals
		// WHERE YEARWEEK(`created`) = YEARWEEK(CURRENT_DATE)
		// GROUP BY `Deals`.`created`
		// ORDER BY date(`Deals`.`created`) ASC
		// ");
		// foreach ($result as $results) {
		// 	//debug($results);
		// 	return $results;
		// }
		// $result = $this->find('all', array(''));
		// $total  = 0;
		// $result = $this->find('all', array(
		// 		'conditions' => array(
		// 			'YEARWEEK(Deal.created) = YEARWEEK(CURRENT_DATE)',
		// 		),
		// 		'group' => array('Deal.created'),
		// 		'order' => array('date(Deal.created)' => 'ASC'),
		// 	));
		// // debug($result);
		// // exit();
		// foreach ($result as $key => $value) {
		// 	// debug($value);
		// 	$total += $value['Deal']['value'];
		// 	debug($total);

		// }
		// 		$result = $this->query("SELECT DISTINCT DAYNAME(date(`created`)) AS thedate, count(`value`) AS count
		// FROM deals
		// WHERE YEARWEEK(`created`) = YEARWEEK(CURRENT_DATE)
		// GROUP BY `Deals`.`created`
		// ORDER BY date(`Deals`.`created`) ASC
		// ");
	// 	return $result;
	// }

	// public function findTotalSalesPerMonth() {
	// 	$this->virtualFields['total'] = 'SUM(Deal.value)';
	// 	$this->virtualFields['month'] = 'MONTHNAME(date(Deal.created)) ';

	// 	$result = $this->find('all', array(
	// 			'fields' => array(
	// 				'month',
	// 				'total',
	// 			),
	// 			//'conditions' => false,
	// 			'group' => 'MONTH(Deal.created)',
	// 			'order' => array('date(Deal.created)' => 'DESC'),
	// 		));

	// 	return $result;

	// }

	public function getTopDeal() {
		$this->recursive = 1;
		//unset virtualfield otherwise it will sum the value
		unset($this->virtualFields['total']);
		unset($this->virtualFields['month']);
		$result = $this->find('all', array(
				'conditions' => array('Deal.deal_stage_id' => 3),
				'order'      => array('Deal.value'      => 'DESC'),
				'limit'      => 10
			));
		return $result;
	}



	public function countSalesCycle() {
		$result = $this->find('count', array(
				'conditions' => array('Deal.deal_stage_id' => 6),
			));
		return $result;
	}

	public function getSalesLeaderBoard(){
		$this->virtualFields['totalWorth'] = 'SUM(Deal.value)';
		$this->virtualFields['totalDeal'] = 'COUNT(*)';
		$result = $this->find('all',array(
				'fields'     => array(
					'totalDeal',
					'totalWorth',
					'Deal.created_by',
					),
				'order'=>array('totalWorth'=>'DESC'),
				'group'=>'Deal.created_by'
			));
		return $result;
	}

	public function getMtdSales($year,$month){
		$this->recursive = 0;
		$this->virtualFields['totalDeal'] = 'COUNT(*)';
		$result = $this->find('first',array(
			'conditions'=>array(
				'Deal.status'=>'won'
				),
			'fields'=>array(
				'totalDeal'
				),
			'group'=>array('YEAR('.$year.')','MONTH('.$month.')'),
			'limit'=>1
			));
		return $result;
	}

	public function countWonDeals(){
		$result = $this->find('count',array(
			'conditions'=>array('Deal.status'=>'won')
			));
		return $result;
	}

	public function countLostDeals(){

	}

}
