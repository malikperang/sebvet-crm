<?php
App::uses('AppModel', 'Model');
/**
 * DealStage Model
 *
 * @property Deal $Deal
 */
class DealStage extends AppModel {
	public $recursive = 3;

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Deal' => array(
			'className' => 'Deal',
			'foreignKey' => 'deal_stage_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
