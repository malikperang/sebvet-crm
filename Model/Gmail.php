<?php
App::uses('AppModel', 'Model');
/**
 * Gmail Model
 *
 * @property User $User
 */
class Gmail extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed


	public $hasMany = array(
		'GmailsAttachment' => array(
			'className' => 'GmailsAttachment',
			'foreignKey' => 'gmail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		);
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GoogleAccount' => array(
			'className' => 'GoogleUser',
			'foreignKey' => 'google_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getAllMessage(){
		$result = $this->find('all',array(
			'conditions'=>array(),
			'group'=>'Gmail.messageId',
			));
		return $result;
	}

	public function getAllInbox(){
		$result = $this->find('all',array(
			'conditions'=>array('messageLabel'=>'INBOX'),
			'group'=>'Gmail.messageId',
			));
		return $result;
	}

	public function getAllSentMail(){
		$result = $this->find('all',array(
			'conditions'=>array('messageLabel'=>'SENT'),
			'group'=>'Gmail.messageId',
			));
		return $result;
	}
}
