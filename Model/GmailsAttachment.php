<?php
App::uses('AppModel', 'Model');
/**
 * GmailsAttachment Model
 *
 * @property Gmail $Gmail
 */
class GmailsAttachment extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Gmail' => array(
			'className' => 'Gmail',
			'foreignKey' => 'gmail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
