<?php
App::uses('AppModel', 'Model');
/**
 * Message Model
 *
 * @property Attachment $Attachment
 */
class Message extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'User'     => array(
			'className'  => 'User',
			'foreignKey' => 'user_id',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'Sender'   => array(
			'className'  => 'User',
			'foreignKey' => 'sender',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		),
		'Recipient'        => array(
			'className'  => 'User',
			'foreignKey' => 'recipient',
			'dependent'=>true,
			'conditions' => '',
			'fields'     => '',
			'order'      => '',
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Attachment' => array(
			'className' => 'Attachment',
			'foreignKey' => 'message_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function findMessageDesc($user_id){
		$results = $this->find('all',array(
			'conditions'=>array('Message.recipient'=>$user_id),
			'order'=>array('Message.created'=>'DESC')
			));
		return $results;
	}


	public function countNewMessage($user_id){
		$result = $this->find('count',array(
			'conditions'=>array('Message.recipient'=>$user_id,'Message.status'=>'new')
			));
		return $result;
	}
}	
