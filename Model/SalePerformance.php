<?php
App::uses('AppModel', 'Model');
/**
 * SalePerformance Model
 *
 */
class SalePerformance extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'target' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

		'created_by' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'modified_by' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function getCurrentMonthTarget($month,$year){
		$result = $this->find('first',array(
			'conditions'=>array(
				'SalePerformance.month'=>$month,
				'SalePerformance.year'=>$year
				),
			'order'=>array('SalePerformance.created'=>'DESC'),
			'group'=>array()
		));

		return $result;
	}
}
