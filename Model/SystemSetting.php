<?php
App::uses('AppModel', 'Model');
/**
 * SystemSetting Model
 *
 */
class SystemSetting extends AppModel {

	public function getLastSetting(){
		$setting = $this->find('first',array(
				'conditions' => array(''),
				 'order' => array('SystemSetting.created' => 'DESC')
			));

		return $setting;
	}

}
