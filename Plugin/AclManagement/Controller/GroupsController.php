<?php

App::uses('AclManagementAppController', 'AclManagement.Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AclManagementAppController {

    public $components = array('Paginator');

    function beforeFilter() {
        parent::beforeFilter();
        $this->theme = 'SebvetCRM';
        $this->layout = 'setting';
    }
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->set('title', __('Groups'));
        $this->set('description', __('Manage Groups'));

        $this->Group->recursive = 0;
        $this->set('groups', $this->paginate());
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        $this->set('group', $this->Group->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userDetails = $this->Session->read('Auth.User');
        if ($this->request->is('post')) {
            //assign company id
            $this->request->data['Group']['company_id'] = $userDetails['id'];
            $this->Group->create();
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'alert/success');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'alert/error');
            }
        }
        $this->Group->recursive = 0;
        $this->Paginator->settings = array(
            'conditions'=>array('Group.company_id'=>$userDetails['id'])
            );

        $groups = $this->Paginator->paginate('Group');
        $this->set('groups',$groups);
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $userDetails = $this->Session->read('Auth.User');
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'alert/success');
                $this->redirect(array('action' => 'add'));
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'alert/error');
            }
        } else {
            $this->request->data = $this->Group->read(null, $id);
        }

        $this->Group->recursive = 0;
        $this->Paginator->settings = array(
            'conditions'=>array('Group.company_id'=>$userDetails['id'])
            );

        $groups = $this->Paginator->paginate('Group');
        $this->set('groups',$groups);
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'), 'alert/error');
        }
        if ($this->Group->delete()) {
            $this->Session->setFlash(__('Group deleted'), 'alert/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Group was not deleted'), 'alert/error');
        $this->redirect(array('action' => 'index'));
    }

}
?>