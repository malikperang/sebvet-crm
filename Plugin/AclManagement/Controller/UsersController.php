<?php

App::uses('AclManagementAppController', 'AclManagement.Controller');
App::uses('CakeEmail', 'Network/Email');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AclManagementAppController {

	//used model
	public $uses = array('AclManagement.User','DealStage','Deal','GoogleUser','UserGroup');

	//used component
	public $components = array('Paginator');

	function beforeFilter() {
		parent::beforeFilter();

		$this->layout = "default";

		$this->Auth->allow('login', 'logout', 'forgot_password', 'register', 'activate_password', 'confirm_register', 'confirm_email_update');

		$this->User->bindModel(array('belongsTo' => array(
					'Group'                               => array(
						'className'                          => 'AclManagement.Group',
						'foreignKey'                         => 'group_id',
						'dependent'                          => true
					)
				)), false);


	}
	/**
	 * Temp acl init db
	 */
	//    function initDB() {
	//        $this->autoRender = false;
	//
	//        $group = $this->User->Group;
	//        //Allow admins to everything
	//        $group->id = 1;
	//        $this->Acl->allow($group, 'controllers');
	//
	//        //allow managers to posts and widgets
	//        $group->id = 2;
	//        $this->Acl->deny($group, 'controllers');
	//        //$this->Acl->allow($group, 'controllers/Posts'); //allow all action of controller posts
	//        $this->Acl->allow($group, 'controllers/Posts/add');
	//        $this->Acl->deny($group, 'controllers/Posts/edit');
	//
	//        //we add an exit to avoid an ugly "missing views" error message
	//        echo "all done";
	//        exit;
	//    }
	/**
	 * login method
	 *
	 * @return void
	 */
	function login() {
		$this->layout = 'auth';

		//google api credentials
		$client_id = Configure::read('Google.clientId');
		$client_secret = Configure::read('Google.clientSecret');

		//create google object
		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);
		  
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$userDetails = $this->Session->read('Auth.User');
				$googleUser = $this->GoogleUser->find('first',array(
					'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
					));
			 	if (!empty($googleUser)) {
			    	$client->setAccessToken($googleUser['GoogleUser']['access_token']);
			    	$access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
					$refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;

			    	if ($client->isAccessTokenExpired() == TRUE) {
			    		$client->refreshtoken($refreshToken);
					  	$newtoken= $client->getAccessToken();
					  	$this->GoogleUser->id = $googleUser['GoogleUser']['id'];
					  	$data = array(
					  		'access_token'=>$newtoken,
					  		);
					  	$this->GoogleUser->save($data);
			    	}
			    }else{
			    	$this->redirect($this->Auth->redirect());
			    }



			    $this->redirect(array('action'=>'companyPerformance'));
				
			} else {
				$this->Session->setFlash('Your username or password was incorrect.', 'alert/error');
			}
		}
	}


	public function socialSign(){

		if ($this->request->is('post')) {
			debug($this->request->data);
		}
		// //for google application
		// /************************************************
		//   ATTENTION: Fill in these values! Make sure
		//   the redirect URI is to this page, e.g:
		//   http://localhost:8080/user-example.php
		//  ************************************************/
		// $client_id = '655021573082-57vl29mcfv15vvhhtaocj67sc5gctenl.apps.googleusercontent.com';
		// $client_secret = 'AXGmFbBvO57eOHGMq_FB7XKW';
		// $redirect_uri = 'http://localhost/my/crm/users/socialSign';

		// $client = new Google_Client();
		// $client->setClientId($client_id);
		// $client->setClientSecret($client_secret);
		// $client->setRedirectUri($redirect_uri);
		// $client->setScopes('email');

		// if ($product == 'google') {
		// 	$authUrl = $client->createAuthUrl();
		// 	$this->redirect($authUrl);
			
		// }
		// if (isset($this->request->query['code'])) {
		//   	$client->authenticate($this->request->query['code']);
		//   	$this->Session->write('access_token',$client->getAccessToken());
		//   	// $this->redirect($this->Auth->redirect());
		// }

		// $access_token = $this->Session->read('access_token');

		// if (isset($access_token)) {
		// 	 $client->setAccessToken($access_token);
		// }

		// /************************************************
		//   If we're signed in we can go ahead and retrieve
		//   the ID token, which is part of the bundle of
		//   data that is exchange in the authenticate step
		//   - we only need to do a network call if we have
		//   to retrieve the Google certificate to verify it,
		//   and that can be cached.
		//  ************************************************/
		// if ($client->getAccessToken()) {
		//  	$this->Session->write('access_token',$client->getAccessToken());

		//   	$token_data = $client->verifyIdToken()->getAttributes();
		//   	debug($token_data);
		//   	if ($token_data['payload']['email'] == 'malikperang@gmail.com') {
		  		
		//   		$this->redirect(array('plugin'=>false,'controller'=>'deals','action'=>'index'));
		//   	}
		// }
	}


	//google connection
	public function connection(){

	}


	/**
	 * logout method
	 *
	 * @return void
	 */
	function logout() {
		$this->Session->destroy();
		// $this->Session->setFlash('Good-Bye', 'alert/success');
		$this->redirect($this->Auth->logout());
	}
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$userDetails = $this->Session->read('Auth.User');
		$this->layout = 'setting';
		$this->set('title', __('Users'));
		$this->set('description', __('Manage Users'));


		$this->User->recursive = 1;

		$this->paginate        = array(
			'conditions'=>array('User.company_id'=>$userDetails['company_id']),
			'limit' => 20,
		);
		$this->set('users', $this->paginate("User"));
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->layout   = 'setting';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'), 'alert/error');
		}
		$this->set('user', $this->User->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$userDetails = $this->Session->read('Auth.User');
		$this->layout = 'setting';
		if ($this->request->is('post')) {
			
			if (empty($this->request->data['User']['profile_picture']['name'])) {
				$this->request->data['User']['profile_picture'] = NULL;
			}else{
				$this->request->data['User']['profile_picture'] = $this->imageUpload($this->request->data['User']['profile_picture']);
			}
				

			$this->request->data['User']['company_id'] = $userDetails['company_id'];
		
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'alert/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'alert/error');
			}
		}
		
		$userGroups = $this->UserGroup->find('list',array(
			'conditions'=>array('UserGroup.company_id'=>$userDetails['company_id'])
			));
		$groups = $this->User->Group->find('list',array());
		$this->set(compact('groups','userGroups'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$userDetails = $this->Session->read('Auth.User');
		$this->layout   = 'setting';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post','put'))) {
			if (empty($this->request->data['User']['profile_picture']['name'])) {
				$user = $this->User->findById($id);
				$this->request->data['User']['profile_picture'] = $user['User']['profile_picture'];
			}else{
				$this->request->data['User']['profile_picture'] = $this->imageUpload($this->request->data['User']['profile_picture']);
			}
				
			if ($this->User->save($this->request->data)) {
				$user = $this->User->findById($id);
				$this->Session->write('Auth',$user);
				$this->Session->setFlash(__('The user has been saved'), 'alert/success');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'alert/error');
			}
		} else {
			$this->request->data                     = $this->User->read(null, $id);
			$this->request->data['User']['password'] = null;
		}
		$user   = $this->User->findById($id);
		$groups = $this->User->Group->find('list');
		$userGroups = $this->UserGroup->find('list',array(
			'conditions'=>array('UserGroup.company_id'=>$userDetails['company_id'])
			));

		$this->set(compact('groups', 'user','userGroups'));
	}

	/**
	 * delete method
	 *
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'), 'alert/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'), 'alert/error');
		$this->redirect(array('action' => 'index'));
	}

	/**
	 *  Active/Inactive User
	 *
	 * @param <int> $user_id
	 */
	public function toggle($user_id, $status) {
		$this->layout = "ajax";
		$status       = ($status)?0:1;
		$this->set(compact('user_id', 'status'));
		if ($user_id) {
			$data['User'] = array('id'                                           => $user_id, 'status'                                           => $status);
			$allowed      = $this->User->saveAll($data["User"], array('validate' => false));
		}
	}

	/**
	 * register method
	 *
	 * @return void
	 */
	public function register() {
		$this->layout = 'auth';
		if ($this->request->is('post')) {
			$this->loadModel('AclManagement.User');
			$this->User->create();
			$this->request->data['User']['group_id'] = 2;//member
			$this->request->data['User']['status']   = 0;//active user
			$token                                   = md5(time());
			$this->request->data['User']['token']    = $token;//key
			if ($this->User->save($this->request->data)) {
				$ident        = $this->User->getLastInsertID();
				$comfirm_link = Router::url("/acl_management/users/confirm_register/$ident/$token", true);

				$cake_email = new CakeEmail('smtp');
				$cake_email->from(array('user@sebvet.my' => 'Please Do Not Reply'));
				$cake_email->to($this->request->data['User']['email']);
				$cake_email->subject(''.__('Register Confirm Email'));
				$cake_email->viewVars(array('comfirm_link' => $comfirm_link));
				$cake_email->emailFormat('html');
				$cake_email->template('AclManagement.register_confirm_email');
				$cake_email->send();

				$this->Session->setFlash(__('Thank you for sign up! Please check your email to complete registration.'), 'alert/success');
				$this->request->data = null;
				$this->redirect(array('action' => 'login'));
			} else {
				$this->Session->setFlash(__('Register could not be completed. Please, try again.'), 'alert/error');
				$this->redirect(array('action' => 'login'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}
	/**
	 * confirm register
	 * @return void
	 */
	public function confirm_register($ident = null, $activate = null) {//echo $ident.'  '.$activate;
		$return = $this->User->confirmRegister($ident, $activate);
		if ($return) {
			
			$this->Session->setFlash(__('Congrats! Register Completed.'), 'alert/success');

			$this->redirect(array('action' => 'login'));
		} else {
			$this->Session->setFlash(__('Something went wrong. Please, check your information.'), 'alert/error');
		}
	}
	/**
	 * forgot password
	 * @return void
	 */
	public function forgot_password() {
		if ($this->request->is('post')) {
			//$this->autoRender = false;
			$email = $this->request->data["User"]["email"];
			if ($this->User->forgotPassword($email)) {
				$this->Session->setFlash(__('Please check your email for instructions on resetting your password.'), 'alert/success');
				$this->redirect(array('action' => 'login'));
			} else {
				$this->Session->setFlash(__('Your email is invalid or not registered.'), 'alert/error');
			}
		}
	}
	/**
	 * active password
	 * @return void
	 */
	public function activate_password($ident = null, $activate = null, $expiredTime) {//echo $ident.'  '.$activate;
		$nowTime = strtotime(date('Y-m-d H:i'));
		if (empty($expiredTime) || $nowTime > $expiredTime) {
			$this->Session->setFlash(__('Your link had been expired.'), 'alert/error');
			$this->redirect(array('action' => 'login'));
		}

		if ($this->request->is('post')) {
			if (!empty($this->request->data['User']['ident']) && !empty($this->request->data['User']['activate'])) {
				$this->set('ident', $this->request->data['User']['ident']);
				$this->set('activate', $this->request->data['User']['activate']);

				$return = $this->User->activatePassword($this->request->data);
				if ($return) {
					$this->User->set($this->request->data);
					if ($this->User->validates()) {
						$this->request->data['User']['id'] = $this->request->data['User']['ident'];
						if ($this->User->saveAll($this->request->data['User'], array('validate' => false))) {
							$this->Session->setFlash(__('New password is saved.'), 'alert/success');
							$this->redirect(array('action' => 'login'));
						}
					} else {
						$errors = $this->User->validationErrors;
						$this->Session->setFlash(__('Error Occur!'), 'alert/error');
					}
				} else {
					$this->Session->setFlash(__('Sorry password could not be saved. Please check your email and click the password reset link again.'), 'alert/error');
				}
			}
		}
		$this->set(compact('ident', 'activate'));
	}

	/**
	 * edit profile method
	 *
	 * @return void
	 */
	public function edit_profile() {
		if ($this->request->is('post') || $this->request->is('put')) {
			if (!empty($this->request->data['User']['password']) || !empty($this->request->data['User']['password2'])) {
				//do nothing
			} else {
				//do not check password validate
				unset($this->request->data['User']['password']);
			}

			$this->User->set($this->request->data);
			if ($this->User->validates()) {
				//check email change
				if ($this->request->data['User']['email'] != $this->Session->read('Auth.User.email')) {
					$this->Session->write('Auth.User.needverify_email', $this->request->data['User']['email']);
					$id           = $this->Session->read('Auth.User.id');
					$email        = base64_encode($this->request->data['User']['email']);
					$expiredTime  = strtotime(date('Y-m-d H:i', strtotime('+24 hours')));
					$comfirm_link = Router::url("/acl_management/users/confirm_email_update/$id/$email/$expiredTime", true);
					$cake_email   = new CakeEmail('smtp');
					$cake_email->from(array('no-reply@example.com' => 'Please Do Not Reply'));
					$cake_email->to($this->request->data['User']['email']);
					$cake_email->subject(''.__('Email Address Update'));
					$cake_email->viewVars(array('comfirm_link' => $comfirm_link, 'old_email' => $this->Session->read('Auth.User.email'), 'new_email' => $this->request->data['User']['email']));
					$cake_email->emailFormat('html');
					$cake_email->template('AclManagement.email_address_update');
					$cake_email->send();

					unset($this->request->data['User']['email']);
				}

				$this->request->data['User']['id'] = $this->Session->read('Auth.User.id');
				if ($this->User->saveAll($this->request->data['User'], array('validate' => false))) {
					$this->Session->setFlash(__('Congrats! Your profile has been updated successfully'), 'alert/success');
					$this->redirect(array('action' => 'edit_profile', ));
				}
			} else {
				$errors = $this->User->validationErrors;
				$this->Session->setFlash(__('Something went wrong. Please, check your information.'), 'alert/error');
			}

		} else {
			$this->request->data                     = $this->User->read(null, $this->Auth->user('id'));
			$this->request->data['User']['password'] = '';
		}
	}
	/**
	 * confirm register
	 * @return void
	 */
	public function confirm_email_update($id = null, $email = null, $expiredTime = null) {
		$nowTime = strtotime(date('Y-m-d H:i'));
		if (empty($expiredTime) || $nowTime > $expiredTime) {
			$this->Session->setFlash(__('Your link had been expired.'), 'alert/error');
			$this->redirect(array('action' => 'login'));
		}

		$email = base64_decode($email);
		if (Validation::email($email)) {
			$checkExistId    = $this->User->find('count', array('conditions' => array('User.id' => $id)));
			$checkExistEmail = $this->User->find('count', array('conditions' => array('User.email' => $email)));

			if ($checkExistId > 0 && $checkExistEmail <= 0) {
				$this->request->data['User']['id']    = $id;
				$this->request->data['User']['email'] = $email;
				$this->User->saveAll($this->request->data, array('validate' => false));
				$this->Auth->logout();
				$this->Session->setFlash(__('Email updated. Now, you can login with new email.'), 'alert/success');
				$this->redirect(array('action' => 'login'));
			}
		}

		$this->Session->setFlash(__('Something went wrong. Sorry for any inconvenience.'), 'alert/error');
		$this->redirect(array('action' => 'login'));
	}

	//below function will not be used
	public function dashboard(){
		$userDetails = $this->Session->read('Auth.User');
		//filter dealstage by company id
		$dealStages = $this->DealStage->find('all',array(
			'conditions'=>array('DealStage.company_id'=>$userDetails['company_id']),
			));
		$deals = $this->Deal->find('count',array(
			'conditions'=>array('Deal.created_by'=>$userDetails['id']),
			'order'=>array('Deal.deal_stage_id'=>'DESC'),
			));
		$this->set(compact('dealStages','deals'));
	}

	public function companyPerformance(){
		$salesLeaders = $this->Deal->getSalesLeaderBoard();
		
		$deals = $this->Deal->find('all');
		$this->set(compact('deals','salesLeaders'));
	}

	public function personalPerformance($userID = null){
		$userDetails = $this->Session->read('Auth.User');
		if (isset($userID)) {
			
			//filter dealstage by company id
			$dealStages = $this->DealStage->find('all',array(
				'conditions'=>array('DealStage.company_id'=>$userDetails['company_id']),
				));
			$deals = $this->Deal->find('count',array(
				'conditions'=>array('Deal.created_by'=>$userDetails['id']),
				'order'=>array('Deal.deal_stage_id'=>'DESC'),
				));
			$users = $this->User->findById($userID);
			

		}else{
			$this->Paginator->settings = array(
        		'conditions' => array('User.company_id' => $userDetails['company_id']),
        		'limit' => 20
    			);
		 	$users = $this->Paginator->paginate('User');
		}

	
	
		$this->set(compact('dealStages','deals','users'));
	}

	/**
	 * imageUpload only method
	 * This function will upload file,
	 * The location for the uploaded file is under
	 * folder /webroot/attachments/DATE/
	 */

	public function imageUpload($image) {
	
		$current_date = date('Y-m-d');

		//image upload
		$imageTypes   = array('image/gif', 'image/jpeg', 'image/png');
		$uploadFolder = 'img'.DS.'uploads'.DS.$current_date.DS;

		//check availability of a folder
		//return create new folder with current date if not exist

		if (file_exists($uploadFolder)) {
			$uploadFolder = $uploadFolder;
		} else {
			$old = umask(0);
			//give read permission
			mkdir('img'.DS.'uploads'.DS.$current_date.DS, 0755, true);
			umask($old);
			// Checking
			if ($old != umask()) {
				die('An error occurred while changing back the umask');
			}
		}

		$uploadPath = WWW_ROOT.$uploadFolder;

		if (!empty($image)) {
			$ext = substr(strtolower(strrchr($image['name'], '.')), 1);//get the extension

			//check file type
			//if not an image type,flash an error
			if (in_array($image['type'], $imageTypes)) {
				$date      = date_create();
				$rand      = rand(100000, 999999);
				$file_name = 'pic_'.date_format($date, 'U').'_'.$rand.'.'.$ext;
				move_uploaded_file($image['tmp_name'], $uploadPath.$file_name);

				$imagelink = $current_date.DS.$file_name;
				return $imagelink;

			} else {
				$this->Session->setFlash(__('Please make sure the image you tried to upload are in correct format : jpeg/gif/png'), 'alert/error');
				return false;
			}
		}

	}
}
?>