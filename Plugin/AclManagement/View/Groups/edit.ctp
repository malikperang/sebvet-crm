<?php //debug($groups);?>

<div class="page-header">
<h1> <?php echo __('Access Control Level Group');?>
      <?php echo $this->Html->link(__('<i class="fa fa-plus-circle"></i> Add New Group'), array('action' => 'add'), array('class'=>'btn btn-default','escape'=>false)); ?>
</h1>
<p>By default there 2 types of <strong>Access Control Level (ACL)</strong> Group which is <strong>Admin</strong> & <strong>Members</strong> . However,you can add more group but we are strongly recommend this two types because its already normalize.</p>
</div>
<div class="row setting-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
    <div class="panel panel-default">
    <div class="panel-body">
<?php echo $this->Form->create('Group', array('class'=>'form-horizontal'));?>
    <fieldset>
    <?php
            echo $this->Form->input('name', array('div'=>'control-group',
                'before'=>'<label class="control-label">'.__('ACL Group Name').'</label><div class="controls">',
                'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
    ?>
    <br />
    <div class="text-center">    
    <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-default', 'div'=>false, 'disabled'=>false));?>
    </div>
    </fieldset>
<?php echo $this->Form->end();?>
</div>
</div>
</div>
<div class="col-lg-8">
  <div class="panel panel-default">
    <div class="panel-body">
    <table cellpadding="0" cellspacing="0" class="table table-bordered">
    <tr>
            <th class="header">#</th>
            <th class="header"><?php echo $this->Paginator->sort('name');?></th>
            <th class="header"><?php echo $this->Paginator->sort('created');?></th>
            <th class="header"><?php echo $this->Paginator->sort('modified');?></th>
            <th class="header"><?php echo __('Actions');?></th>
    </tr>
    <?php
    $num = 1;
    foreach ($groups as $group):
        ?>
    <tr>
        <td><?php echo $num++ ?>&nbsp;</td>
        <td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
        <td><?php echo date('d M Y',strtotime(h($group['Group']['created']))); ?>&nbsp;</td>
        <td><?php echo date('d M Y',strtotime(h($group['Group']['modified']))); ?>&nbsp;</td>
        <td class="">
            <div class="btn-group">
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $group['Group']['id']), array('class'=>'btn btn-default')); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $group['Group']['id']), array('class'=>'btn btn-default'), __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
            </div>
        </td>
    </tr>
        <?php endforeach; ?>
    </table>
    <?php echo $this->element('pagination');?>
</div>
</div>
</div>
</div>
</div>
