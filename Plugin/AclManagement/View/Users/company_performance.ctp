  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/highcharts-more.js"></script>
  <script src="http://code.highcharts.com/modules/funnel.js"></script>
  <script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <style type="text/css">
.highcharts-data-labels div {
  /*left:190px!important;*/
    text-align:center!important;
    width:100px;
}
.highcharts-container { overflow: auto !important; }
  </style>
  <script type="text/javascript">
    $(function() {
    $( ".resizable" ).resizable();
  });

    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    function mtdChart(achievement,target){

        $('#gauge_chart').highcharts({
              chart: {
                   type: 'solidgauge',
                   fontFamily: 'Open Sans'
              },

              title: null,

              pane: {
                  center: ['50%', '85%'],
                  size: '140%',
                  startAngle: -90,
                  endAngle: 90,
                  background: {
                      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                      innerRadius: '60%',
                      outerRadius: '100%',
                      shape: 'arc'
                  }
              },

              tooltip: {
                  enabled: false
              },

              
              yAxis: {
                  min: 0,
                  max: target,
                  title: {
                      text: 'Speed'
                  },
                  stops: [
                      [0.1, '#55BF3B'],
                      [0.5, '#DDDF0D'], 
                      [0.9, '#DF5353']
                  ],
                  lineWidth: 0,
                  minorTickInterval: null,
                  tickPixelInterval: 400,
                  tickWidth: 0,
                  title: {
                      y: -70
                  },
                  labels: {
                      y: 16
                  }
              },

              plotOptions: {
                height: '200px',
                width:'20%',
                  solidgauge: {
                      dataLabels: {
                          y: 5,
                          borderWidth: 0,
                          useHTML: true
                      }
                  }
              },
              credits: {
                  enabled: false
              },

              series: [{
                  animation:false,
                  name: 'Speed',
                  data: [achievement],
                  dataLabels: {
                      format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                          ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                             '<span style="font-size:14px;color:silver">Deals</span></div>'
                  },
                  tooltip: {
                      valueSuffix: ' Deals '
                  }
              }]
        });

        var handler = setTimeout(function () {
            var mtd = parseInt(getMtdSales());
            var target = parseInt(getSalesTarget());
            mtdChart(mtd,target);

        }, 5000); 


    }


      function funnelChart(){

           var data = $.ajax({
              url: "<?php echo $this->webroot;?>deals/funnel.json",
              dataType:"json",
              async: false
              }).responseText;
        
            data = JSON.parse(data);

            var chartdata = [];
            var totalDealPerPipeLine = 0;
             $.each(data['stats'], function (index, value) {
              
                var dealname = value['DealStage']['name'];

                var dealtotal = value['Deal'].length;
                var x = [dealname,dealtotal];
                totalDealPerPipeLine += dealtotal;
          
          
                chartdata.push(x);
            });


             if (totalDealPerPipeLine == 0) {
                 $('#funnel-zero').show();
             }else{
                $('#funnel-zero').hide();
             };

              $('#funnel_chart').highcharts({
                exporting: { enabled: false },
                  credits: {
                     enabled: false
                  },
                  chart: {
                    fontFamily: 'Open Sans',
                      type: 'funnel',
                      marginRight: 100,
                    
                  },
                  title: {
                   text:''
                  },
                  plotOptions: {
                      series: {
                          dataLabels: {
                              // align:'right',
                              position:'inside',
                              overflow:'none',
                              crop:false,
                              distance:2,
                              enabled: true,
                              useHTML:true,
                              format: '<b>{point.name}</b> ({point.y:,.0f})',
                              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                              softConnector: true,
                              formatter:function(){
                                return '<div class="datalabel">'+this.y+'</div>';
                              }
                          },
                          neckWidth: '30%',
                          neckHeight: '25%',
                      }
                  },
                  legend: {
                      enabled: false
                  },
                      series: [{
                          name: 'Deals',
                          data: chartdata
                      }]
                });

           var handler = setTimeout(function () {      
              funnelChart();
           }, 5000); 


        };


    function salesLeaderBoard(){
        var data = $.ajax({
            url: "<?php echo $this->webroot;?>deals/getSalesLeaderBoard.json",
            dataType:"json",
            async: false
            }).responseText;
    
        data = JSON.parse(data);
        var path = "<?php echo $this->webroot;?>webroot/img/uploads/";
        var num = 1;
         $.each(data, function (index, value) {
            if (value['Owner']['profile_picture'] == null) {
                $('#leaderboard').append('<tr><td>'+ordinal_suffix_of(num)+'</td><td> <img src="<?php echo $this->webroot;?>/webroot/img/default/no-profile-img.gif" class="img-responsive" style="height:20px;"> '+value['Owner']['name']+' </td><td>'+value['Deal']['totalWorth']+'</td><td> '+value['Deal']['totalDeal']+' </td></tr>');    
                num++;   
            }else{
                $('#leaderboard').append('<tr><td>'+ordinal_suffix_of(num)+'</td><td> <img src="'+path+value['Owner']['profile_picture']+'" class="img-responsive" style="height:20px;">'+value['Owner']['name']+' </td><td>'+value['Deal']['totalWorth']+'</td><td> '+value['Deal']['totalDeal']+' </td></tr>');      
                num++; 
            };
           

        });
           $('html, body').stop();
    }

    function getSalesTarget(){
       var data =  $.ajax({
            url: "<?php echo $this->webroot;?>deals/currentMonthTarget.json",
            dataType:"json",
            async: false,
          }).responseText;

       data = JSON.parse(data);
        
       $('html, body').stop();
       return data['SalePerformance']['target'];

    }

    function getMtdSales(){
       var data =  $.ajax({
            url: "<?php echo $this->webroot;?>deals/getMtdSales.json",
            dataType:"json",
            async: false,
          }).responseText;
       data = JSON.parse(data);
         $('html, body').stop();
       return data['Deal']['totalDeal'];
     
    }

    var userID = "<?php echo $userDetails['id'];?>";

    function getActivitiviesFeed(){
      $.get( "<?php echo $this->webroot;?>activities/liveFeed.json", function( data ) {
        $("#activity-feed").find(".feed-row").remove();
        var count = 0;

        $.each(data,function(index, value) {

            var dealID = value['Deal']['id'];
            var dealName = value['Deal']['name'];
            if(dealName != null){
               $('#activity-feed').append('<tr class="feed-row"><td>'+value['User']['name']+'</td><td>'+value['Activity']['notes']+'</td><td>'+value['Activity']['created']+'</td><td><a href="<?php echo $this->webroot;?>deals/view/'+value['Deal']['id']+'/'+userID+'">'+dealName+'</a></td></tr>');      
            }else{
                 $('#activity-feed').append('<tr class="feed-row"><td>'+value['User']['name']+'</td><td>'+value['Activity']['notes']+'</td><td>'+value['Activity']['created']+'</td><td>No Deal Associated</td></tr>');      
            }
          count++;
          if (count == 5) {
             $("#activity-feed").closest('tr').remove();
          };
        });
      
      }, "json");
    
      var handler = setTimeout(function () {
       getActivitiviesFeed();
      }, 5000); 
     
    }
     
  $( document ).ajaxStart(function(e) {
     e.preventDefault();
  });

  function counter(){
      var target = parseInt(getSalesTarget());
      var mtd = parseInt(getMtdSales());

      if(target == 0){
        $('#salesTarget').text('0');
        $('#noTarget').text('Please set sales target for this month');
      }else{
          $('#noTarget').text('');
        $('#salesTarget').text(target);
        $('#totalDeal').text(mtd);
      }
     

      var handler = setTimeout(function () {
        counter();
      }, 5000); 
  }


  $(document).ready(function(){
      getActivitiviesFeed();
      var mtd = parseInt(getMtdSales());
      var target = parseInt(getSalesTarget());
      mtdChart(mtd,target);
      counter();
      funnelChart();

      salesLeaderBoard();


      $(window).resize(function() {
          clearTimeout(this.id);
          this.id = setTimeout(doneResizing, 500);
      });

  });
  


</script>

<style type="text/css">
  .panel-title{
    font-size: 18px;
  }
</style>
 <div class="page-header">
    <h1 class="page-header-override">
      <?php echo __('Company Sales Performance');?>
    </h1>
  </div>

<div class="row">
<div class="col-lg-8">
  <div class="panel panel-default">
          <div class="panel-heading ">
          
          <span class="panel-title">
             <strong><?php echo __('Sales Leaderboard');?></strong></span>
             
          </div>
          
            <div class="table-responsive">
             <table class="table table-hover table-bordered no-hd-color" style="border-radius:4px;">
              <thead>
                <tr>
                    <th><i class="ace-icon fa fa-star"></i>Places</th>
                    <th><i class="ace-icon fa fa-star"></i>Name</th>
                    <th><i class="ace-icon fa fa-star"></i>Total Worth</th>
                    <th><i class="ace-icon fa fa-star"></i>Total Deal</th>
                </tr>    
               </thead>
               <tbody id="leaderboard">
            

                </tbody>
              </table>    
     
      </div>
      </div>
      </div>
<div class="col-lg-4" >
  <div class="panel panel-default" >
          <div class="panel-heading">
              <span class="panel-title">
             <strong><?php echo __('Sales Funnel');?></strong></span>
          </div>
           <div class="panel-body">
             <div class="alert alert-warning" role="alert" id="funnel-zero" style="display:none;">
                <strong>Heads Up!</strong> No deals currently running on your company pipeline.Add one and see the funnel chart generated.
               </div>
               <div  class="resizable" id="funnel_chart" style="width: auto; height: 200px;"></div>
      </div>
      </div>
      </div>
      </div>
<div class="row">
<div class="col-lg-8">
  <div class="panel panel-default">
          <div class="panel-heading">
            <span class="panel-title">
             <strong><?php echo __('Activities Feed');?></strong></span>
                        <a href="<?php echo $this->webroot;?>activities">
                           View All
                        </a>
              </div>
      
            <table class="table table-bordered table-striped no-hd-color" id="feed-table" style="overflow:auto;">
              <thead>
                  <tr>
                    <th><i class="ace-icon fa fa-caret-right blue"></i>User</th>
                    <th><i class="ace-icon fa fa-caret-right blue"></i>Notes</th>
                    <th><i class="ace-icon fa fa-caret-right blue"></i>Created at</th>
                    <th><i class="ace-icon fa fa-caret-right blue"></i>Asscociated Deal</th>
                  </tr>
              </thead>
              <tbody  id="activity-feed">
                  
              </tbody>
            </table>
  

     
      </div>
      </div>

<div class="col-lg-4">
  <div class="panel panel-default">
          <div class="panel-heading">
           <span class="panel-title">
             <strong><?php echo __('MTD Sales');?></strong></span>
          </div>
           <div class="widget-body">
            <div class="widget-main">

              <p class=""> 
              Current Month:<span id="current-month"></span>
              <br />
               This Month Sales Target : <span id="salesTarget"></span> <span id="noTarget" style="color:red;"></span>
                  <?php echo $this->Html->link('Update Sales Target',array('plugin'=>false,'controller'=>'sale_performances'),array('class'=>'btn btn-default btn-sm pull-right'));?>
               <br />
               Current Achievements : <span id="totalDeal"></span>
               
               </p>


              </div>
              <div class="center-block" id="gauge_chart" style="width: auto; height: 200px;"></div>
            
            <div class="btn-group center" style="padding:5px;">
              <?php echo $this->Html->link('View Won Deals',array('plugin'=>false,'controller'=>'deals','action'=>'viewWon'),array('class'=>'btn btn-sm btn-default','escape'=>false));?>
              <?php echo $this->Html->link('View Lost Deals',array('plugin'=>false,'controller'=>'deals','action'=>'viewLost'),array('class'=>'btn btn-sm btn-default','escape'=>false));?>
            </div>
            
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>


</div>
</div>