<?php 
// debug($dealStages);
// foreach ($dealStages as $key => $value) {
//   echo $value['DealStage']['name'];
//   // debug($value);
//   if (isset($value['Deal'][0])) {
//     if ($value['Deal'][0]['created_by'] == $userDetails['id']) {
//       echo '<br />';
//         echo count($value['Deal']);
//     }
//   }
//   // echo $value['DealStage']['name'];
// }
?>

  <script type="text/javascript" src="https://www.google.com/jsapi"></script>

  <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart','gauge']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function lineChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Lead', 3],
          ['Contact Made', 1],
          ['Appointment/Demo', 1],
          ['Booking/Financing', 1],
          ['Invoiced', 2]
        ]);

        // Set chart options
        var options = {'title':'',
                      };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);

      }

      function meterChart(){
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Lead', 80],
          ['Invoice', 55],
        ]);

        var options = {
         
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('meter_chart'));

        chart.draw(data, options);

        setInterval(function() {
          data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
          chart.draw(data, options);
        }, 13000);
        setInterval(function() {
          data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
          chart.draw(data, options);
        }, 5000);
        setInterval(function() {
          data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
          chart.draw(data, options);
        }, 26000);
      }

      function weekChart(){
         //perweek chart
        var perWeek = $.ajax({
          url: "<?php echo $this->webroot;?>deals/getSaleStatsWeek.json",
          dataType:"json",
          async: false
          }).responseText;
   

        var perWeek = JSON.parse(perWeek);
     
      var sunday = 0;
      var monday = 0;
      var tuesday = 0;
      var wednesday = 0;
      var thursday = 0;
      var friday = 0;
      var saturday = 0;

    var curr = new Date();
var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));

firstday = moment(firstday);
firstday = firstday.format('Do MMMM YYYY');
lastday = moment(lastday);
lastday = lastday.format('Do MMMM YYYY');
console.log(firstday);
console.log(lastday);

      //index 0 
      if (perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[0]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Monday'){
         monday = perWeek[0]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[0]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[0]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[0]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Friday'){
         friday = perWeek[0]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([0]) && perWeek[0]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[0]['Deal']['total'];
      }else{
        
      };

      //index 1 
      if (perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[1]['Deal']['total'];
      
      }else if(perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Monday'){
         monday = perWeek[1]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[1]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[1]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[1]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Friday'){
         friday = perWeek[1]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([1]) && perWeek[1]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[1]['Deal']['total'];
      }else{
        
      };

      //index 2
      if (perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[2]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Monday'){
         monday = perWeek[2]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[2]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[2]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[2]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Friday'){
         friday = perWeek[2]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([2]) && perWeek[2]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[2]['Deal']['total'];
      }else{
        
      };


      //index 3 
      if (perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[3]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Monday'){
         monday = perWeek[3]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[3]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[3]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[3]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Friday'){
         friday = perWeek[3]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([3]) && perWeek[3]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[3]['Deal']['total'];
      }else{
        
      };


      //index 4 
      if (perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[4]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Monday'){
         monday = perWeek[4]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[4]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[4]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[4]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Friday'){
         friday = perWeek[4]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([4]) && perWeek[4]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[4]['Deal']['total'];
      }else{
        
      };

      //index 5
      if (perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[5]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Monday'){
         monday = perWeek[5]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[5]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[5]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[5]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Friday'){
         friday = perWeek[5]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([5]) && perWeek[5]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[5]['Deal']['total'];
      }else{
        
      };

      //index 6 
      if (perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Sunday') {
         sunday = perWeek[6]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Monday'){
         monday = perWeek[6]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Tuesday'){
         tuesday = perWeek[6]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Wednesday'){
         wednesday = perWeek[6]['Deal']['total'];
      }else if (perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Thursday') {
         thursday = perWeek[6]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Friday'){
         friday = perWeek[6]['Deal']['total'];
      }else if(perWeek.hasOwnProperty([6]) && perWeek[6]['Deal']['day'] == 'Saturday'){
         saturday = perWeek[6]['Deal']['total'];
      }else{
        
      };

    
      // var monday = checkAvailableDay(perWeek[0]['Deal']['day']);
      // console.log(perWeek[6]['Deal']['day']);
      // console.log(sunday);
        var perWeekData = google.visualization.arrayToDataTable([
          ['Day', 'Sales'],
          ['Sunday', parseFloat(sunday)],
          ['Monday', parseFloat(monday)],
          ['Tuesday ', parseFloat(tuesday)],
          ['Wednesday', parseFloat(wednesday)],
          ['Thursday', parseFloat(thursday)],
          ['Friday', parseFloat(friday)],
          ['Saturday', parseFloat(saturday)]
        ]);

        var options = {
          title: 'This week from'+' '+firstday+' - '+' '+lastday,
        vAxis: {title: "Value (RM)", titleTextStyle: {italic: false}},
          //curveType: 'function',
          hAxis: {
                format: "HH:mm"
                //format: "HH:mm:ss"
                //format:'MMM d, y'
            },
          legend: { position: 'bottom' }
        };

        var perWeekchart = new google.visualization.LineChart(document.getElementById('week_chart'));

        perWeekchart.draw(perWeekData, options);
      }

      function drawChart () {
        // body...
        weekChart();
        meterChart();
        lineChart();
      }
    </script>

  <div class="page-header">
    <h1>
      <?php echo __('Sales Performance');?>
    </h1>
  </div>

<div class="row">
<?php foreach($dealStages as $key => $stages): ?>

    <div class="col-sm-4">
        <div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
            <?php echo $stages['DealStage']['name'];?>
              
            </h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
          <?php if (isset($stages['Deal'][0]) && $stages['Deal'][0]['created_by'] == $userDetails['id']):?>
                  <h1 class="center"><?php echo sizeof($stages['Deal']);?></h1>
              <?php else:?>
                 <h1 class="center"><?php echo '0';?></h1>
              <?php endif;?>

            </div>
          </div>
        </div>
      </div>
      
  <?php endforeach;?>

  <div class="col-sm-12">
        <div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
            <?php echo __('Daily Sales Chart');?>
              </h4>
          </div>
           <div class="widget-body">
            <div class="widget-main no-padding">
 <div id="week_chart"></div>
            </div>
          </div>
        </div>
      </div>

 <div class="col-sm-6">
        <div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
            <?php echo __('Pipeline Chart');?>
              </h4>
          </div>
           <div class="widget-body">
            <div class="widget-main no-padding">
 <div id="chart_div"></div>
            </div>
          </div>
        </div>
      </div>

       <div class="col-sm-6">
        <div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
            <?php echo __('Lead & Invoiced Pipeline ');?>
              </h4>
          </div>
           <div class="widget-body">
            <div class="widget-main no-padding">
            <div class="center">
          <div id="meter_chart" class="center-block" style="align-center"></div>
        </div>
            </div>
          </div>
        </div>
      </div>
</div>