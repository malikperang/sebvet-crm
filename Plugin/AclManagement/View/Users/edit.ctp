<div class="page-header">
    <h1 style="margin-left:3px;">
        <?php echo __('Edit User Profile');?>
    </h1>

</div>
<div class="row" style="margin-left:-13px">
    <div class="col-xs-12">
        <div>
            <div id="user-profile-1" class="user-profile row">
                <div class="col-xs-12 col-sm-12 col-lg-2">
                    <div>
                        <span class="profile-picture">
                        <?php if(empty($user['User']['profile_picture'])):?>

                        <?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'editable img-responsive','height'=>300,'width'=>240,'id'=>'prof')); ?>
                        <?php else:echo $this->Html->image('uploads'.DS.$user['User']['profile_picture'],array('fullBase'=>true,'class'=>'editable img-responsive','id'=>'prof','height'=>300,'width'=>240,));endif;?>
                        </span>

                        <div class="space-4"></div>
                    </div>
                </div>

                    <div class="col-xs-12 col-sm-9">
                        <div class="center">
                        </div>
                        <?php echo $this->Form->create('User', array('role' => 'form','type'=>'file','action'=>'edit'));?>


                         <div class="profile-user-info profile-user-info-striped">
                            <div class="profile-info-row">
                                <div class="profile-info-name"> <?php echo __('Name');?> </div>
                                <div class="profile-info-value">
                                    <?php echo $this->Form->input('name', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                </div>
                            </div>

                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Job Position');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('job_position', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Phone Number');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('phone', array('div'=>false,
                                            'error' => array('attributes' => array('style' => 'display:none')),
                                            'label'=>false, 'class'=>'form-control','type'=>'number'));?>
                                    </div>
                                </div>


                           
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Email');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('email', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                 <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Street Address');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('street_address', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Postcode');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('postcode', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('City');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('city', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>



                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('State');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('state', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                

                                  <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Country');?> </div>
                                    <div class="profile-info-value">
                                         <?php echo $this->Form->input('country', array('div'=>false,
'error' => array('attributes' => array('style' => 'display:none')),
'label'=>false, 'class'=>'form-control'));?>
                                    </div>
                                </div>

                                 

                                 <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Profile Picture');?> </div>
                                    <div class="profile-info-value">
                                  <?php echo $this->Form->input('profile_picture', array('div'=>false,
                                'error' => array('attributes' => array('style' => 'display:none')),
                                'label'=>false, 'class'=>'form-control','type'=>'file','onchange'=>"readURL(this);"));?>
                                </div>
                                </div>

                                 <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Password');?> </div>

                                    <div class="profile-info-value">
                                       <?php echo $this->Form->input('password', array('div'=>false,
                                'error' => array('attributes' => array('style' => 'display:none')),
                                'label'=>false, 'class'=>'form-control'));?>
                                        <p class="help-block">Password must be at least 8 character length</p>
                                    </div>

                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Confirm Password');?> </div>

                                    <div class="profile-info-value">
                                       <?php echo $this->Form->input('password2', array('div'=>false, 'type'=>'password',
    'error' => array('attributes' => array('style' => 'display:none')),
    'label'=>false, 'class'=>'form-control'));?>
                                      
                                    </div>
                                </div>


                               <?php 
                               //only admin can view below input
                               if($userDetails['group_id'] == 1):
                               ?>    
                                 <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('User Group');?> </div>

                                <div class="profile-info-value">
                                       <?php  echo $this->Form->input('user_group_id', array('div'=>false,
      'label'=>false, 'class'=>'form-control'));?>
                                          <p class="help-block"><?php echo __('');?></p>
                                    </div>
                                </div>
                                  
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Access Level Group');?> </div>

                                <div class="profile-info-value">
                                       <?php  echo $this->Form->input('group_id', array('div'=>false,
      'label'=>false, 'class'=>'form-control'));?>
                                          <p class="help-block"><?php echo __('<strong>Admin</strong> need to assign a user to available access level group. By default,there are 2 types of <strong>Access Level Group </strong> which is <strong>Admin</strong> & <strong>Members</strong>. Different between the 2 permission is <strong> Admin</strong> can access all section in this system while <strong>Members</strong> are probihited on some section');?></p>
                                    </div>
                                </div>
                                 
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> <?php echo __('Status');?> </div>

                                    <div class="profile-info-value">
                                       <?php echo $this->Form->input('status', array('div'=>false,
                                   'label'=>false, 'class'=>'form-control'));?>
                                <p class="help-block"><?php echo __('Tick to enable this user.Untick to disable this user.');?></p>
                                    </div>
                                </div>
                            <?php endif;?>
                        </div>
                                        
                        <div class="space-10"></div>
                        <div class="text-center">
                         <?php echo $this->Form->button(__('<i class="fa fa-floppy-o"></i>  Update Profile'), array('class'=>'btn btn-default', 'div'=>false,'escape'=>false,'type'=>'submit'));?>
                         </div>
                                    </div>           
                                </div>                                 
                            
                                <div class="space-20"></div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#prof').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>