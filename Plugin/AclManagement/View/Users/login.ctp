 <style type="text/css">
 .login-widget{
  border:1px solid #ddd;
 }
 </style>
 <div class="space-30"></div>
    <div class="position-relative">
    <div class="panel panel-default">
    <div class="panel-body">
          <h4 class="header blue lighter bigger text-center">
            
               <?php echo __('Login'); ?>
          </h4>
          <div class="center">
          <?php 
            echo $this->Html->image('new-logo.jpg',array('fullBase'=>true,'class'=>'img-responsive login-logo center-block'));?>   
            </div>
          
          <h4 class="app-name text-center">Sales Response System</h4>
           
            <div class="space-6"></div>
                <?php
                echo $this->Form->create('User', array('action' => 'login'));
                ?>
          <fieldset>
                <label class="block clearfix">
                    <span class="block input-icon input-icon-right">
            <?php echo $this->Form->input('email', array(
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));?>
                    <i class="ace-icon fa fa-user"></i>
                </span>
            </label>
            <label class="block clearfix">
                <span class="block input-icon input-icon-right">
            <?php echo $this->Form->input('password', array(
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
            ?>
            <i class="ace-icon fa fa-lock"></i>
                </span>
            </label>
           
            <!-- <a href="<?php echo $this->Html->url('/users/forgot_password');?>">Forgot password?</a> -->
             <div class="space-5"></div>

                <?php echo $this->Form->button(__('Sign in'), array('class'=>'btn btn-primary pull-right','escape'=>false, 'div'=>false));?>

        </fieldset>
         <?php echo $this->Form->end();?>

   <!--  <div class="space-6">  </div>
        <?php echo $this->Form->create('User',array('action'=>'socialSign'));?>
         <?php 
            echo $this->Form->button('<i class="fa fa-google-plus"></i> Sign in with Google',array('class'=>'btn btn-block btn-social btn-google-plus','onclick'=>'_gaq.push(["_trackEvent", "btn-social", "click", "btn-google-plus"]);','escape'=>false,'name'=>'google','type'=>'submit'));

         ?>
        <?php echo $this->Form->end();?>
     -->
     <p class="black">&copy;Sebvet PLT (LLP0002907-LGN)</p>

  
    </div>
    </div>
    
