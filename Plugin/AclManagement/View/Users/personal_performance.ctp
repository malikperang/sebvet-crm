<?php 
// debug($this->params);

?>

<div class="page-header">
    <h1 class="page-header-override">
      <?php echo __('Personal Sales Performance');?>
    </h1>
  </div>


<?php //if user is admin and passing an id of user
  //display the user performance details
if($userDetails['group_id'] == 1 && isset($this->params['pass'][0])):
?>
<div class="row">
<?php foreach($dealStages as $key => $stages): ?>
<div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <?php echo $stages['DealStage']['name'];?>
          </div>

          <div class="panel-body">
              <?php if (isset($stages['Deal'][0]) && $stages['Deal'][0]['created_by'] == $this->params['pass'][0]):?>
                  <h1 class="center"><?php echo sizeof($stages['Deal']);?></h1>
              <?php else:?>
                 <h1 class="center"><?php echo '0';?></h1>
              <?php endif;?>

          </div>
        </div>
      </div>
    <?php endforeach;?>
      </div>
    <?php //debug($users);?>
    <div class="row">
      <div id="user-profile-1" class="user-profile row">
              <div class="col-xs-12 col-sm-3">
                  <div>
                      <span class="profile-picture">
                      <?php if(empty($users['User']['profile_picture'])):?>

                      <?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'editable img-responsive','height'=>300,'width'=>240)); ?>
                      <?php else:echo $this->Html->image('uploads'.DS.$users['User']['profile_picture'],array('fullBase'=>true,'class'=>'editable img-responsive'));endif;?>
                      </span>

                </div>
                </div>


                  <div class="col-xs-12 col-sm-9">
  
                      <div class="profile-user-info profile-user-info-striped">

                       <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Name');?> </div>
                              <div class="profile-info-value">
                                  <?php echo h($users['User']['name']);?>
                              </div>
                          </div>



                       <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Job Position');?> </div>
                              <div class="profile-info-value">
                                  <?php echo h($users['User']['job_position']);?>
                              </div>
                          </div>

                          <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Email');?> </div>
                              <div class="profile-info-value">
                                  <?php echo h($users['User']['email']);?>
                              </div>
                          </div>

                          <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Street Address');?> </div>

                              <div class="profile-info-value">
                                  <?php echo h($users['User']['street_address']);?>
                                  <!-- <span class="editable editable-click" id="signup">2010/06/20</span> -->
                              </div>
                          </div>


                           <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('City');?>  </div>

                              <div class="profile-info-value">
                                  <?php echo h($users['User']['city']);?>
                              </div>
                          </div>

                          <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('State');?>  </div>

                              <div class="profile-info-value">
                                  <?php echo h($users['User']['state']);?>
                              </div>
                          </div>

                          <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Country');?> </div>

                              <div class="profile-info-value">
                              <?php echo h($users['User']['country']);?>
                                  <!-- <span class="editable editable-click" id="login">3 hours ago</span> -->
                              </div>
                          </div>

                          <div class="profile-info-row">
                              <div class="profile-info-name"> <?php echo __('Phone Number');?>  </div>

                              <div class="profile-info-value">
                                  <?php echo h($users['User']['phone']);?>
                              </div>
                          </div>
                          </div>
                      </div>
<?php 
  //if user is admin and not passing an user id
  //display the user index
  elseif($userDetails['group_id'] ==  1 && !isset($this->params['pass'][0])):?>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
    <table class="table table-bordered table-hover performance">
    <tr>
        <th class="header col-lg-4"><?php echo $this->Paginator->sort('name');?></th>    
        <th class="header col-lg-3"><?php echo $this->Paginator->sort('email');?></th>
   <!--      <th class="header col-lg-3"><?php echo $this->Paginator->sort('totaldeal','Total Deal');?></th>
        <th class="header col-lg-3"><?php echo $this->Paginator->sort('pipelineworth','Pipeline Worth');?></th> -->
        <th class="header center col-lg-2" style="width:20px"><?php echo __('Actions');?></th>
    </tr>
    <?php
    foreach ($users as $user): ?>
    <tr>  
            <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
            <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
       <!--      <td></td>
            <td></td> -->
            <td class="center">
                  <?php echo $this->Html->link(__('View'), array('plugin'=>'acl_management','controller'=>'users','action' => 'personalPerformance', $user['User']['id']), array('class'=>'btn btn-default btn-sm')); ?>

            </td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
     <?php if(empty($users)):?>
     <ul class="pagination pagination pagination-right">
       <li><?php
            echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
       ?>
       </li><li><?php
            echo $this->Paginator->numbers(array('separator' => ''));
       ?>
       </li><li><?php
            echo $this->Paginator->next(__('Next') . ' >>', array(), null, array('class' => 'next disabled'));
       ?>
       </li>    
     </ul>
     <?php endif;?>
</div>
</div>
<?php else:?>

  <div class="row">
<?php foreach($dealStages as $key => $stages): ?>

    <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <?php echo $stages['DealStage']['name'];?>
          </div>

          <div class="panel-body">
              <?php if($userDetails['group_id'] == 1):?>
                 <h1 class="center"><?php echo sizeof($stages['Deal']);?></h1>
              <?php else:?>
              <?php if (isset($stages['Deal'][0]) && $stages['Deal'][0]['created_by'] == $userDetails['id']):?>
                  <h1 class="center"><?php echo sizeof($stages['Deal']);?></h1>
              <?php else:?>
                 <h1 class="center"><?php echo '0';?></h1>
              <?php endif;?>
               <?php endif;?>
          </div>
        </div>
      </div>
      
  <?php endforeach;?>
   <?php endif;?>

  </div>
