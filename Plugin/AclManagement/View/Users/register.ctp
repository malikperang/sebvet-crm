<div class="center">
        <h1>
        <?php 
            //check if user didn't upload login logo
            if(empty($sysSetting['SystemSetting']['login_logo'])):?>
        <?php echo $this->Html->image('sebvet-logo.jpg',array('fullBase'=>true,'class'=>'img-responsive login-logo'));?>
        <?php else:
            echo $this->Html->image('uploads'.DS.$sysSetting['SystemSetting']['login_logo'],array('fullBase'=>true,'class'=>'img-responsive login-logo'));?>
        <?php endif; ?>
           
        </h1>
         <h4 class="black" id="id-text2">xRM Application</h4>
        
     </div>

     
    <div class="space-6"></div>
    <div class="position-relative">
    <div id="login-box" class="login-box visible widget-box no-border">
    <div class="widget-body">
        <div class="widget-main">
            <h4 class="header blue lighter bigger text-center">
            
               <?php echo __('Register New Account'); ?>
            </h4>
              <div class="space-6"></div>
    <?php echo $this->Form->create('User', array('action'=>'register'));?>
	<fieldset>
	<?php
            echo $this->Form->input('name', array('div'=>'control-group',
                'before'=>'<label class="control-label">'.__('Name').'</label><div class="controls">', 
                'after'=>$this->Form->error('name', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
               echo $this->Form->input('company', array('div'=>'control-group',
                'before'=>'<label class="control-label">'.__('Company').'</label><div class="controls">', 
                'after'=>$this->Form->error('company', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
            echo $this->Form->input('email', array('div'=>'control-group', 
                'before'=>'<label class="control-label">'.__('Email').'</label><div class="controls">',
                'after'=>$this->Form->error('email', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
            echo $this->Form->input('password', array('div'=>'control-group', 
                'before'=>'<label class="control-label">'.__('Password').'</label><div class="controls">',
                'after'=>$this->Form->error('password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
            echo $this->Form->input('password2', array('div'=>'control-group', 'type'=>'password', 
                'before'=>'<label class="control-label">'.__('Confirm Password').'</label><div class="controls">',
                'after'=>$this->Form->error('password2', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                'error' => array('attributes' => array('style' => 'display:none')),
                'label'=>false, 'class'=>'form-control'));
	?>
        <div class="form-actions">
            <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn btn-primary', 'div'=>false));?>
            <?php echo $this->Form->reset(__('Cancel'), array('class'=>'btn', 'div'=>false));?>
        </div>
	</fieldset>
    </div>
    </div>
    </div>
    </div>

<?php echo $this->Form->end();?>
</div>