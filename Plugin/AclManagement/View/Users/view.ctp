<div class="page-header">
<h1>
    <?php echo __('User Profile');?>
</h1>
</div>
<div class="row">
<div class="col-xs-12  setting-row">
    <div>
        <div id="user-profile-1" class="user-profile row">
            <div class="col-xs-12 col-sm-3 center">
                <div>
                    <span class="profile-picture">
                    <?php if(empty($user['User']['profile_picture'])):?>

                    <?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'editable img-responsive','height'=>300,'width'=>240)); ?>
                    <?php else:echo $this->Html->image('uploads'.DS.$user['User']['profile_picture'],array('fullBase'=>true,'class'=>'editable img-responsive'));endif;?>
                    </span>

                    <!-- /section:pages/profile.picture -->
                    <div class="space-4"></div>

                   
                </div>

                <div class="space-6"></div>

                <!-- #section:pages/profile.contact -->
                <div class="profile-contact-info">
                    

                    <div class="space-6"></div>

                </div>

            
            </div>

            <div class="col-xs-12 col-sm-9">
                <div class="center">
                </div>
                <div class="profile-user-info profile-user-info-striped">

                 <div class="profile-info-row">
                        <div class="profile-info-name"> <?php echo __('Name');?> </div>
                        <div class="profile-info-value">
                            <?php echo h($user['User']['name']);?>
                        </div>
                    </div>


                 <div class="profile-info-row">
                        <div class="profile-info-name"> <?php echo __('Job Position');?> </div>
                        <div class="profile-info-value">
                            <?php echo h($user['User']['job_position']);?>
                        </div>
                    </div>


              <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('Phone Number');?> </div>
                    <div class="profile-info-value">
                        <?php echo h($user['User']['phone']);?>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('Email');?> </div>
                    <div class="profile-info-value">
                        <?php echo h($user['User']['email']);?>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('Street Address');?> </div>

                    <div class="profile-info-value">
                        <?php echo h($user['User']['street_address']);?>
                        <!-- <span class="editable editable-click" id="signup">2010/06/20</span> -->
                    </div>
                </div>


                 <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('City');?>  </div>

                    <div class="profile-info-value">
                        <?php echo h($user['User']['city']);?>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('State');?>  </div>

                    <div class="profile-info-value">
                        <?php echo h($user['User']['state']);?>
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('Country');?> </div>

                    <div class="profile-info-value">
                    <?php echo h($user['User']['country']);?>
                        <!-- <span class="editable editable-click" id="login">3 hours ago</span> -->
                    </div>
                </div>

                <div class="profile-info-row">
                    <div class="profile-info-name"> <?php echo __('Phone Number');?>  </div>

                    <div class="profile-info-value">
                        <?php echo h($user['User']['phone']);?>
                    </div>
                </div>
                    
                </div>

             
                <div class="space-20"></div>

                <div class="text-center">
                <?php echo $this->Html->link('<i class="fa fa-pencil-square-o"></i> Update Profile',array('action'=>'edit',$user['User']['id']),array('escape'=>false,'class'=>'btn btn-default'));?>
                </div>
            </div>
        </div>
    </div> 

    

    <!-- PAGE CONTENT ENDS -->
</div>
</div>