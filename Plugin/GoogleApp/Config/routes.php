<?php
Router::connect('/google/connect/index', array('plugin' => 'google_app', 'controller' => 'connect', 'action' => 'index'));
Router::connect('/google/connect/oauthCallback', array('plugin' => 'google_app', 'controller' => 'connect', 'action' => 'oauthCallback'));
Router::connect('/google/connect/processGoogleSignin', array('plugin' => 'google_app', 'controller' => 'connect', 'action' => 'processGoogleSignin'));
Router::connect('/google/connect/logoutGoogle', array('plugin' => 'google_app', 'controller' => 'connect', 'action' => 'logoutGoogle'));

Router::connect('/google/gmail', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'index'));
Router::connect('/google/gmail/inbox', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'index'));
Router::connect('/google/gdrive/index', array('plugin' => 'google_app', 'controller' => 'gdrive', 'action' => 'index'));
Router::connect('/google/gmail/view/*', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'view'));
Router::connect('/google/gmail/getInbox', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'getInbox'));
Router::connect('/google/gmail/attachments/*', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'attachments'));
Router::connect('/google/gmail/sendMail', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'sendMail'));
Router::connect('/google/gmail/sentmail', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'viewSendMail'));
Router::connect('/google/gmail/getSentMail', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'getSentMail'));
// Router::connect('/google/gmail/test', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'test'));
Router::connect('/google/gmail/reply/*', array('plugin' => 'google_app', 'controller' => 'gmail', 'action' => 'replyMail'));