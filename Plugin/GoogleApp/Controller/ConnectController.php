<?php

App::uses('GoogleAppApp', 'Controller');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
// require_once(APP . 'Vendor/autoload.php');
class ConnectController extends GoogleAppAppController {



    protected $scope = 'https://mail.google.com https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/plus.login';
    protected $state = 'generate_a_unique_state_value';
    protected $redirectUri = '';
    protected $responseType = 'code';
    protected $accessType = 'offline';

    public function beforeFilter(){

    }


    public function index(){



    }
    public function oauthCallback(){
        $this->autoRender = false;
        $this->Session->write('User.email',$this->request->data['email']);
        $this->redirectUri=Router::url(['plugin'=>'google_app','controller'=>'connect', 'action'=>'processGoogleSignin'],true);
        $data=[
            'scope'=>$this->scope,
            'state'=>$this->state,
            'redirect_uri'=>$this->redirectUri,
            'response_type'=>$this->responseType,
            'client_id'=>$this->client_id,
            'access_type'=>$this->accessType,

        ];

        if(!$this->Session->check('Google.token')){
            $this->redirect('https://accounts.google.com/o/oauth2/auth?'.http_build_query($data));
        }
    }

    public function processGoogleSignin(){
        $this->autoRender = false;



        $client = new Google_Client();
        $client->setClientId($this->client_id);
        $client->setClientSecret($this->client_secret);

        $setRedirectUri = Router::url(['plugin'=>'google_app','controller'=>'connect', 'action'=>'processGoogleSignin'],true);
        $client->setRedirectUri($setRedirectUri);
        $client->authenticate($_GET['code']);

        if(!$this->Session->check('Google.token')){
            $this->Session->write('Google.token', json_decode($client->getAccessToken(),true));
            $this->Session->write('Google.token.refresh_token', $client->getRefreshToken());
        }

        $this->redirect(['plugin'=>'google_app','controller'=>'connect', 'action'=>'index']);
    }
    public function logoutGoogle(){
        $this->autoRender = false;
        $this->Session->delete('Google.token');
        $this->redirect(['plugin'=>'google_app','controller'=>'connect','action'=>'index']);
    }
}
