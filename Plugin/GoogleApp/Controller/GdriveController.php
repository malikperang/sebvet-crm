<?php

App::uses('GoogleAppApp', 'Controller');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
// require_once(APP . 'Vendor/autoload.php');
class GdriveController extends GoogleAppAppController {

    public $layout='gdrive';

    public function beforeFilter(){
        $client = new Google_Client();
        $client->setClientId($this->client_id);
        $client->setClientSecret($this->client_secret);
        $client->setAccessToken(json_encode($this->Session->read('Google.token')));
        if ($client->isAccessTokenExpired() == TRUE) {
            $this->Session->delete('Google.token');
            $this->redirect(['plugin'=>'google_app','controller'=>'connect', 'action'=>'index']);
        }
        $this->client=$client;
    }


    public function index(){

        $drive_service = new Google_Service_Drive($this->client);
        //$files_list = $drive_service->files->listFiles([])->getItems();
        $q='';
        if(isset($this->request['url']['parent'])){
            $q.="'{$this->request['url']['parent']}' in parents";
        }else{
            $q.="'root' in parents";
        }

        $files_list = $drive_service->files->listFiles(['q'=>$q])->getItems();
        $this->set('files_list', $files_list);
    }
}
