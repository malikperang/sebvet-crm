<?php
set_time_limit(0);
ini_set('max_execution_time', 7000);
App::uses('GoogleAppApp', 'Controller');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/autoload.php');
require_once(APP . 'Vendor' . DS. 'google-api-php-client/src/Google/Service/Gmail.php');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeEmail', 'Network/Email');
// require_once(APP . 'vendor/autoload.php');
class GmailController extends GoogleAppAppController {
    public $htmlmsg; 
    public $plainmsg; 
    public $charset; 
    public $attachments;

    public $uses = array('GoogleUser','Gmail','GmailsAttachment');

    public $layout = 'gmail';



    public function beforeFilter(){
        // $client = new Google_Client();
        // $client->setClientId($this->client_id);
        // $client->setClientSecret($this->client_secret);
        // $client->setAccessToken(json_encode($this->Session->read('Google.token')));
        // if ($client->isAccessTokenExpired() == TRUE) {
        //     $this->Session->delete('Google.token');
        //     $this->redirect(['plugin'=>'google_app','controller'=>'connect', 'action'=>'index']);
        // }
        // $this->client=$client;
    }


    public function getInbox(){
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        // if ($this->request->is('post')) {
            $userDetails = $this->Session->read('Auth.User');
            //google api credentials
            $client_id = Configure::read('Google.clientId');
            $client_secret = Configure::read('Google.clientSecret');

            //create new google object
            $client = new Google_Client();
            $client->setClientId($client_id);
            $client->setClientSecret($client_secret);


            $googleUser = $this->GoogleUser->find('first',array(
                'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
            ));

            if (!empty($googleUser)) {

                $client->setAccessToken($googleUser['GoogleUser']['access_token']);
                $access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
                $refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;

                if (!empty($googleUser['GoogleUser']['google_email'])) {

                    if ($client->isAccessTokenExpired() == TRUE) {
                        $client->refreshtoken($refreshToken);
                        $newtoken= $client->getAccessToken();
                        $this->GoogleUser->id = $googleUser['GoogleUser']['id'];
                        $data = array(
                            'access_token'=>$newtoken,
                            );
                        $this->GoogleUser->save($data);
                        return json_encode('token expired');
                    }else{

                        $service = new Google_Service_Gmail($client);
                        $optParams['labelIds'] = 'INBOX'; // Only show messages in Inbox
                        //load everything messages and other info
                        $messages       = $service->users_messages->listUsersMessages($googleUser['GoogleUser']['google_email'],$optParams);

                        $data = $service->users_labels->get($googleUser['GoogleUser']['google_email'], 'INBOX');
                        // debug($data);
                        $all = $data->messagesTotal;
                        //get list of messages
                        $lists          = $messages->getMessages();
                        
                      
                        foreach ($lists as $item) {
                            // debug($item);
                       
                            $text = "";
                            $html = "";
                            $messageId = $item->getId();
                            $threadId = $item->getThreadId();
                            
                            $optParamsGet = array();
                            $optParamsGet['format'] = 'full'; // Display message in payload
                            //load details of a single message
                            $message = $service->users_messages->get($googleUser['GoogleUser']['google_email'], $messageId, $optParamsGet);
                            // debug($message);
                            //get headers - subject, sender etc
                            $headers = $message->getPayload()->getHeaders();
                            $snippet = $message->getSnippet();
                            $labels = $message->getlabelIds();
                            // debug($labels);
                            foreach($headers as $single) {
                                if ($single->getName() == 'Subject') {
                                    $message_subject = $single->getValue();
                                } else if ($single->getName() == 'Date') {
                                    $message_date = $single->getValue();
                                    @$message_date = date('M jS Y h:i A', strtotime($message_date));
                                } else if ($single->getName() == 'From') {
                                    $message_sender = $single->getValue();
                                    $message_sender = str_replace('"', '', $message_sender);
                                }
                            }

                            //process parts to find different content, attachments etc
                            $parts = $message->getPayload()->getParts();
                            if (count($parts)>0) {
                                $attachment_ids = array();
                                $attachment_vals = array();
                                $attachments = array();
                                //first load attachments since we need to replace later resources in html with links to attachments
                                foreach ($parts as $part) {
                                    if (isset($part['body']->attachmentId)) {
                                         $headers = $part->getHeaders();
                                         foreach ($headers as $single) {
                                            if ($single->getName()=='X-Attachment-Id' ||
                                                $single->getName()=='Content-ID') {
                                                
                                                $attachment_ids[] = "cid:".trim($single->getValue(),'<>');
                                           

                                                $this->GmailsAttachment->create();
                                                //save gmail attachments to database
                                                $data = array(
                                                    'messageId'=>$messageId,
                                                    'attachmentId'=>$part['body']->attachmentId,
                                                    'mimeType'=>$part['mimeType'],
                                                    'name'=>$part['filename']
                                                    );
                                                $this->GmailsAttachment->save($data);
                                            

                                                $attachment_vals[] =  $this->webroot.'google/gmail/attachments/'.$messageId.'/'.$part['body']->attachmentId;
                                            }
                                         }
                                      
                                    } 
                                  
                                }
                                //when attachments loaded we can process content
                                foreach ($parts as $part) {
                                    //if complicated includes attachments
                                    if (strstr($part['mimeType'], 'multipart/alternative')) {
                                        $subp = $part->getParts();
                                        foreach ($subp as $p) {
                                            if (strstr($p['mimeType'],'text/plain')) {
                                                $text = $this->urlsafe_b64decode($p['body']->data);
                                            } 
                                            if (strstr($p['mimeType'], 'text/html')) {
                                                $html = $this->urlsafe_b64decode($p['body']->data);
                                                $html = str_replace($attachment_ids, $attachment_vals, $html);
                                            }           
                                        }
                                    }
                                    //content text/plain
                                    if (strstr($part['mimeType'],'text/plain')) {
                                        $text = $this->urlsafe_b64decode($part['body']->data);
                                    } 
                                    //content text/html
                                    if (strstr($part['mimeType'], 'text/html')) {
                                        $html = $this->urlsafe_b64decode($part['body']->data);
                                    }
                                } 
                            } else {
                                //if there was no parts array using getPayload to load content
                                //and again process the content type
                                $part = $message->getPayload();
                                if (strstr($part['mimeType'],'text/plain')) {
                                    $text = $this->urlsafe_b64decode($part['body']->data);
                                } 
                                if (strstr($part['mimeType'], 'text/html')) {
                                    $html = $this->urlsafe_b64decode($part['body']->data);
                                }       
                            }
                            $messageBody = '';
                            if (!empty($text)) {
                                $messageBody = $text;
                            }
                            if (!empty($html)) {
                                $messageBody = $html;
                            }
                            
                            //check duplicate message
                            $duplicateMessage = $this->Gmail->find('all',array(
                                'conditions'=>array('Gmail.messageId'=>$messageId)
                                ));

                            //if empty duplicate message
                            if (empty($duplicateMessage)) {
                                
                                $this->Gmail->create();
                                $data = array(
                                    'messageId'=>$messageId,
                                    'messageSnippet'=>$snippet,
                                    'messageLabel'=> 'INBOX',
                                    'messageSubject'=>$message_subject,
                                    'messageDate'=>$message_date,
                                    'messageSender'=>$message_sender,
                                    'messageBody'=> $messageBody,
                                    );
                                $this->Gmail->save($data);

                                //update Gmails Id for attachment
                                $gmailId = $this->Gmail->getLastInsertId();
                                $gmailDetails = $this->Gmail->findById($gmailId);

                            
                                $gAttachments = $this->GmailsAttachment->find('all',array(
                                    'conditions'=>array('GmailsAttachment.messageId'=>$gmailDetails['Gmail']['messageId'])
                                    ));
                             
                                foreach ($gAttachments as $attachments) {
                                   
                                    //update gmail attachments with correct gmail id
                                    $this->GmailsAttachment->id = $attachments['GmailsAttachment']['id'];
                                    $this->GmailsAttachment->saveField('gmail_id',$gmailId);
                                }

                            // return json_encode('success');
                            }
                        // return json_encode('success');
                        } 
                        return true;
                     // return json_encode('success');
                    }
                }

            }
        // }
   
    }

    public function getSentMail(){
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
    // if ($this->request->is('post')) {
        $userDetails = $this->Session->read('Auth.User');
        //google api credentials
        $client_id = Configure::read('Google.clientId');
        $client_secret = Configure::read('Google.clientSecret');

        //create new google object
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);


        $googleUser = $this->GoogleUser->find('first',array(
            'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
        ));

        if (!empty($googleUser)) {

            $client->setAccessToken($googleUser['GoogleUser']['access_token']);
            $access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
            $refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;

            if (!empty($googleUser['GoogleUser']['google_email'])) {

                if ($client->isAccessTokenExpired() == TRUE) {
                    $client->refreshtoken($refreshToken);
                    $newtoken= $client->getAccessToken();
                    $this->GoogleUser->id = $googleUser['GoogleUser']['id'];
                    $data = array(
                        'access_token'=>$newtoken,
                        );
                    $this->GoogleUser->save($data);
                    return json_encode('token expired');
                }else{

                    $service = new Google_Service_Gmail($client);
                    $optParams['labelIds'] = 'SENT'; // Only show messages in SENT
                    //load everything messages and other info
                    $messages       = $service->users_messages->listUsersMessages($googleUser['GoogleUser']['google_email'],$optParams);

                    $data = $service->users_labels->get($googleUser['GoogleUser']['google_email'], 'SENT');
                    // debug($data);
                    $all = $data->messagesTotal;
                    //get list of messages
                    $lists          = $messages->getMessages();
                    
                  
                    foreach ($lists as $item) {
                        // debug($item);
                   
                        $text = "";
                        $html = "";
                        $messageId = $item->getId();
                        $threadId = $item->getThreadId();
                        
                        $optParamsGet = array();
                        $optParamsGet['format'] = 'full'; // Display message in payload
                        //load details of a single message
                        $message = $service->users_messages->get($googleUser['GoogleUser']['google_email'], $messageId, $optParamsGet);
                        // debug($message);
                        //get headers - subject, sender etc
                        $headers = $message->getPayload()->getHeaders();
                        $snippet = $message->getSnippet();
                        $labels = $message->getlabelIds();
                        // debug($labels);
                        foreach($headers as $single) {
                            if ($single->getName() == 'Subject') {
                                $message_subject = $single->getValue();
                            } else if ($single->getName() == 'Date') {
                                $message_date = $single->getValue();
                                @$message_date = date('M jS Y h:i A', strtotime($message_date));
                            } else if ($single->getName() == 'From') {
                                $message_sender = $single->getValue();
                                $message_sender = str_replace('"', '', $message_sender);
                            }
                        }

                        //process parts to find different content, attachments etc
                        $parts = $message->getPayload()->getParts();
                        if (count($parts)>0) {
                            $attachment_ids = array();
                            $attachment_vals = array();
                            $attachments = array();
                            //first load attachments since we need to replace later resources in html with links to attachments
                            foreach ($parts as $part) {
                                if (isset($part['body']->attachmentId)) {
                                     $headers = $part->getHeaders();
                                     foreach ($headers as $single) {
                                        if ($single->getName()=='X-Attachment-Id' ||
                                            $single->getName()=='Content-ID') {
                                            
                                            $attachment_ids[] = "cid:".trim($single->getValue(),'<>');
                                       

                                            $this->GmailsAttachment->create();
                                            //save gmail attachments to database
                                            $data = array(
                                                'messageId'=>$messageId,
                                                'attachmentId'=>$part['body']->attachmentId,
                                                'mimeType'=>$part['mimeType'],
                                                'name'=>$part['filename']
                                                );
                                            $this->GmailsAttachment->save($data);
                                        

                                            $attachment_vals[] =  $this->webroot.'google/gmail/attachments/'.$messageId.'/'.$part['body']->attachmentId;
                                        }
                                     }
                                  
                                } 
                              
                            }
                            //when attachments loaded we can process content
                            foreach ($parts as $part) {
                                //if complicated includes attachments
                                if (strstr($part['mimeType'], 'multipart/alternative')) {
                                    $subp = $part->getParts();
                                    foreach ($subp as $p) {
                                        if (strstr($p['mimeType'],'text/plain')) {
                                            $text = $this->urlsafe_b64decode($p['body']->data);
                                        } 
                                        if (strstr($p['mimeType'], 'text/html')) {
                                            $html = $this->urlsafe_b64decode($p['body']->data);
                                            $html = str_replace($attachment_ids, $attachment_vals, $html);
                                        }           
                                    }
                                }
                                //content text/plain
                                if (strstr($part['mimeType'],'text/plain')) {
                                    $text = $this->urlsafe_b64decode($part['body']->data);
                                } 
                                //content text/html
                                if (strstr($part['mimeType'], 'text/html')) {
                                    $html = $this->urlsafe_b64decode($part['body']->data);
                                }
                            } 
                        } else {
                            //if there was no parts array using getPayload to load content
                            //and again process the content type
                            $part = $message->getPayload();
                            if (strstr($part['mimeType'],'text/plain')) {
                                $text = $this->urlsafe_b64decode($part['body']->data);
                            } 
                            if (strstr($part['mimeType'], 'text/html')) {
                                $html = $this->urlsafe_b64decode($part['body']->data);
                            }       
                        }
                        $messageBody = '';
                        if (!empty($text)) {
                            $messageBody = $text;
                        }
                        if (!empty($html)) {
                            $messageBody = $html;
                        }
                        
                        //check duplicate message
                        $duplicateMessage = $this->Gmail->find('all',array(
                            'conditions'=>array('Gmail.messageId'=>$messageId)
                            ));

                        //if empty duplicate message
                        if (empty($duplicateMessage)) {
                            
                            $this->Gmail->create();
                            $data = array(
                                'messageId'=>$messageId,
                                'messageSnippet'=>$snippet,
                                'messageLabel'=> 'SENT',
                                'messageSubject'=>$message_subject,
                                'messageDate'=>$message_date,
                                'messageSender'=>$message_sender,
                                'messageBody'=> $messageBody,
                                );
                            $this->Gmail->save($data);

                            //update Gmails Id for attachment
                            $gmailId = $this->Gmail->getLastInsertId();
                            $gmailDetails = $this->Gmail->findById($gmailId);

                        
                            $gAttachments = $this->GmailsAttachment->find('all',array(
                                'conditions'=>array('GmailsAttachment.messageId'=>$gmailDetails['Gmail']['messageId'])
                                ));
                         
                            foreach ($gAttachments as $attachments) {
                               
                                //update gmail attachments with correct gmail id
                                $this->GmailsAttachment->id = $attachments['GmailsAttachment']['id'];
                                $this->GmailsAttachment->saveField('gmail_id',$gmailId);
                            }

                        // return json_encode('success');
                        }
                    // return json_encode('success');
                    } 
                    return true;
                 // return json_encode('success');
                }
            }

        }
    }

    public function index(){
      $userDetails = $this->Session->read('Auth.User');
      $messages = $this->Gmail->getAllInbox();
      $gUser = $this->GoogleUser->findByUserId($userDetails['id']);
      $this->set(compact('messages','gUser'));
    }

    public function viewSendMail(){
      $userDetails = $this->Session->read('Auth.User');
      $messages = $this->Gmail->getAllSentMail();
      $gUser = $this->GoogleUser->findByUserId($userDetails['id']);
      $this->set(compact('messages','gUser'));
    }

    public function view($messageId = null){
        // $this->autoRender = false;
        $message = $this->Gmail->find('first',array(
            'conditions'=>array('Gmail.messageId'=>$messageId)
            ));
                   //google api credentials
//             $client_id = Configure::read('Google.clientId');
//             $client_secret = Configure::read('Google.clientSecret');

//             //create new google object
//             $client = new Google_Client();
//             $client->setClientId($client_id);
//             $client->setClientSecret($client_secret);

//             $service = new Google_Service_Gmail($client);
                       
//         $attach = $service->users_messages_attachments->get('me', $messageId, 'ANGjdJ_galu6A6iSov7M_1fGqs1vjfyYGKfZJoUlVXTYCF6anFZ1Tp7nkfIrKTPUgazZgGI0wuj-sswygTGDc6_xxHFNykARBmR0S-7mq-jGQ5huZ4NfUdmujC6jPMP0lCV73Tq9WzCKa4xeA4fh2ZCckvlO-Stajr8kPq6RtWM77GRWFzBFMTO_qpFChlrdA2Lythm_trM_zQLsKFNxJPekZyHbgQTZ7Oh7nNzNsps0jIsVcox9iTJ4pt07v72eJm0r7g_PG2_W6snBg_OBVF3k8P-9WGnhBkdx2nmV6g');
// // print_r($attach);
//         debug($attach);
//         exit();
        $attachments = $this->GmailsAttachment->find('all',array(
            'conditions'=>array('GmailsAttachment.messageId'=>$messageId)
            ));
        $this->set(compact('message','attachments'));
    }

    public function attachments($messageId,$attachmentId,$mimeType,$fileName){
        $this->autoRender = false;
        // debug($this->params);
        // exit;
        // $this->request->onlyAllow('ajax');
        // if ($this->request->is('post')) {
        $userDetails = $this->Session->read('Auth.User');
        //google api credentials
        $client_id = Configure::read('Google.clientId');
        $client_secret = Configure::read('Google.clientSecret');

        //create new google object
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);


        $googleUser = $this->GoogleUser->find('first',array(
            'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
        ));

        if (!empty($googleUser)) {

            $client->setAccessToken($googleUser['GoogleUser']['access_token']);
            $access_token = json_decode($googleUser['GoogleUser']['access_token'])->access_token;
            $refreshToken = json_decode($googleUser['GoogleUser']['access_token'])->refresh_token;

            if (!empty($googleUser['GoogleUser']['google_email'])) {

                if ($client->isAccessTokenExpired() == TRUE) {
                    $client->refreshtoken($refreshToken);
                    $newtoken= $client->getAccessToken();
                    $this->GoogleUser->id = $googleUser['GoogleUser']['id'];
                    $data = array(
                        'access_token'=>$newtoken,
                        );
                    $this->GoogleUser->save($data);
                    debug('yes');
                }else{
                    $mimeType = str_replace('-', '/', $mimeType);
                    // debug($mimeType);
                    // exit;
                    $service = new Google_Service_Gmail($client);
                    $attachmentData = $service->users_messages_attachments->get($googleUser['GoogleUser']['google_email'], $messageId,  $attachmentId);
             
                    //decode
                    $data = $this->urlsafe_b64decode($attachmentData['data']);
                    //output
                    // debug($data);
                    // exit;
                    if (isset($mimeType)) {
                        // $this->response->header(array(
                        //     // 'WWW-Authenticate: Negotiate',
                        //     'Content-type: '.$mimeType,
                        // ));
                        header("Content-type: ".$mimeType);
                        exit;
                    }

                    debug($fileName);
                    if (isset($filename)) {
                        //     $this->response->header(array(
                        //     // 'WWW-Authenticate: Negotiate',
                        //     'Content-disposition: attachment; filename='.html_entity_decode($fileName),
                        // ));
                        header('Content-disposition: attachment; filename="'.$fileName.'"');  
                        exit;  
                    }
                    echo $data;
                }
            }
        }

    }

 

    public function sendMail(){
        $userDetails = $this->Session->read('Auth.User');
        $senderEmail = $this->GoogleUser->findByUserId($userDetails['id']);
        if ($this->request->is('post')) {
        
            $recipients = array();
            $email = new CakeEmail('smtp');
            $email->from(array($senderEmail['GoogleUser']['google_email'] => 'Sebvet SRS'));
            $recipientEmail = explode(',', $this->request->data['Gmail']['recipient']);

            //multiple recipient
            foreach ($recipientEmail as $recipient) {
                $recipients[] =  $recipient; 
            }

            $attachments = array();
            //multiple attachment
            foreach ($this->request->data['Gmail']['attachments'] as $key => $attachment) {
        
                $addAttachment = $this->addAttachment($attachment);
                $link = WWW_ROOT.'email/attachments'.DS.$addAttachment['link'];
                $attachments[] = WWW_ROOT.'email/attachments'.DS.$addAttachment['link'];         

            }  

            $email->to($recipients);
            $email->attachments($attachments);
            $email->subject($this->request->data['Gmail']['subject']);
            $email->send($this->request->data['Gmail']['content']);
        }
        $email = $this->GoogleUser->findByUserId($userDetails['id']);
        $this->set(compact('email'));
    }

    public function replyMail($recipient){
        $userDetails = $this->Session->read('Auth.User');
        $senderEmail = $this->GoogleUser->findByUserId($userDetails['id']);
        if ($this->request->is('post')) {
        
            $recipients = array();
            $email = new CakeEmail('smtp');
            $email->from(array($senderEmail['GoogleUser']['google_email'] => 'Sebvet SRS'));
            $recipientEmail = explode(',', $this->request->data['Gmail']['recipient']);

            //multiple recipient
            foreach ($recipientEmail as $recipient) {
                $recipients[] =  $recipient; 
            }

            $attachments = array();
            //multiple attachment
            foreach ($this->request->data['Gmail']['attachments'] as $key => $attachment) {
        
                $addAttachment = $this->addAttachment($attachment);
                $link = WWW_ROOT.'email/attachments'.DS.$addAttachment['link'];
                $attachments[] = WWW_ROOT.'email/attachments'.DS.$addAttachment['link'];         

            }  

            $email->to($recipients);
            $email->attachments($attachments);
            $email->subject($this->request->data['Gmail']['subject']);
            $email->send($this->request->data['Gmail']['content']);
        }
        $email = $this->GoogleUser->findByUserId($userDetails['id']);
        $this->set(compact('email'));
    }


    /**
     * addAttachment method
     * This function will upload file,
     * The location for the uploaded file is under
     * folder /webroot/attachments/DATE/file.jpg
     */

    public function addAttachment($file) {
        
        $current_date = date('Y-m-d');

        //type of files that are allowed
        $fileTypes = array(
            // 'image/gif', 
            // 'image/jpeg',
            // 'image/png', 
            // 'application/pdf', 
            // 'application/msword', 
            // 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            // 'application/vnd.ms-excel',
            // 'application/vnd.ms-powerpoint'
            );

        $uploadFolder = 'email'.DS.'attachments'.DS.$current_date.DS;

        //check availability of a folder
        //return create new folder with current date if not exist

        if (file_exists($uploadFolder)) {
            $uploadFolder = $uploadFolder;
        } else {
            $old = umask(0);
            //give read permission
            mkdir(WWW_ROOT.'email/attachments'.DS.$current_date.DS, 0755, true);
            umask($old);
            // Checking
            if ($old != umask()) {
                die('An error occurred while changing back the umask');
            }
        }

        $uploadPath = WWW_ROOT.$uploadFolder;

        if (!empty($file)) {
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1);//get the extension

            //check file type
            //if file type is not lists in $fileTypes array,flash an error
         
                $date     = date_create();
                $rand     = rand(100000, 999999);
                $fileName = 'attachments'.date_format($date, 'U').'_'.$rand.'.'.$ext;
                move_uploaded_file($file['tmp_name'], $uploadPath.$fileName);

                $fileLink = $current_date.DS.$fileName;
                return array('fileType' => $file['type'], 'link' => $fileLink,'name'=>$file['name'],'size'=>$file['size']);

           
        }
    }
    public function urlsafe_b64decode($string) {
            $data = str_replace(array('-','_'),array('+','/'),$string);
            $mod4 = strlen($data) % 4;
            if ($mod4) {
                $data .= substr('====', $mod4);
            }
            return base64_decode($data);
    }












    /********IMAP R&d************/    
    public function imap_utf8_fix($string) { 
          //return iconv_mime_decode($string,0,"UTF-8"); 
          $data = imap_mime_header_decode($string);
          $str = "";
          foreach ($data as $d) {
            if ($d->charset == 'default') {
                $str .= $d->text;
            } else {
                $str .= iconv($d->charset, "utf-8", $d->text);
            }   
          }
          return $str;
        } 


    public function get_mail_headers($email, $inbox) {
        $headers=imap_fetchheader($inbox, $email);
        preg_match_all('/([^: ]+): (.+?(?:\r\n\s(?:.+?))*)\r\n/m', $headers, $matches);
        $hdrs = array();
        foreach ($matches[1] as $k=>$v) {
            $hdrs[$v] = $this->imap_utf8_fix($matches[2][$k]);
        }
        // debug($hdrs);
        return $hdrs;   
    }


    public function get_full_type($t, $st) {
    $types = array(
        0 => 'text',
        1 => 'multipart',
        2 => 'message',
        3 => 'application',
        4 => 'audio',
        5 => 'image',   
        6 => 'video',
        7 => 'other');
    return "{$types[$t]}/$st";  
    }

     function get_attachments($mbox,$mid,$p,$partno) {
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        // global $attachments;
        // DECODE DATA
        $data = ($partno)?
            imap_fetchbody($mbox,$mid,$partno):  // multipart
            imap_body($mbox,$mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding==4)
            $data = quoted_printable_decode($data);
        elseif ($p->encoding==3)
            $data = base64_decode($data);
        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->parameters){
            foreach ($p->parameters as $x){
                $params[strtolower($x->attribute)] = $x->value;
            }
        }
        if ($p->dparameters){
            foreach ($p->dparameters as $x){
                $params[strtolower($x->attribute)] = $x->value;
            }
        }
        // ATTACHMENT
        // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if ($params['filename'] || $params['name']) {
            // filename may be given as 'Filename' or 'Name' or both
            $filename = ($params['filename'])? $params['filename'] : $params['name'];
            // filename may be encoded, so see imap_mime_header_decode()
            $mime = get_full_type($p->type, $p->subtype);
            $this->attachments[$filename] = array( 'data'=>$data, 'mime'=> $mime, 'id'=>"cid:".trim($p->id,'<>') );  // this is a problem if two files have same name
        }
        // SUBPART RECURSION
        if ($p->parts) {
            foreach ($p->parts as $partno0=>$p2)
                $this->get_attachments($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
        }
    }

    public function getpart($mbox,$mid,$p,$partno) {
        // debug($mbox);
        // debug($mid);
        // debug($p);
        // return $p;
        // debug($partno);
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        // global $htmlmsg,$plainmsg,$charset,$attachments;
        // DECODE DATA
        $data = ($partno)?
            imap_fetchbody($mbox,$mid,$partno):  // multipart
            imap_body($mbox,$mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding==4){
            $data = quoted_printable_decode($data);
            // debug($data);
        }
        elseif ($p->encoding==3){
            $data = base64_decode($data);
            // debug($data);
        }
        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->parameters){
            foreach ($p->parameters as $x){
                $params[strtolower($x->attribute)] = $x->value;

            }
        }
        // debug($p);
        if (isset($p->dparameters)){
            foreach ($p->dparameters as $x){
                $params[strtolower($x->attribute)] = $x->value;
            }
        }
        // debug($params);

        if (isset($params['filename']) || isset($params['name'])) {
            // filename may be given as 'Filename' or 'Name' or both
        }       
          

        // // TEXT
        if ($p->type==0 && $data) {
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtolower($p->subtype)=='plain'){
                $this->plainmsg .= trim($data) ."\n\n";
            }
            else{
                $this->htmlmsg .= $data ."<br><br>";
                $this->charset = $params['charset'];  // assume all parts are same charset
            }
           
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($p->type==2 && $data) {
            $this->plainmsg .= $data."\n\n";
        }
        // SUBPART RECURSION
        if (isset($p->parts)) {
            foreach ($p->parts as $partno0=>$p2){
                $this->getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
            }
        }
        //   debug( $this->plainmsg);
        // debug( $this->htmlmsg);
        // debug($this->charset);
        //  debug($params);
         return array('plainMsg'=>$this->plainmsg,'htmlMsg'=>$this->htmlmsg,'charset'=>$this->charset,'params'=>$params);
    }

        public function test(){
            // global $htmlmsg,$plainmsg,$charset,$attachments;
            // $userDetails = $this->Session->read('Auth.User');
            // $googleUser = $this->GoogleUser->find('first',array(
            //     'conditions'=>array('GoogleUser.user_id'=>$userDetails['id'],'GoogleUser.isConnected'=>1)
            // ));
            $user = 'malikperang@gmail.com';
            $pass = 'F4r1zl1nux321*';
            $host = 'imap.gmail.com';
            $port = '993';

            $inbox = imap_open('{'.$host.':'.$port.'/imap/ssl}INBOX',$user,$pass) or die('Cannot connect to Gmail: ' . imap_last_error());
            $emails = imap_search($inbox,'ALL');
            if($emails) {
                /* begin output var */
                $output = '';   
                /* put the newest emails on top */
                rsort($emails);
                foreach ($emails as $email) {
                    $overview = imap_fetch_overview($inbox,$email,0);
                    $body = imap_body($inbox, $email);
                    debug($body);
                     // $structure = imap_fetchstructure($inbox, $email);
                    // debug($structure);
                    // if ($body->encoding==4){
                    //         $data = quoted_printable_decode($data);
                    //         // debug($data);
                    // }
                    //     elseif ($body->encoding==3){
                    //         $data = base64_decode($data);
                    //         // debug($data);
                    //     }
                    // $structure = imap_fetchstructure($inbox, $email);
                    // if($structure->encoding == "3"){
                    //     $body = base64_decode(imap_fetchbody($inbox, imap_msgno($inbox, $email), 1));
                    // }
                    // elseif($structure->encoding == "4"){
                    //     $body = quoted_printable_decode(imap_fetchbody($inbox, imap_msgno($inbox, $email), 1));
                    // }else{
                    //     $body = imap_fetchbody($inbox, imap_msgno($inbox, $email), 1);
                    // }
                    // debug($body);
                    //show content
                    // $html = str_replace($r_find, $r_repl, $htmlmsg);
                    // $text = $plainmsg;

                    // // debug($email);
                    // $headers = $this->get_mail_headers($email, $inbox);
                    // $text = ""; $html = "";
                    // $message_subject = $headers['Subject'];
                    // @$message_date = date('M jS Y h:i A', strtotime($headers['Date']));
                    // $message_sender = str_replace('"', '', $headers['From']);
                    // $ctype = $headers['Content-Type'];
                    // $s = imap_fetchstructure($inbox,$email);
        
                    // if (!isset($s->parts)){  // simple
                    //     // $this->getpart($inbox,$email,$s,0);  // pass 0 as part-number
                    //     $part = $this->getpart($inbox,$email,$s,0);

                    //     $messageBody = '';
                    //     if (isset($part['plainMsg'])) {
                    //         $messageBody = $part['plainMsg'];
                    //     }

                    //     if (isset($part['htmlMsg'])) {
                    //         $messageBody = $part['htmlMsg'];
                    //     }

                    //     // debug($email);

                    //     // debug($message);
                    //     $this->Gmail->create();
                    //     $data = array(
                    //         'messageId'=> $email,
                    //         // 'messageSnippet'=>$snippet,
                    //         'messageSubject'=>$message_subject,
                    //         'messageDate'=>$message_date,
                    //         'messageSender'=>$message_sender,
                    //         'messageBody'=> $messageBody,
                    //         );
                    //     $this->Gmail->save($data);

                    // }else {
                    //     foreach ($s->parts as $partno0=>$p){
                    //         $message = $this->getpart($inbox,$email,$p,$partno0+1);
                    //          // debug($this->getpart($inbox,$email,$p,$partno0+1));
                    //     }
                    // }
                    // $text = $this->plainmsg;
                    // $this->text = 'lalala';
                    // debug($text);
                }
            } 
        }
   
}
