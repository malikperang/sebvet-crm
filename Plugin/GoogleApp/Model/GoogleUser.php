<?php
App::uses('GoogleAppAppModel', 'Model');
/**
 * GoogleUser Model
 *
 * @property User $User
 * @property GoogleCalendar $GoogleCalendar
 */
class GoogleUser extends GoogleAppAppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
//	public $belongsTo = array(
//		'User' => array(
//			'className' => 'User',
//			'foreignKey' => 'user_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		),
//	);
}
