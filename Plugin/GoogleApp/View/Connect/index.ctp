<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if(!$this->Session->check('Google.token')):?>

                <form action="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'oauthCallback'),true)?>" method="post" >
                    <input type="text" name="email" value="uharith@gmail.com">
                    <button type="submit">Connect</button>
                </form>

            <?php endif ?>
            <?php if($this->Session->check('Google.token')):?>
            <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'logoutGoogle'),true)?>" class="btn btn-danger"  >forget access token</a>

            <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gmail','action' => 'index'),true)?>" class="btn btn-info" >Gmail</a>
            <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gdrive','action' => 'index'),true)?>"  class="btn btn-info" >Drive</a>
            <?php endif ?>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
<hr>
