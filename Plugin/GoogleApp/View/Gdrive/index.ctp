<table width="100%" class="table table-bordered table-striped">
    <?php foreach($files_list as $index => $item): ?>

        <tr>
            <td width="1%">
                <?php if((string)$item->mimeType==='application/vnd.google-apps.folder'):?>
                    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gdrive','action' => 'index','?'=>['parent'=>$item->id]),true) ?>">
                        <img src="http://icons.iconarchive.com/icons/laurent-baumann/aqua-blend/128/Aqua-Smooth-Folder-Sites-icon.png" width="25" height="25">
                    </a>

                <?php endif ?>

            </td>
            <td>
                <?php echo $item->title ?>
            </td>
            <td></td>

        </tr>

    <?php endforeach ?>
</table>