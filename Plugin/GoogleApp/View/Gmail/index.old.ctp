<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span>Primary</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane fade in active" id="home">
        <div class="list-group">

            <?php

            function findHeaderItem($name,$items){
                foreach($items as $index=>$item){
                    if((string)$name===(string)$item->name){
                        return $index;
                    }
                }
                return 0;
            }
            ?>



            <?php foreach($threads_detail as $index => $item): ?>

<?php


// debug($item);
// //var_dump($item->messages[0]->payload->headers);


                ?>


                <a href="<?php echo $this->webroot;?>google/gmail/view/<?php echo $item->id;?>" class="list-group-item" style="padding-bottom: 20px">

                    <span class="name" style="min-width: 120px;display: inline-block;"><?php echo $item->messages[0]->payload->headers[findHeaderItem('From',$item->messages[0]->payload->headers)]->value?></span>
                    <span class=""><?php echo $item->messages[0]->payload->headers[findHeaderItem('Subject',$item->messages[0]->payload->headers)]->value?></span>
                    <span class="text-muted" style="font-size: 11px;"><?php echo $item->messages[0]->snippet?></span>
                </a>

            <?php endforeach ?>
        </div>
    </div>
    <div class="tab-pane fade in" id="profile">
        <div class="list-group">
            <div class="list-group-item">
                <span class="text-center">This tab is empty.</span>
            </div>
        </div>
    </div>
    <div class="tab-pane fade in" id="messages">
        ...</div>
    <div class="tab-pane fade in" id="settings">
        This tab is empty.</div>
</div>