<?php 
echo $this->Html->css('bootstrap-tagsinput');
echo $this->Html->script('bootstrap-tagsinput');
// debug($this->params['pass']);
?>
<style type="text/css">
	.bootstrap-tagsinput {
  width: 100% !important;
}
</style>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<div class="page-header">
    <h1 style="margin-left:3px;">
        <i class="fa fa-envelope"></i> <?php echo __('Compose Email');?>
    </h1>
</div>
<div class="row" >
    <div class="col-xs-12 col-lg-10" style="margin-top:-15px;border:1px solid #ddd;padding:10px;border-radius:5px;">
	<?php echo $this->Form->create('Gmail',array('type'=>'file')); ?>
	<fieldset>
	<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right no-padding-left">From</label>
	<?php echo $this->Form->input('subject',array('div'=>false,'label'=>false,'class'=>'form-control','readonly'=>true,'placeholder'=>$email['GoogleUser']['google_email']));?>
	<?php echo $this->Html->link('Change Google account',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect')); ?>
	</div>
 
		<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right no-padding-left"> Recipient</label>
			<?php echo $this->Form->input('recipient',array('div'=>false,'label'=>false,'class'=>'form-control','data-role'=>'tagsinput','value'=>$this->params['pass'])); ?>
		<p class="help-block">Add up to more than 1 email address.</p>
	</div>
	<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right no-padding-left"> Subject</label>
	<?php echo $this->Form->input('subject',array('div'=>false,'label'=>false,'class'=>'form-control'));?>
	</div>
 
	<div class="form-group">
	<?php echo $this->Form->input('content',array('type'=>'textarea','rows'=>20,'div'=>false,'label'=>false,'class'=>'form-control','id'=>'editor1'));?>
	</div>
 
	<div class="form-group  input_fields_wrap">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="attachments"> Attachments</label>
		<?php echo $this->Form->input('attachments.', array('type' => 'file', 'multiple' => true,'class'=>'form-control'));?>
		<p class="help-block">Can upload more than 1 file.</p>
	</div>
	
	</fieldset>
	
	<div class="text-center">
	
		<?php echo $this->Form->button('Send',array('class'=>'btn btn-default','type'=>'submit','escape'=>false));?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		CKEDITOR.replace( 'editor1' );	
		$('.bootstrap-tagsinput :input').addClass('col-lg-12');
	});

</script>
<!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" > -->