<script type="text/javascript">
jQuery(document).ready(function($) {
        var defaultCSS = document.getElementById('bootstrap-css');
        (function changeCss(css){
                             

            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        
                })();        
});
       
</script>
<?php// debug($message['GoogleAccount']);?>

<div class="message-content" id="id-message-content">
<?php echo $this->Html->link('Back',array('plugin'=>'google_app','controller'=>'gmail','action'=>'index'),array('class'=>'btn btn-default back-btn'))?>
<br />
<div class="pull-left">
        <div class="space-4"></div>
        &nbsp;
     
       <?php 
        $sender = substr($message['Gmail']['messageSender'], strpos($message['Gmail']['messageSender'], "<") + 1);
        $sender = str_replace('>', ' ', $sender); 
       ?>
        <a href="#" class="sender">From : <?php echo $message['Gmail']['messageSender']?></a>

        &nbsp;
        <i class="ace-icon fa fa-clock-o bigger-110 orange middle"></i>
        <span class="time grey"><?php echo $message['Gmail']['messageDate']?></span>

        <?php //endforeach;?>  
</div>


<div class="pull-right action-buttons">
        <a href="<?php echo $this->webroot;?>google/gmail/reply/<?php echo $sender;?>" class="reply">
                <i class="ace-icon fa fa-reply green icon-only bigger-130"></i>
                Reply
        </a>
</div>

<br />

<div class="hr hr-double"></div>

<div class="message-body">
<?php //foreach ($messages as $message) :?>
<?php echo $message['Gmail']['messageBody'];?>
<?php //endforeach;?>      

</div>


<!-- /section:pages/inbox.message-body -->
<br />
<div class="hr-double"></div>
Attachments
<ul>

    <?php foreach($attachments as $attachment):?>
        <?php $mimeType = str_replace('/', '-', $attachment['GmailsAttachment']['mimeType']);?>
        <li>
        <!-- <a href="<?php echo $this->webroot;?>"></a> -->
        <?php echo $this->Html->link($attachment['GmailsAttachment']['name'],array('plugin'=>'google_app','controller'=>'gmail','action'=>'attachments',$attachment['GmailsAttachment']['messageId'],$attachment['GmailsAttachment']['attachmentId'],$mimeType,$attachment['GmailsAttachment']['name']));?></li>
    <?php endforeach;?>
</ul>

<style type="text/css">
    a:visited{
        color: white !important;
    }
    a.reply{
        color: #428bca !important;
    }
    a.sender{
        color: #6A9CBA !important;
    }
    a.back-btn:visited{
        color: #3985a8 !important;
    }
</style>