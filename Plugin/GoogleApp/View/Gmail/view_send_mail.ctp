<style type="text/css">
.nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
.tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
.tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
.tab-pane .list-group .checkbox { display: inline-block;margin: 0px; }
.tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
.tab-pane .list-group .glyphicon { margin-right:5px; }
.tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
a.list-group-item.read { color: #222;background-color: #F3F3F3; }
hr { margin-top: 5px;margin-bottom: 10px; }
.nav-pills>li>a {padding: 5px 10px;}

.ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
.ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
.ad a.url {color: #093;text-decoration: none;}
</style>

<?php if(empty($gUser) ||$gUser['GoogleUser']['isConnected'] == false ):?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#not-connect-modal').modal('show');    
        });
    </script>

<?php endif;?>
<div class="page-header">
    <h1>
        Sent Mail
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default " id="sync" data-toggle="tooltip" data-placement="bottom" title="Sync your contact with Google Contact">
                    <i class="fa fa-refresh" id="sync-icon"></i>
                    <img src="<?php echo $this->webroot;?>img/ajax-loader.gif" id="ajax-loader"  style='display:none'>
                </button>
        <?php echo $this->Html->link('Compose Email',array('plugin'=>'google_app','controller'=>'gmail','action'=>'sendMail'),array('class'=>'btn btn-default'));?>
        </div>
    </h1>
</div>
        
    <div class="row" style="margin-left:-12px;margin-top:-65px;">
        <div class="col-sm-3 col-md-2 col-lg-12" style="margin-top:-120px;">
                 
              
            <!-- Nav tabs -->
       <!--      <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox">
                    </span>Primary</a>
                </li>
            </ul> -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                <?php if(empty($gUser) ||$gUser['GoogleUser']['isConnected'] == false ):?>
                     <div class="alert alert-danger" role="alert">
                      <p>In order to fetch your Gmail messages,please connect your google account by clicking below button.
                      <br />  
                      <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Connection ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button></p>
                     </div>

                <?php endif;?>
                   
                    <div class="list-group">
                     <?php foreach($messages as $message): ?>
                        <a href="<?php echo $this->webroot;?>google/gmail/view/<?php echo $message['Gmail']['messageId']?>" class="list-group-item">   
                            <span class="name" style="min-width: 120px;
                                display: inline-block;">
                                    <?php
                                        $sender = substr($message['Gmail']['messageSender'], 0, 20);
                                        $sender = substr($sender, 0, strrpos($sender, ' ')) . " ...";
                                        // $sender =  substr($message['Gmail']['messageSender'], 30) . '...';
                                        // echo $sender;
                                        echo $message['Gmail']['messageSender'];
                                    ?>
                                </span> 
                                <span class="">
                                 
                                    <?php echo $message['Gmail']['messageSubject']?>
                                </span>
                            <span class="text-muted" style="font-size: 11px;">

                                <?php
                                  //  $snippet = substr($message['Gmail']['messageSnippet'], 10) . '...';
                                    //echo $snippet;
                                 ?>

                            </span>
                             <span class="badge"><?php echo $message['Gmail']['messageDate']?></span>
                                <?php if(!empty($message['GmailsAttachment'])):?>
                                 <span class="pull-right"><span class="glyphicon glyphicon-paperclip">
                                <?php endif;?>
                            </a>
                            <!--     <span class=""><? 
                                $t = mb_substr($message_subject, 0, 50);
                                echo mb_substr($t, 0, strrpos($t,' '));
                                ?><?if (strlen(trim($message_subject))>50):?>...<?endif;?></span>
                                <span class="text-muted" style="font-size: 11px;"><?
                                $st= mb_substr($text,0,50) ;
                                echo mb_substr($st, 0, strrpos($st,' ')); ?></span> 
                                <span class="badge"><? echo $message_date ?></span> 
                                <span class="pull-right">
                                    <span class="glyphicon glyphicon-paperclip"></span>
                                </span> -->
                    <?php endforeach;?>
                    </div>
                </div>
              
        </div>
    </div>
</div>
<!-- <ul class="nav nav-tabs">
    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span>Primary</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="home">
        <div class="list-group">
            <?php foreach($messages as $message): ?>
                <?php //debug($message);?>
                <a href="#" class="list-group-item" style="padding-bottom: 20px">

                    <span class="name" style="min-width: 120px;display: inline-block;"><?php echo $message['Gmail']['messageSender']?></span>
                    <span class=""><?php echo $message['Gmail']['messageSubject']?></span>
                    <span class="text-muted" style="font-size: 11px;"><?php echo $message['Gmail']['messageSnippet']?></span>
                </a>

            <?php endforeach ?>
        </div>
    </div>
</div> -->

<div class="modal fade" id="not-connect-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="center">Please connect your google account first.</h4>
          <img src="http://img3.wikia.nocookie.net/__cb20100520131746/logopedia/images/5/5c/Google_logo.png" class="img-responsive center-block">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Connection ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
jQuery(document).ready(function($) {
    $(document).on({
        ajaxStart:function(){
            $("#sync-icon").hide(),
            $("#ajax-loader").show()
        },
        ajaxStop:function(){
            $("#sync-icon").show(),
            $("#ajax-loader").hide()
    }});
    $("#sync").on('click', function(event) {
        event.preventDefault();
        /* Act on the event */        
        $.ajax({
            url: '<?php echo $this->webroot;?>google/gmail/getSentMail',
            // type: 'default GET (Other values: POST)',
            // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
            // data: {param1: 'value1'},
        })
        .done(function() {
            console.log("success");
            location.reload();
        })
        .fail(function(message) {
            console.log("error"+message);
             location.reload();
        });
    });    
});

</script>