<html>
<head>
    <script src="https://apis.google.com/js/client:platform.js" async defer></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
    <style>
        body{ margin-top:50px;}
        .nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
        .tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
        .tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
        .tab-pane .list-group .checkbox { display: inline-block;margin: 0px; }
        .tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
        .tab-pane .list-group .glyphicon { margin-right:5px; }
        .tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
        a.list-group-item.read { color: #222;background-color: #F3F3F3; }
        hr { margin-top: 5px;margin-bottom: 10px; }
        .nav-pills>li>a {padding: 5px 10px;}

        .ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
        .ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
        .ad a.url {color: #093;text-decoration: none;}
    </style>
</head>
<body>




<?php echo $this->Session->flash(); ?>

<div class="container">

    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'index'),true)?>" class="btn btn-info"  >Go to connect page</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'logoutGoogle'),true)?>" class="btn btn-danger"  >forget access token</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gmail','action' => 'index'),true)?>" class="btn btn-info" >Go to Gmail page</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gdrive','action' => 'index'),true)?>"  class="btn btn-info" >Go to Drive page</a>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
    <hr />
</div>

</body>
</html>

