<html>
<head>
    <script src="https://apis.google.com/js/client:platform.js" async defer></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
    <style>
        body{ margin-top:50px;}
        .nav-tabs .glyphicon:not(.no-margin) { margin-right:10px; }
        .tab-pane .list-group-item:first-child {border-top-right-radius: 0px;border-top-left-radius: 0px;}
        .tab-pane .list-group-item:last-child {border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;}
        .tab-pane .list-group .checkbox { display: inline-block;margin: 0px; }
        .tab-pane .list-group input[type="checkbox"]{ margin-top: 2px; }
        .tab-pane .list-group .glyphicon { margin-right:5px; }
        .tab-pane .list-group .glyphicon:hover { color:#FFBC00; }
        a.list-group-item.read { color: #222;background-color: #F3F3F3; }
        hr { margin-top: 5px;margin-bottom: 10px; }
        .nav-pills>li>a {padding: 5px 10px;}

        .ad { padding: 5px;background: #F5F5F5;color: #222;font-size: 80%;border: 1px solid #E5E5E5; }
        .ad a.title {color: #15C;text-decoration: none;font-weight: bold;font-size: 110%;}
        .ad a.url {color: #093;text-decoration: none;}
    </style>
</head>
<body>




<?php echo $this->Session->flash(); ?>

<div class="container">

    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'index'),true)?>" class="btn btn-info"  >Go to connect page</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'connect','action' => 'logoutGoogle'),true)?>" class="btn btn-danger"  >forget access token</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gmail','action' => 'index'),true)?>" class="btn btn-info" >Go to Gmail page</a>
    <a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gdrive','action' => 'index'),true)?>"  class="btn btn-info" >Go to Drive page</a>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <!-- Split button -->
            <div class="pull-right">
                <span class="text-muted"><b>1</b>–<b>50</b> of <b>277</b></span>
                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </button>
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <a href="#" class="btn btn-danger btn-sm btn-block" role="button">COMPOSE</a>
            <hr />
            <ul class="nav nav-pills nav-stacked">
                <?php foreach($labels as $index => $item): ?>
                    <?php if(in_array($item->id,['INBOX','SENT','SPAM','TRASH'])): ?>

                        <li class="<?php echo isset($this->request->query['label'])&&$this->request->query['label']==$item->id?'active':'' ?>"><a href="<?php echo $this->Html->url(array('plugin'=>'google_app','controller' => 'gmail','action' => 'index','?'=>['label'=>$item->id]),true)?>"><?php echo $item->id ?></a></li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
</div>



</body>
</html>

