<?php
App::uses('ContactCompany', 'Model');

/**
 * ContactCompany Test Case
 *
 */
class ContactCompanyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contact_company',
		'app.contact',
		'app.company',
		'app.user',
		'app.deal',
		'app.deal_stage',
		'app.activity'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ContactCompany = ClassRegistry::init('ContactCompany');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ContactCompany);

		parent::tearDown();
	}

}
