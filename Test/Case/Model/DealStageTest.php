<?php
App::uses('DealStage', 'Model');

/**
 * DealStage Test Case
 *
 */
class DealStageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.deal_stage',
		'app.deal'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DealStage = ClassRegistry::init('DealStage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DealStage);

		parent::tearDown();
	}

}
