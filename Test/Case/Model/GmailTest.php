<?php
App::uses('Gmail', 'Model');

/**
 * Gmail Test Case
 *
 */
class GmailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.gmail',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Gmail = ClassRegistry::init('Gmail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Gmail);

		parent::tearDown();
	}

}
