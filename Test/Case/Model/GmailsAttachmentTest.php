<?php
App::uses('GmailsAttachment', 'Model');

/**
 * GmailsAttachment Test Case
 *
 */
class GmailsAttachmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.gmails_attachment',
		'app.gmail',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GmailsAttachment = ClassRegistry::init('GmailsAttachment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GmailsAttachment);

		parent::tearDown();
	}

}
