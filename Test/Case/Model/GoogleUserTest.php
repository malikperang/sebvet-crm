<?php
App::uses('GoogleUser', 'Model');

/**
 * GoogleUser Test Case
 *
 */
class GoogleUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.google_user',
		'app.user',
		'app.google_calendar'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GoogleUser = ClassRegistry::init('GoogleUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GoogleUser);

		parent::tearDown();
	}

}
