<?php
App::uses('LeadStatus', 'Model');

/**
 * LeadStatus Test Case
 *
 */
class LeadStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.lead_status',
		'app.contact',
		'app.company',
		'app.deal',
		'app.deal_stage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LeadStatus = ClassRegistry::init('LeadStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LeadStatus);

		parent::tearDown();
	}

}
