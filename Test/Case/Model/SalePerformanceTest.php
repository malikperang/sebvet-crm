<?php
App::uses('SalePerformance', 'Model');

/**
 * SalePerformance Test Case
 *
 */
class SalePerformanceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sale_performance'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SalePerformance = ClassRegistry::init('SalePerformance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SalePerformance);

		parent::tearDown();
	}

}
