<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Calendar');?>
		</h1>
	</div>
		<div class="row">
	<div class="col-sm-12 col-lg-12">
	<?php if(empty($googleUser['GoogleUser']['google_calendar_id'])):?>
		<div class="modal fade" id="notSetCalendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		      <div class="modal-body">
		          <img src="http://appcenter.clickmeeting.com/uploads/2014/05/google-calendar-logo1.gif" class="img-responsive center-block">
		            <h4 class="center">Please set your google calendar!</h4>
		            <p>We are unable to generate the calendar.You need to set a google calendar you wanted to synchronize by clicking the button below.</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Calendar ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleCalendar'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button>
		      </div>
		    </div>
		  </div>
		</div>

		<script type="text/javascript">
				jQuery(document).ready(function($) {
					$('#notSetCalendar').modal('show');
				});		
		</script>
	<?php endif;?>
	<div class="responsive-iframe-container">
    <iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=<?php echo $googleUser['GoogleUser']['google_calendar_id'];?>&amp;color=%238D6F47&amp;ctz=Asia%2FKuala_Lumpur" style=" border-width:0 " width="1300" height="900" frameborder="0" scrolling="no"></iframe>
</div>
		
</div>
</div>