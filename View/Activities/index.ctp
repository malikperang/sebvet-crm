<?php //debug($activities);?>
<div class="page-header">
	<h1 class="page-header-override">
			<?php echo __('Activities');?>
			<button type="button" class="btn btn-default" data-toggle="modal" data-target=".addActivity">
			 		<i class="ace-icon fa fa-user-plus"></i> New Activity
				</button>
				<div class="btn-group pull-right">
				<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View Google Calendar">
			 		<a href="<?php echo $this->webroot;?>activities/calendar"><i class="menu-icon fa fa-calendar"></i></a>
				</button>

				<button type="button" class="btn btn-default" id="sync" data-toggle="tooltip" data-placement="bottom" title="Sync with Google Calendar">
			 		<i class="fa fa-refresh" id="sync-icon"></i>
			 		<img src="<?php echo $this->webroot;?>img/ajax-loader.gif" id="ajax-loader"  style='display:none'>
				</button>
				</div>
	</h1>
	</div>
<div class="row">
<div class="col-sm-12 col-lg-12 activity-table" >
	<div class="table-responsive">
	<div class="space-10"></div>
	<a id="selectall" ><i class="fa fa-arrow-circle-down"></i> Select All</a>
	<?php echo $this->Form->create('Activity',array('action'=>'deleteMany','id'=>'dAform'));?>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-hover dt-responsive shadow-table" cellspacing="0" width="100%">
	<thead>
	<tr>	
		<th id="hover-check"><a type="submit" id="deleteAct" style="display:none"><i class="ace-icon fa fa-trash fa-2x"></i></a></th>
		<th class="col-lg-6"><?php echo $this->Paginator->sort('notes'); ?></th>
		<th class="col-lg-2"><?php echo $this->Paginator->sort('created','Date Created'); ?></th>
		<th class="col-lg-2"><?php echo $this->Paginator->sort('created_by','Created by'); ?></th>
		<th class="col-lg-2"><?php echo $this->Paginator->sort('deal_id','Associated Deal'); ?></th>
		<th>Action</th>
	</tr>
	</thead>

	<tbody>
	
	<?php foreach ($activities as $activity): ?>
	<tr>
		<td id="hover-check"><?php echo $this->Form->checkbox('Activity.' . $activity['Activity']['id'],array('class'=>'act-check display-none','value'=>$activity['Activity']['id'],'name'=>'data[Activity][id][]','hiddenField' => false,'id'=>'act-check'));?></td>
		<td><?php echo h($activity['Activity']['notes']); ?>&nbsp;</td>
		<td><?php echo date('d M Y H:i:s',strtotime($activity['Activity']['created'])); ?>&nbsp;</td>
		<td><?php echo $this->Html->link(h($activity['User']['name']),array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$activity['User']['id'])); ?>&nbsp;</td>
		<td><?php echo $this->Html->link(h($activity['Deal']['name']),array('plugin'=>false,'controller'=>'deals','action'=>'view',$activity['Deal']['id'],$userDetails['id'])); ?>&nbsp;</td>
		<td><?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $activity['Activity']['id']), array('class'=>'btn btn-default btn-sm','escape'=>false), __('Are you sure you want to delete this activity?')); ?></td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<?php echo $this->Form->end();?>
</div>
</div>
</div>




<div class="modal fade success-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-color="red">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <img src="http://appcenter.clickmeeting.com/uploads/2014/05/google-calendar-logo1.gif" class="img-responsive center-block" style="margin-left:10px">
     <h1 class="text-center" style="color:#0266C8">Sync Success!</h1>
    </div>
  </div>
</div>

<div class="modal fade error-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
    <h1> Error </h1>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="notSetCalendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
          <img src="http://appcenter.clickmeeting.com/uploads/2014/05/google-calendar-logo1.gif" class="img-responsive center-block">
            <h4 class="center">Please set your google calendar!</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Calendar ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleCalendar'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="notConnect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
          <img src="http://appcenter.clickmeeting.com/uploads/2014/05/google-calendar-logo1.gif" class="img-responsive center-block">
      	   <h4 class="center">Please connect your google account!</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Connection ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$('#deleteAct').click(function(event) {
			/* Act on the event */
			$('#dAform').submit();
		});

		$('[id^=hover-check],#selectall').hover(function() {
			/* Stuff to do when the mouse enters the element */
			$('input[name="data[Activity][id][]"]').show();
			$('#deleteAct').show();
			
			$('[id^=hover-check]').addClass('border-right');
			
		}, function() {
			/* Stuff to do when the mouse leaves the element */
			$('#deleteAct').hide();
			$('input[name="data[Activity][id][]"]').hide();
			$('[id^=hover-check]').removeClass('border-right');
		});

		$('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

      })	
	 $('[data-toggle="tooltip"]').tooltip();

	  $('#selectall').click(function(event) {  //on click 
	   	var selectActive = $(this).attr('class');
	   	// console.log(selectActive);
	 
        if($(this).data('clicked', true)) { // check select status
        	$(this).addClass('clicked');
            $('.act-check').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1" 
                if (this.checked == true) {
                	$('#selectall').html('<i class="fa fa-arrow-circle-down"></i>  Unselect All')
                };             
            });
        	}
        	if(selectActive == 'clicked'){
        		$(this).removeClass('clicked');
        		$('#selectall').html('<i class="fa fa-arrow-circle-down"></i> Select All')
        	
	   			$('.act-check').each(function() { //loop through each checkbox
                this.checked = false;  //select all checkboxes with class "checkbox1"               
            });
	   	}
       });


	$(document).on({
    ajaxStart: function() {  
    	$('#sync-icon').hide();
    	$('#ajax-loader').show();  
    },
    ajaxStop: function() { 
    	$('#sync-icon').show();
    	$('#ajax-loader').hide();  
     }    
	});
		$("#sync").click(function(event) {
			event.preventDefault();
			$.ajax({
	            type: 'POST',
	            url: '<?php echo $this->Html->url(array("action" => "sync")); ?>',
	            success: function (message){
	              	var message = jQuery.parseJSON(message);
	              	console.log(message);
	              	if (message == 'sync success') {
	              		if($('.success-modal').modal('show')){
	              			setInterval(function() {
	              				location.reload();
	              			},1500);
	              			
	              		};
	              	
	              	}else if(message == 'google account not connect'){
	              		$('#notConnect').modal('show');
	              	}else if(message == 'google calendarID not found'){
	              		$('#notSetCalendar').modal('show');

	              	}else{
	              		$('.error-modal').modal('show');
	              	};

	            	},
		        error:function(message){
		        	var message = jQuery.parseJSON(message);
		          	if (message == 'not connected') { $('.not-connect-modal').modal('show'); };
		         }
        	});
		});



	   $(".clickableRow").click(function() {
	   	  window.document.location = $(this).attr("href");
          
      });

	  // $(".select_box").chosen({
		 //  	width:"100%",
		 //    no_results_text: "Oops, nothing found!",
		    
		 //    search_contains:true
	  // 	});
	  $('[data-toggle="tooltip"]').tooltip();

	});

</script>

<?php 
		// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-timepicker.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');
		

?>
<div class="modal fade addActivity" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="addContactModal"><i class="fa fa-user"></i> Add new activity</h4>
      </div>
      <div class="modal-body">
    <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
							<a data-toggle="tab" href="#note" class="act-text">		
								<?php echo __('Add new activity');?>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#attachment" class="act-text">
								<?php echo __('Attachment');?>
							</a>
						</li>
				</ul>
				</div>
				<div class="tab-content">
				<div id="note" class="tab-pane in active">	
			<?php echo $this->Form->create('Activity',array('id'=>'activity-form','controller'=>'activities','action'=>'add','type'=>'file','class'=>''));?>

			
		<div class="row no-margin-right" >
			<div class="form-group">
			 <label><?php echo __('Associated Deals');?></label>
			<?php echo $this->Form->input('deal_id',array('class'=>'form-control select_box','label'=>false,'div'=>false,'empty'=>'Choose Deal'))?>
			</div>
		</div>

		<div class="row" >
					<div class="form-group">
					 <label><?php echo __('Left a note');?></label>
					<?php echo $this->Form->input('notes',array('class'=>'form-control','label'=>false,'div'=>false,'type'=>'textarea'))?>
					</div>
				</div>

			
							<div class="row">
							<div class="form-group">

							<div class="col-sm-2">
							Date
								<?php echo $this->Form->input('start_date',array(
									'class'=>'form-control date-picker','type'=>'text',
									'div'=>false,'label'=>false,'data-date-format'=>'yyyy-mm-dd'
								))?>
							</div>

							<div class="col-sm-2">
							Due Date
								<?php echo $this->Form->input('due',array(
									'class'=>'form-control date-picker','type'=>'text',
									'div'=>false,'label'=>false,'data-date-format'=>'yyyy-mm-dd'
								))?>
							</div>

							<div class="col-sm-2">
							<?php echo __('Location');?>
								<?php echo $this->Form->input('location',array(
									'class'=>'form-control','type'=>'text',
									'div'=>false,'label'=>false,'placeholder'=>'Optional'
								))?>
							</div>

							<div class="col-sm-2">
							<?php echo __('Time');?>
								<?php echo $this->Form->input('event_time',array(
								'class'=>'form-control','type'=>'text',
									'id'=>'timepicker',
									'div'=>false,'label'=>false,'placeholder'=>'Optional'
								))?>
							</div>


								<div class="col-sm-2">
								Assigned To
								<?php echo $this->Form->input('user_id',array('class'=>'form-control','label'=>false,'div'=>false))?>
							</div>
							</div>
							</div>

										<br />
				<div class="">
				<?php echo $this->Form->input('created_by',array('type'=>'hidden','value'=>$userDetails['id']));?>		
       				<?php echo $this->Form->button('<i class="fa fa-upload"></i> Submit',array('class'=>'btn btn-sm pull-right act-btn','escape'=>false));?>
		&nbsp; &nbsp; &nbsp;
			      </div>
				</div>

			<div id="attachment" class="tab-pane">	
			 <label><?php echo __('Add an Attachment');?></label>
			<?php echo $this->Form->input('attachment',array('class'=>'form-control','label'=>false,'type'=>'file'))?>
			<div class="">
       				<?php echo $this->Form->button('<i class="fa fa-upload"></i> Submit',array('class'=>'btn btn-sm pull-right act-btn','escape'=>false));?>
		&nbsp; &nbsp; &nbsp;
			      </div>
			<?php echo $this->Form->end();?>
				</div>
				</div>
    </div>
  </div>
</div>
</div>
				
		
			<script type="text/javascript">
			jQuery(function($) {


			$("#activity-form").on('submit',function(e) {
					e.preventDefault();
					// console.log('submit');
					// $("#message").empty();
					// $('#loading').show();
					// $(this).ajaxStart(function() {
					// 	$('.act-btn').button('loading');
					// });
					
					
					$.ajax({
						url: '<?php echo $this->Html->url(array("controller"=>"activities","action" => "add")); ?>', // Url to which the request is send
						type: 'POST',             // Type of request to be send, called as method
						data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,        // To send DOMDocument or non processed data file it is set to false
						success: function(data)   // A function to be called if request succeeds
						{
							location.reload();
						}
						})
					});
								
				$('#timepicker').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

				 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
	});


				 $(".select_box").chosen({
				  	width:"100%",
				    no_results_text: "Oops, nothing found!",
				    
				    search_contains:true
			  	});
				 $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			});
</script>

