<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Edit Announcement');?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-lg-12">
	<?php echo $this->Form->create('Announcement',array('type'=>'file','action'=>'add')); ?>
	<fieldset>
		<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="title"> Title</label>
			<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
	</div>
	
	<div class="form-group">
	<?php echo $this->Form->input('content',array('type'=>'textarea','rows'=>20,'div'=>false,'label'=>false,'class'=>'form-control','id'=>'editor1'));?>
	</div>

	<div class="form-group  input_fields_wrap">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="attachments"> Attachments</label>
		<?php echo $this->Form->input('file.', array('type' => 'file','multiple'=>true,'class'=>'form-control'));?>
		<p class="help-block">Can upload more than 1 file. Supported file type : (pdf,xls,doc,ppt)</p>
	</div>
	
	</fieldset>
	
	<div class="text-center">
		<?php echo $this->Form->button('Submit Announcement',array('class'=>'btn btn-default','type'=>'submit'));?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		CKEDITOR.replace( 'editor1' );	
	});

</script>