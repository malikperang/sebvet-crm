<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Announcements');?>
			<?php if($userDetails['group_id'] == 1):?>
				<button type="button" class="btn btn-default" data-toggle="modal" data-target=".addAnnoucement">
			 		<i class="fa fa-plus-circle"></i> New Announcement
				</button>
			<?php endif;?>
		</h1>

	</div>
	<div class="row">
	<div class="col-sm-12 col-lg-12">
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-hover table-condensed table-striped">
	<thead>
	<tr>
			<th><?php echo '#'; ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('created','Created at'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Updated'); ?></th>
			<th><?php echo $this->Paginator->sort('created_by'); ?></th>
			<th><?php echo $this->Paginator->sort('modified_by','Updated By'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $number = 1;?>
	<?php foreach ($announcements as $announcement): ?>
	<tr>
		<td><?php echo h($number++); ?>&nbsp;</td>
		<td><?php echo h($announcement['Announcement']['title']); ?>&nbsp;</td>
		<td><?php echo h(date('d M Y H:i:s',strtotime($announcement['Announcement']['created']))); ?>&nbsp;</td>
		<td><?php echo h(date('d M Y H:i:s',strtotime($announcement['Announcement']['modified']))); ?>&nbsp;</td>
		<td><?php echo h($announcement['Created']['name']); ?>&nbsp;</td>
		<td><?php echo h($announcement['Modified']['name']); ?>&nbsp;</td>
		<td class="actions">
		<div class="btn-group">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $announcement['Announcement']['id']),array('class'=>'btn btn-sm btn-default')); ?>
			<?php if($userDetails['group_id'] == 1):?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $announcement['Announcement']['id']),array('class'=>'btn btn-sm btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $announcement['Announcement']['id']), array('class'=>'btn btn-sm btn-default'), __('Are you sure you want to delete # %s?', $announcement['Announcement']['id'])); ?>
			<?php endif;?>
			</div>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
     <?php
     echo $this->Paginator->counter(array(
     'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
     ));
     ?>     </p>
     <ul class="pagination pagination pagination-right">
     <li><?php
          echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
     ?>
     </li><li><?php
          echo $this->Paginator->numbers(array('separator' => ''));
     ?>
     </li><li><?php
          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
     ?>
     </li>    
     </ul>
</div>
</div>
</div>

<div class="modal fade addAnnoucement" tabindex="-1" role="dialog" aria-labelledby="addAnnoucement" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addAnnoucement">Add New Annoucement</h4>
      </div>
     <div class="modal-body">
 
     	<?php echo $this->Form->create('Announcement',array('type'=>'file','action'=>'add')); ?>
		<fieldset>
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right no-padding-left" for="title"> Title</label>
				<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
			</div>
		
		<div class="form-group">

		<?php echo $this->Form->input('content',array('type'=>'textarea','rows'=>20,'div'=>false,'label'=>false,'class'=>'form-control','id'=>'editor1'));?>
		</div>

		<div class="form-group  input_fields_wrap">
			<label class="col-sm-2 control-label no-padding-right no-padding-left" for="attachments"> Attachments</label>
			<?php echo $this->Form->input('file.', array('type' => 'file','multiple'=>true,'class'=>'form-control','required'=>false));?>
			<p class="help-block">Can upload more than 1 file. Supported file type : (pdf,xls,doc,ppt)</p>
		</div>
		
		</fieldset>
		
		<div class="text-center">
			<?php echo $this->Form->button('Submit Announcement',array('class'=>'btn btn-primary','type'=>'submit'));?>
		</div>
		<?php echo $this->Form->end(); ?>
	     </div>
	     </div>
    </div>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
		CKEDITOR.replace( 'editor1' );	
	});
</script>
