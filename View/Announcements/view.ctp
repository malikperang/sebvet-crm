<div class="widget-box">
<div class="widget-header">
	<h5 class="widget-title">	<?php echo h($announcement['Announcement']['title']); ?></h5>
	<div class="widget-toolbar">
		<p class="text-right">Created at : <strong><?php echo h(date('d-M-Y',strtotime($announcement['Announcement']['created']))); ?></strong>
Created by: <strong><?php echo h($announcement['Created']['name']); ?></strong>
		&nbsp;

	Last Update at: <strong><?php echo h(date('d-M-Y',strtotime($announcement['Announcement']['created']))); ?></strong>
	
	Updated by:  <strong><?php echo h($announcement['Created']['name']); ?></strong>
		</p>
			</div>
		</div>

		<div class="widget-body">
			<div class="widget-main">
				<?php echo $announcement['Announcement']['content']; ?>
				<p>Attachments:</p>
				
					<ul class="list-group">
					<?php $num = 1;?>
						<?php foreach($attachments as $attachment):?>
							<li class="list-group-item">
							<?php echo $num++;?>.
							<?php echo $this->Html->link($attachment['Attachment']['name'],array('controller'=>'attachments','action'=>'view',$attachment['Attachment']['id']));?></li>
						<?php endforeach;?>
					</ul>
				</p>
				
			</div>
		</div>
	</div>
</div>