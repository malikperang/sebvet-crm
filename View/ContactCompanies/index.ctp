<div class="contactCompanies index">
	<h2><?php echo __('Contact Companies'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('created_by'); ?></th>
			<th><?php echo $this->Paginator->sort('modified_by'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($contactCompanies as $contactCompany): ?>
	<tr>
		<td><?php echo h($contactCompany['ContactCompany']['id']); ?>&nbsp;</td>
		<td><?php echo h($contactCompany['ContactCompany']['name']); ?>&nbsp;</td>
		<td><?php echo h($contactCompany['ContactCompany']['created']); ?>&nbsp;</td>
		<td><?php echo h($contactCompany['ContactCompany']['modified']); ?>&nbsp;</td>
		<td><?php echo h($contactCompany['ContactCompany']['created_by']); ?>&nbsp;</td>
		<td><?php echo h($contactCompany['ContactCompany']['modified_by']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contactCompany['ContactCompany']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contactCompany['ContactCompany']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contactCompany['ContactCompany']['id']), array(), __('Are you sure you want to delete # %s?', $contactCompany['ContactCompany']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contact Company'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
