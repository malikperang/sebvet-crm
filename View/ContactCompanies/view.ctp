<div class="contactCompanies view">
<h2><?php echo __('Contact Company'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['created_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified By'); ?></dt>
		<dd>
			<?php echo h($contactCompany['ContactCompany']['modified_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contact Company'), array('action' => 'edit', $contactCompany['ContactCompany']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contact Company'), array('action' => 'delete', $contactCompany['ContactCompany']['id']), array(), __('Are you sure you want to delete # %s?', $contactCompany['ContactCompany']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Companies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Company'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Contacts'); ?></h3>
	<?php if (!empty($contactCompany['Contact'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Profile Picture'); ?></th>
		<th><?php echo __('Contact Status Id'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th><?php echo __('Contact Company Id'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Country'); ?></th>
		<th><?php echo __('Street Address'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Remarks'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Created By'); ?></th>
		<th><?php echo __('Modified By'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contactCompany['Contact'] as $contact): ?>
		<tr>
			<td><?php echo $contact['id']; ?></td>
			<td><?php echo $contact['name']; ?></td>
			<td><?php echo $contact['profile_picture']; ?></td>
			<td><?php echo $contact['contact_status_id']; ?></td>
			<td><?php echo $contact['company_id']; ?></td>
			<td><?php echo $contact['contact_company_id']; ?></td>
			<td><?php echo $contact['email']; ?></td>
			<td><?php echo $contact['country']; ?></td>
			<td><?php echo $contact['street_address']; ?></td>
			<td><?php echo $contact['city']; ?></td>
			<td><?php echo $contact['state']; ?></td>
			<td><?php echo $contact['phone']; ?></td>
			<td><?php echo $contact['remarks']; ?></td>
			<td><?php echo $contact['created']; ?></td>
			<td><?php echo $contact['modified']; ?></td>
			<td><?php echo $contact['created_by']; ?></td>
			<td><?php echo $contact['modified_by']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'contacts', 'action' => 'view', $contact['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'contacts', 'action' => 'edit', $contact['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'contacts', 'action' => 'delete', $contact['id']), array(), __('Are you sure you want to delete # %s?', $contact['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
