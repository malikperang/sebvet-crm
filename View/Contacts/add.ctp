<div class="page-header">
		<h1>
			<?php echo __('New contact');?>
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
			</small>
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> Contact List',array('controller'=>'contacts','action'=>'index'),array('class'=>'btn btn-danger ','escape'=>false));?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-lg-12">
<?php echo $this->Form->create('Contact',array('class'=>'form-horizontal','role'=>'form','type'=>'file')); ?>
	<fieldset>
	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Name</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('name',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>

	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Phone </label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('phone',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>

	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Company</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('company_name',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Email</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>
	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Address</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('street_address',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Country</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('country',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>


	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> City</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('city',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</div>

	<?php 
		$states = array(
			'Terengganu'=>'Terengganu',
			'Selangor'=>'Selangor',
			'Sarawak'=>'Sarawak',
			'Sabah'=>'Sabah',
			'Perlis'=>'Perlis',
			'Perak'=>'Perak',
			'Penang'=>'Penang',
			'Pahang'=>'Pahang',
			'Negeri Sembilan'=>'Negeri Sembilan',
			'Melaka'=>'Melaka',
			'Kelantan'=>'Kelantan',
			'Kedah'=>'Kedah',
			'Johor'=>'Johor',
			'Wilayah Persekutuan Putrajaya'=>'Wilayah Persekutuan Putrajaya',
			'Wilayah Persekutuan Labuan'=>'Wilayah Persekutuan Labuan',
			'Wilayah Persekutuan Kuala Lumpur'=>'Wilayah Persekutuan Kuala Lumpur',

			);
	?>

	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> State</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('state',array('class'=>'form-control','div'=>false,'label'=>false,'options'=>$states));?>
	</div>
	</div>

	

	<div class="form-group">
		<label class="col-sm-1 control-label no-padding-right" for="form-field-1"> Profile Picture</label>
		<div class="col-sm-6">
	<?php echo $this->Form->input('profile_picture',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'file'));?>
	</div>
	</div>
	
	<?php echo $this->Form->input('created_by',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'hidden'));?>
	</fieldset>
	<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-8">
			<?php echo $this->Form->button('Create Contact',array('type'=>'submit','class'=>'btn btn-info center-block'));?>
			&nbsp; &nbsp; &nbsp;
			
		</div>
	</div>
	</div>
	</div>
<?php echo $this->Form->end(); ?>
</div>

