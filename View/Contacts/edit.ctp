<?php  //debug($this->data);?>
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Update Contacts');?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div class="widget-box " style="padding:5px">
          <div class="widget-body only-body-widget">
            <div class="widget-main no-padding">
 	<?php echo $this->Form->create('Contact',array('controller'=>'contact','action'=>'edit','type'=>'file')); ?>
	<fieldset>
	 <legend>Basic Info</legend>
	<div class="form-group">
		<label for="name"> Full Name</label>
	<?php echo $this->Form->input('name',array('class'=>'form-control','label'=>false,'placeholder'=>'John Doe'));?>
	</div>

	<div class="form-group">
		<label for="name"> Phone</label>
	<?php echo $this->Form->input('phone',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>

	<div class="form-group">
		<label for="name"> Company Name</label>
	<?php echo $this->Form->input('company_name',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Optional'));?>
	</div>

	<div class="form-group">
		<label for="name"> E-mail</label>
	<?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'johndoe@email.com','type'=>'email'));?>
	</div>
  <div class="row">
  <legend>Address</legend>
    <div class="col-xs-6 col-md-6 col-lg-6">
	<div class="form-group">
		<label for="name"> Street Address</label>
	<?php echo $this->Form->input('street_address',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'No.20 Jalan 2 Taman Jaya'));?>
	</div>
	</div>
	 
    <div class="col-xs-6 col-md-6 col-lg-6">
	<div class="form-group">
		<label for="name"> Postcode</label>
	<?php echo $this->Form->input('postcode',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'3200'));?>
	</div>
</div>
<div class="row">

    <div class="col-xs-4 col-md-4 col-lg-4">
	<div class="form-group">
		<label for="name"> City</label>
	<?php echo $this->Form->input('city',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Kuala Lumpur'));?>
	</div>
</div>
	<?php 
		$states = array(
			'Terengganu'=>'Terengganu',
			'Selangor'=>'Selangor',
			'Sarawak'=>'Sarawak',
			'Sabah'=>'Sabah',
			'Perlis'=>'Perlis',
			'Perak'=>'Perak',
			'Penang'=>'Penang',
			'Pahang'=>'Pahang',
			'Negeri Sembilan'=>'Negeri Sembilan',
			'Melaka'=>'Melaka',
			'Kelantan'=>'Kelantan',
			'Kedah'=>'Kedah',
			'Johor'=>'Johor',
			'Wilayah Persekutuan Putrajaya'=>'Wilayah Persekutuan Putrajaya',
			'Wilayah Persekutuan Labuan'=>'Wilayah Persekutuan Labuan',
			'Wilayah Persekutuan Kuala Lumpur'=>'Wilayah Persekutuan Kuala Lumpur',

			);
	?>
<div class="col-xs-4 col-md-4 col-lg-4">
	<div class="form-group">
		<label for="name"> State</label>
		<?php echo $this->Form->input('state',array('class'=>'form-control','div'=>false,'label'=>false,'options'=>$states));?>
	</div>
	</div>
<div class="col-xs-2 col-md-2 col-lg-2">

	<div class="form-group">
		<label for="name"> Country</label>
	<?php echo $this->Form->input('country',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Malaysia'));?>
	</div>
	
	</div>
	</div>
	<div class="form-group">
		<label for="profile"> Contact Picture</label>
	<?php echo $this->Form->input('profile_picture',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'file'));?>
	</div>


	<div class="form-group">
	<label for="name"> Remarks</label>
		<?php echo $this->Form->input('remarks',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	</fieldset>
	<?php echo $this->Form->input('googleContactId',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'hidden','value'=>$this->data['Contact']['google_contact_id']));?>
	<?php echo $this->Form->input('googleContactEtag',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'hidden','value'=>$this->data['Contact']['google_contact_etag']));?>
   	<?php echo $this->Form->button('Update Contact',array('type'=>'submit','class'=>'btn btn-danger center-block'));?>
			&nbsp; &nbsp; &nbsp;
	<?php echo $this->Form->end();?>