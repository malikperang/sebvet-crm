<?php
		// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
?>
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Contacts');?>
				<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addContact" >
			 		<i class="ace-icon fa fa-user-plus"></i> New Contact
				</button>
				<div class="btn-group pull-right">
			
				<button type="button" class="btn btn-default btn-sm" id="sync" data-toggle="tooltip" data-placement="bottom" title="Sync your contact with Google Contact">
			 		<i class="fa fa-refresh" id="sync-icon"></i>
			 		<img src="<?php echo $this->webroot;?>img/ajax-loader.gif" id="ajax-loader"  style='display:none'>
				</button>
				</div>
				<?php //echo $this->Html->link('<i class="fa fa-refresh"></i>',array('controller'=>'contacts','action'=>'sync'),array('escape'=>false,'class'=>'btn btn-default pull-right','name'=>'sync'));?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
	<?php echo $this->Form->create('Contact',array('url'=>array('plugin'=>false,'controller'=>'contacts','action'=>'search'),'type'=>'post'));?>
		<?php echo $this->Form->input('contact_id', array(
			'class'=>'form-control select_box',
			// 'placeholder' => 'Username',
			'label' => false,
			'beforeInput' => '<div class="input-group"><span class="input-group-addon"><i class="ace-icon fa fa-search"></i></span>',
			'onchange'=>'this.form.submit()',
			'afterInput' => '</div>','empty'=>'Find a contact..','options'=>$displayContacts
		)); ?>
	<?php echo $this->Form->end();?>

	<div class="space-10"></div>
	<div class="btn-group">
	<a id="selectall" ><i class="fa fa-arrow-circle-down"></i> Select All</a>
	</div>
	<p class="pull-right">Total Contacts : <?php echo $totalContact;?></p>
	<div class="table-responsive">
	<?php echo $this->Form->create('Contact',array('action'=>'deleteMany','id'=>'dCform'));?>
	<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
        	<th id="hover-check" class="col-lg-1 text-center" style="width:2%;" ><a type="submit" id="deleteContact" style="display:none"><i class="ace-icon fa fa-trash fa-2x"></i></a></th>
            <th class="col-md-2 col-lg-1"><?php echo __('Name');?></th>
            <th class="col-md-2 col-lg-1"><?php echo __('Phone');?></th>
            <th class="col-md-2 col-lg-1"><?php echo __('Email');?></th>
            <th class="col-md-2 col-lg-1"><?php echo __('Source');?></th>
            <th class="col-md-2 col-lg-1"><?php echo __('Contact Owner');?></th>
            <th class="col-md-1 col-lg-1"><?php echo __('Action');?></th>
        </tr>
    </thead>
    <tbody class="searchable">

        <?php foreach($contacts as $contact):?>
        		<tr class="row-hover" id="<?php echo $contact['Contact']['id']?>">
        			<td id="hover-check" ><?php echo $this->Form->checkbox('Contact.' . $contact['Contact']['id'],array('class'=>'contact display-none','value'=>$contact['Contact']['id'],'name'=>'data[Contact][id][]','hiddenField' => false,'id'=>'c-check'));?></td>
        			<td  class="col-lg-2 td-hover clickableRow" id="<?php ?>" href="<?php echo $this->webroot;?>contacts/view/<?php echo $contact['Contact']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $contact['Contact']['name'];?></td>
        			<td class="col-lg-2 td-hover clickableRow" id="<?php ?>" href="<?php echo $this->webroot;?>contacts/view/<?php echo $contact['Contact']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $contact['Contact']['phone'];?></td>
        			<td  style="col-lg-2" class="clickableRow" id="<?php ?>" href="<?php echo $this->webroot;?>contacts/view/<?php echo $contact['Contact']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $contact['Contact']['email'];?></td>
        			<td  class="col-lg-2 clickableRow" id="<?php ?>" href="<?php echo $this->webroot;?>contacts/view/<?php echo $contact['Contact']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $contact['Contact']['source'];?></td>
        			<td  class="col-lg-2 clickableRow" id="<?php ?>" href="<?php echo $this->webroot;?>contacts/view/<?php echo $contact['Contact']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $contact['Owner']['name'];?></td>
        			<td class="col-lg-1">
        			<div class="btn-group">
        			<?php echo $this->Html->link('<i class="fa fa-pencil-square-o"></i>',array('controller'=>'contacts','action'=>'edit',$contact['Contact']['id']),array('class'=>'btn btn-default','escape'=>false));?>
        			<?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('controller'=>'contacts','action' => 'delete', $contact['Contact']['id'],urlencode($contact['Contact']['google_contact_id']),$contact['Contact']['google_contact_etag']), array('class'=>'btn btn-default','escape'=>false), __('Are you sure you want to delete this contact?')); ?>
        			</div>
        			</td>
        		</tr>
        <?php endforeach;?>
    </tbody>
	</table>
	<?php echo $this->Form->end();?>

	</div>
	<p>
     <?php
     // echo $this->Paginator->counter(array(
     // 'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
     // ));
     ?>     </p>
     <ul class="pagination pagination pagination-right" id="pagination">
     <li><?php
          echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
     ?>
     </li><li><?php
          echo $this->Paginator->numbers(array('separator' => ''));
     ?>
     </li><li><?php
          echo $this->Paginator->next(__('Next') . ' >>', array(), null, array('class' => 'next disabled'));
     ?>
     </li>    
     </ul>
	</div>
	</div>
	



<!-- Modal -->
<div class="modal fade " id="addContact" tabindex="-1" role="dialog" aria-labelledby="addContactModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContactModal"><i class="fa fa-user"></i> Add new contact</h4>
      </div>
      <div class="modal-body">
       	<?php echo $this->Form->create('Contact',array('controller'=>'contact','action'=>'add','type'=>'file')); ?>
	<fieldset>
	 <legend>Basic Info</legend>
	<div class="form-group">
		<label for="name"> Full Name</label>
	<?php echo $this->Form->input('name',array('class'=>'form-control','label'=>false,'placeholder'=>'John Doe'));?>
	</div>

	<div class="form-group">
		<label for="name"> Phone</label>
	<?php echo $this->Form->input('phone',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>

	<div class="form-group">
		<label for="name"> Company Name</label>
	<?php echo $this->Form->input('company_name',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Optional'));?>
	</div>

	<div class="form-group">
		<label for="name"> E-mail</label>
	<?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'johndoe@email.com','type'=>'email'));?>
	</div>
  <div class="row">
  <legend>Address</legend>
    <div class="col-xs-6 col-md-6 col-lg-6">
	<div class="form-group">
		<label for="name"> Street Address</label>
	<?php echo $this->Form->input('street_address',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'No.20 Jalan 2 Taman Jaya'));?>
	</div>
	</div>
	 
    <div class="col-xs-6 col-md-6 col-lg-6">
	<div class="form-group">
		<label for="name"> Postcode</label>
	<?php echo $this->Form->input('postcode',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'3200'));?>
	</div>
</div>
<div class="row">

    <div class="col-xs-4 col-md-4 col-lg-4">
	<div class="form-group">
		<label for="name"> City</label>
	<?php echo $this->Form->input('city',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Kuala Lumpur'));?>
	</div>
</div>
	<?php 
		$states = array(
			'Terengganu'=>'Terengganu',
			'Selangor'=>'Selangor',
			'Sarawak'=>'Sarawak',
			'Sabah'=>'Sabah',
			'Perlis'=>'Perlis',
			'Perak'=>'Perak',
			'Penang'=>'Penang',
			'Pahang'=>'Pahang',
			'Negeri Sembilan'=>'Negeri Sembilan',
			'Melaka'=>'Melaka',
			'Kelantan'=>'Kelantan',
			'Kedah'=>'Kedah',
			'Johor'=>'Johor',
			'Wilayah Persekutuan Putrajaya'=>'Wilayah Persekutuan Putrajaya',
			'Wilayah Persekutuan Labuan'=>'Wilayah Persekutuan Labuan',
			'Wilayah Persekutuan Kuala Lumpur'=>'Wilayah Persekutuan Kuala Lumpur',

			);
	?>
<div class="col-xs-4 col-md-4 col-lg-4">
	<div class="form-group">
		<label for="name"> State</label>
		<?php echo $this->Form->input('state',array('class'=>'form-control','div'=>false,'label'=>false,'options'=>$states));?>
	</div>
	</div>
<div class="col-xs-2 col-md-2 col-lg-2">

	<div class="form-group">
		<label for="name"> Country</label>
	<?php echo $this->Form->input('country',array('class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Malaysia'));?>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
		<label for="profile"> Contact Picture</label>
	<?php echo $this->Form->input('profile_picture',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'file'));?>
	</div>

	<div class="form-group">
	<label for="name"> Remarks</label>
		<?php echo $this->Form->input('remarks',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	
	<?php echo $this->Form->input('created_by',array('class'=>'form-control','div'=>false,'label'=>false,'type'=>'hidden'));?>
   </fieldset>
   </div>

      <div class="modal-footer">
       		<?php echo $this->Form->button('Add Contact',array('type'=>'submit','class'=>'btn btn-danger center-block'));?>
			&nbsp; &nbsp; &nbsp;
			<?php echo $this->Form->end();?>
      </div>
    </div>
  </div>
</div>


<div class="modal fade success-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-color="red">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <img src="http://static1.squarespace.com/static/53adaf69e4b0b52e4e720557/t/53bfe706e4b0bee6fd0705a8/1405085454058/google-contacts-logo.jpg" class="img-responsive center-block">
     <h1 class="text-center">Sync Success!</h1>
    </div>
  </div>
</div>

<div class="modal fade error-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
    <h1> Error </h1>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="not-connect-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="center">Please connect your google account first.</h4>
          <img src="http://img3.wikia.nocookie.net/__cb20100520131746/logopedia/images/5/5c/Google_logo.png" class="img-responsive center-block">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"><?php echo $this->Html->link('Set Google Connection ',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect'),array('class'=>'dark'));?><i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
	.modal-color-red .modal-backdrop {
  background-color: none !important;
}
.modal-color-green .modal-backdrop {
  background-color: #0f0;
}
.modal-color-blue .modal-backdrop {
  background-color: #00f;
}

</style>

<script type="text/javascript">
$(document).ready(function(t) {


    t("[id^=hover-check],#selectall").hover(function() {
        t('input[name="data[Contact][id][]"]').show(), t("#deleteContact").show(), t("[id^=hover-check]").addClass("border-right")
    }, function() {
        t("#deleteContact").hide(), t('input[name="data[Contact][id][]"]').hide(), t("[id^=hover-check]").removeClass("border-right")
    }), t("#deleteContact").on("click", function(e) {
        e.preventDefault(), t("#dCform").submit()
    });
    var e = "<?php echo $totalContact;?>";
    0 == e && (t("#selectall").hide(), t("#pagination").hide()), t(document).on({
        ajaxStart: function() {
            t("#sync-icon").hide(), t("#ajax-loader").show()
        },
        ajaxStop: function() {
            t("#sync-icon").show(), t("#ajax-loader").hide()
        }
    }), t("#sync").click(function(e) {
        e.preventDefault(), t.ajax({
            type: "POST",
            url: '<?php echo $this->Html->url(array("action" => "sync")); ?>',
            success: function(e) {
                var e = jQuery.parseJSON(e);
                console.log(e), "Success" == e ? t(".success-modal").modal("show") && setInterval(function() {
                    location.reload()
                }, 1500) : "not connected" == e ? swal("Google ") : t(".error-modal").modal("show")
            },
            error: function(e) {
                var e = jQuery.parseJSON(e);
                "not connected" == e && t(".not-connect-modal").modal("show")
            }
        })
    }), t(".clickableRow").click(function() {
        window.document.location = t(this).attr("href")
    }), t(".select_box").chosen({
        width: "100%",
        no_results_text: "Oops, nothing found!",
        search_contains: !0
    }), t('[data-toggle="tooltip"]').tooltip(), t("#selectall").click(function() {
        var e = t(this).attr("class");
        t(this).data("clicked", !0) && (t(this).addClass("clicked"), t(".contact").each(function() {
            this.checked = !0, 1 == this.checked && t("#selectall").html('<i class="fa fa-arrow-circle-down"></i>  Unselect All')
        })), "clicked" == e && (t(this).removeClass("clicked"), t("#selectall").html('<i class="fa fa-arrow-circle-down"></i> Select All'), t(".contact").each(function() {
            this.checked = !1
        }))
    }), t('input[name="data[Contact][id][]"]').click(function() {
        1 == this.checked && t("#deleteContact").show(), this.checked.length < 0 && t("#deleteContact").hide()
    })
});
	</script>

