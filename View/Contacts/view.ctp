<?php 
//debuggin section
// debug($contact);
// debug($dealsHistory);
?>
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('<i class="fa fa-user"></i> Contact');?>
		</h1>
</div>
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-lg-4">
		<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">
			 <strong>Details</strong>
			</span>
		</div>
		<div class="panel-body">
		<div class="row">
		<div class="col-lg-12 text-center">
		<span class="profile-picture">
			<?php if(!empty($contact['Contact']['profile_picture'])):?>
			<?php echo $this->Html->image('uploads/'.$contact['Contact']['profile_picture'],array('fullBase'=>true,'class'=>'editable img-responsive'));?>
			<?php else:?>
			<?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'editable img-responsive contact-img-view '));?>
			<?php endif;?>
		
		</span>
		</div>
		</div>
		
		<div class="profile-user-info profile-user-info-striped">
		    <div class="profile-info-row">
							<div class="profile-info-name"> <?php echo __('Name');?> </div>

							<div class="profile-info-value">
								<?php echo h($contact['Contact']['name']);?>
							</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Company Name');?> </div>

				<div class="profile-info-value">
					<?php echo h($contact['ContactCompany']['name']);?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Email');?> </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['email']);?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Street Address');?> </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['street_address']);?>
				</div>
			</div>

				<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('City');?>  </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['city']);?>
				</div>
			</div>
		
			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('State');?>  </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['state']);?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Country');?> </div>

				<div class="profile-info-value">
				<?php echo h($contact['Contact']['country']);?>
					<!-- <span class="editable editable-click" id="login">3 hours ago</span> -->
				</div>
			</div>


			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Phone Number');?>  </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['phone']);?>
				</div>
			</div>


			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Remarks');?>  </div>

				<div class="profile-info-value">
					<?php echo h($contact['Contact']['remarks']);?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name"> <?php echo __('Owner');?> </div>

				<div class="profile-info-value">
					<?php echo $this->Html->link(h($contact['Owner']['name']),array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$contact['Owner']['id']));?>
				</div>
			</div>	
		</div>
		</div>
		</div>
 </div>

<div class="col-xs-12 col-sm-12 col-lg-8">
<div class="panel panel-default">

  <div class="panel-heading text-center">
  	<span class="panel-title"><strong>Deals History</strong></span>
  </div>
  <div class="panel-body">
  <ul class="list-group col-lg-4">
 	<li class="list-group-item title">Ongoing</li>
   	<?php foreach($ongoing as $deals):?>
   		<li class="list-group-item"><a href="<?php echo $this->webroot;?>deals/view/<?php echo $deals['Deal']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $deals['Deal']['name'];?></a></li>
   	<?php endforeach;?>
  </ul>
  <ul class="list-group col-lg-4">
    <li class="list-group-item title">Won</li>
    <?php foreach($won as $deals):?>
   		<li class="list-group-item"><a href="<?php echo $this->webroot;?>deals/view/<?php echo $deals['Deal']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $deals['Deal']['name'];?></a></li>
   	<?php endforeach;?>
  </ul>
  <ul class="list-group col-lg-4">
    <li class="list-group-item title">Lost</li>
    <?php foreach($lost as $deals):?>
   		<li class="list-group-item"><a href="<?php echo $this->webroot;?>deals/view/<?php echo $deals['Deal']['id']?>/<?php echo $userDetails['id'];?>"><?php echo $deals['Deal']['name'];?></a></li>
   	<?php endforeach;?>
  </ul>
  </div>


</div>


    </div>
 </div>
</div>