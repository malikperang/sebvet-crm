<div class="conversations view">
<h2><?php echo __('Conversation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User One'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['user_one']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Two'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['user_two']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Conversation'), array('action' => 'edit', $conversation['Conversation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Conversation'), array('action' => 'delete', $conversation['Conversation']['id']), array(), __('Are you sure you want to delete # %s?', $conversation['Conversation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Messages'), array('controller' => 'messages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Message'), array('controller' => 'messages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Created'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
