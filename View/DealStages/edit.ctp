<div class="dealStages form">
<?php echo $this->Form->create('DealStage'); ?>
	<fieldset>
		<legend><?php echo __('Edit Deal Stage'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('DealStage.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('DealStage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Deal Stages'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Deals'), array('controller' => 'deals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Deal'), array('controller' => 'deals', 'action' => 'add')); ?> </li>
	</ul>
</div>
