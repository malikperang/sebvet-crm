<div class="dealStages view">
<h2><?php echo __('Deal Stage'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['created_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified By'); ?></dt>
		<dd>
			<?php echo h($dealStage['DealStage']['modified_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Deal Stage'), array('action' => 'edit', $dealStage['DealStage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Deal Stage'), array('action' => 'delete', $dealStage['DealStage']['id']), array(), __('Are you sure you want to delete # %s?', $dealStage['DealStage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Deal Stages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Deal Stage'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Deals'), array('controller' => 'deals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Deal'), array('controller' => 'deals', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Deals'); ?></h3>
	<?php if (!empty($dealStage['Deal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Contact Id'); ?></th>
		<th><?php echo __('Deal Stage Id'); ?></th>
		<th><?php echo __('Value'); ?></th>
		<th><?php echo __('Estimate Close Date'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Created By'); ?></th>
		<th><?php echo __('Modified By'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($dealStage['Deal'] as $deal): ?>
		<tr>
			<td><?php echo $deal['id']; ?></td>
			<td><?php echo $deal['name']; ?></td>
			<td><?php echo $deal['contact_id']; ?></td>
			<td><?php echo $deal['deal_stage_id']; ?></td>
			<td><?php echo $deal['value']; ?></td>
			<td><?php echo $deal['estimate_close_date']; ?></td>
			<td><?php echo $deal['created']; ?></td>
			<td><?php echo $deal['modified']; ?></td>
			<td><?php echo $deal['created_by']; ?></td>
			<td><?php echo $deal['modified_by']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'deals', 'action' => 'view', $deal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'deals', 'action' => 'edit', $deal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'deals', 'action' => 'delete', $deal['id']), array(), __('Are you sure you want to delete # %s?', $deal['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Deal'), array('controller' => 'deals', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
