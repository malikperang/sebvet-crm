<?php 
// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');
		

?>
<div class="page-header">
		<h1 class="page-header-override">
			<i class="fa fa-money"></i> <?php echo __('New Deal');?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		<div class="widget-box " style="padding:5px">
          <div class="widget-body only-body-widget">
            <div class="widget-main no-padding">

        <?php echo $this->Form->create('Deal',array('controller'=>'deals','action'=>'add','class'=>'form-horizontal','role'=>'form','id'=>'addNewDealForm','novalidate'=>true)); ?>
			<fieldset>
			 <legend>Deal Information</legend>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Deal Name</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('name',array('class'=>'form-control','div'=>false,'label'=>false,'required'=>true)); ?>
				<p class="help-block">Specify deal name. E.g: Honda Civic FD2 Type R 2015.</p>
			</div>

			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Contact</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('contact_id',array('class'=>'form-control select_box required','div'=>false,'label'=>false,'empty'=>'Choose Contact')); ?>
			<p class="help-block with-errors">Required. Please add contacts to this deal.</p>
			</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Deal Stage</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('deal_stage_id',array('div'=>false,'class'=>'form-control select_box','label'=>false,'empty'=>'Choose Stage','required'=>true));?>
			<p class="help-block">Required. Please specify deal stages.</p>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Value (RM)</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('value',array('class'=>'form-control','div'=>false,'label'=>false));?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Schedule an Activity</label>
				<div class="col-sm-8">
				<div class="input-group">
			<?php echo $this->Form->input('estimate_close_date',array(
				'class'=>'form-control date-picker',
				'type'=>'text',
				'div'=>false,'label'=>false,
				'id'=>'date-timepicker1',
				'after'=>'<span class="input-group-addon">
						<i class="fa fa-clock-o bigger-110"></i>
						</span>'));?>
			</div>
			</div>
			</div>
			<?php echo $this->Form->input('created_by',array('div'=>false,'label'=>false,'value'=>$userDetails['id'],'type'=>'hidden'));?>
			</fieldset>
   
       	<?php echo $this->Form->button('Add Deal',array('class'=>'btn btn-danger center-block','type'=>'submit '));?>
       	<?php echo $this->Form->end(); ?>
	
<?php echo $this->Form->end(); ?>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
			jQuery(function($) {

				 $(".select_box").chosen({
				  	width:"100%",
				    no_results_text: "Oops, nothing found!",
				    
				    search_contains:true
			  	});
				 $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			});
</script>
