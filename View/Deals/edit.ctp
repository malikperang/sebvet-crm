<?php 
// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');
		

?>
<div class="row">
<div class="col-xs-12 col-lg-8" >
<div class="page-header">
		<h1>
			<?php echo __('Edit Deal');?>

			<?php //echo $this->Html->link('Deals List',array('controller'=>'deals','action'=>'index'),array('class'=>'btn ','escape'=>false));?>
		</h1>
	</div>

<div class="panel panel-default" style="padding:5px;">
<?php echo $this->Form->create('Deal',array('role'=>'form')); ?>
	<fieldset>
	<div class="form-group">
		<label class="control-label no-padding-right" for="form-field-1"> Deals Name</label>
	<?php echo $this->Form->input('name',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
	<p class="help-block">Specify deal name. E.g: Honda Civic FD2 Type R 2015.</p>
	</div>
	<div class="form-group">
		<label class="control-label no-padding-right" for="form-field-1"> Contact</label>
	<?php echo $this->Form->input('contact_id',array('class'=>'form-control select_box','div'=>false,'label'=>false)); ?>
	</div>
	<div class="form-group">
		<label class="control-label no-padding-right" for="form-field-1"> Deal Stage</label>
	<?php echo $this->Form->input('deal_stage_id',array('class'=>'form-control select_box','div'=>false,'label'=>false));?>
	<p class="help-block">Required. Please specify deal stages.</p>
	</div>
	<div class="form-group">
		<label class="control-label no-padding-right" for="form-field-1"> Value (RM)</label>
	<?php echo $this->Form->input('value',array('class'=>'form-control','div'=>false,'label'=>false));?>
	</div>
	<div class="form-group">
		<label class=" control-label no-padding-right" for="form-field-1"> Estimated Close Date</label>
	<?php echo $this->Form->input('estimate_close_date',array('class'=>'form-control date-picker','type'=>'text','div'=>false,'label'=>false,'data-date-format'=>'dd-mm-yyyy'));?>
	</div>
	<?php echo $this->Form->input('created_by',array('div'=>false,'label'=>false,'value'=>$userDetails['id'],'type'=>'hidden'));?>
	</fieldset>
	<div class="clearfix">
			<div class="col-md-offset-5 col-md-9">
				<?php echo $this->Form->button('Update',array('class'=>'btn btn-default','type'=>'submit'));?>

			</div>
		</div>
		</div>
	</div>
		</div>
	
<?php echo $this->Form->end(); ?>
<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
					 $(".select_box").chosen({
		  	width:"100%",
		    no_results_text: "Oops, nothing found!",
		    
		    search_contains:true
	  	});
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			});
		</script>
