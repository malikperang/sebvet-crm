<?php 

		// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');
		

?>
	<div class="page-header">
		<h1 class="page-header-override">
			<?php echo __('Deals');?>
				<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#addNewDeal">
			 		<i class="fa fa-money"></i> New Deal
				</button>
		</h1>
	</div>
	
	<div class="row">
	<div class="col-sm-12 col-lg-12">

		<div role="tabpanel">
		<ul class="nav nav-tabs triangle-tabs"  id="deal-tab" role="tablist">
		<?php foreach($dealStages as $stage):?>
				<li role="presentation" id="list-t">
					<a data-toggle="tab" role="tab" href="#<?php echo $stage['DealStage']['slug'];?>">		
						<?php echo __($stage['DealStage']['name']);?>
					
					</a>

				</li>
		<?php endforeach;?>
		</ul>

		<!--Tab Start-->
		<div class="tab-content" id="deal-tab-content">
		<?php foreach($dealStages as $stage):?>	
		<div role="tabpanel" id="<?php echo $stage['DealStage']['slug'];?>" class="tab-pane in">		
		<?php echo $this->Form->create('Deal',array('url'=>array('plugin'=>false,'controller'=>'deals','action'=>'search'),'type'=>'post'));?>
		<?php echo $this->Form->input('deal_id', array(
			'class'=>'form-control select_box',
			// 'placeholder' => 'Username',
			'label' => false,
			'beforeInput' => '<div class="input-group"><span class="input-group-addon"><i class="ace-icon fa fa-search"></i></span>',
			'onchange'=>'this.form.submit()',
			'afterInput' => '</div>','empty'=>'Find a deal..','options'=>$deals
		)); ?>
	<?php echo $this->Form->end();?>
		 <div class="space-10"></div>
		 <div class="table-responsive">
			<table class="table table-responsive table-striped table-hover table-bordered shadow-table dt-responsive" cellspacing="0" width="100%">
				<thead>
				<tr class="">
						<th><?php echo __('Deal Name'); ?></th>
						<th><?php echo __('Contact Name'); ?></th>
						<th><?php echo __('Deal Value (RM)'); ?></th>
						<th><?php echo __('Scheduled Activity Date'); ?></th>
						<th><?php echo __('Created at'); ?></th>
						<th><?php echo __('Action'); ?></th>
				</tr>
				</thead>
				<tbody class="searchable">
				<?php foreach ($stage['Deal'] as $deal): ?>	
					<?php //debug($deal)?>
				<?php 
					//admin/super user can view all the deals
					if($userDetails['group_id'] == 1):
				?>

				<tr class="">
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['name']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>">
						<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'],$userDetails['id'])); ?>
					</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['value']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['estimate_close_date']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['created']); ?>&nbsp;</td>
					<td><?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $deal['id']), array('class'=>'btn btn-default btn-sm','escape'=>false), __('Are you sure you want to delete this deal?')); ?></td>
				</tr>

				<?php else:?>

				<?php 
				//a members/staff can only get the deal that they has created
				if($deal['created_by'] == $userDetails['id']):?>
				<tr class="">
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['name']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>">
						<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
					</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>">
						<?php echo $deal['DealStage']['name']; ?>
					</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['value']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['estimate_close_date']); ?>&nbsp;</td>
					<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['created']); ?>&nbsp;</td>
					<td><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $deal['id']), array('class'=>'btn btn-primary btn-sm'), __('Are you sure you want to delete this deal?')); ?></td>
				</tr>
						
						<?php endif;?>
					<?php endif;?>
				<?php endforeach; ?>

				</tbody>
				</table>
			</div>
			</div>
			<?php //end loop for tab
			endforeach;
			?>
		</div>
	</div>
</div>
</div>

<!-- Add new deal modal -->
<div class="modal fade" id="addNewDeal" tabindex="-1" role="dialog" aria-labelledby="addNewDealLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addNewDealLabel"> <i class="fa fa-money"></i> New deal</h4>
      </div>
      <div class="modal-body">
        <?php echo $this->Form->create('Deal',array('controller'=>'deals','action'=>'add','class'=>'form-horizontal','role'=>'form','novalidate'=>true)); ?>
			<fieldset>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Deal Name</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('name',array('class'=>'form-control','div'=>false,'label'=>false)); ?>
				<p class="help-block">Specify deal name. E.g: Honda Civic FD2 Type R 2015.</p>
			</div>

			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Contact</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('contact_id',array('class'=>'form-control select_box','div'=>false,'label'=>false,'empty'=>'Choose Contact')); ?>
				<p class="help-block">Required. Please select a contact.</p>
			</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Deal Stage</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('deal_stage_id',array('div'=>false,'class'=>'form-control select_box','label'=>false,'empty'=>'Choose Stage','options'=>$dealStagesList,'required'=>true));?>
			<p class="help-block">Required. Please specify deal stages.</p>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Value (RM)</label>
				<div class="col-sm-8">
			<?php echo $this->Form->input('value',array('class'=>'form-control','div'=>false,'label'=>false));?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Schedule an Activity</label>
				<div class="col-sm-8">
				<div class="input-group">
			<?php echo $this->Form->input('estimate_close_date',array(
				'class'=>'form-control date-picker',
				'type'=>'text',
				'div'=>false,'label'=>false,
				'id'=>'date-timepicker1',
				'after'=>'<span class="input-group-addon">
						<i class="fa fa-clock-o bigger-110"></i>
						</span>'));?>
			</div>
			</div>
			</div>
			<?php echo $this->Form->input('created_by',array('div'=>false,'label'=>false,'value'=>$userDetails['id'],'type'=>'hidden'));?>
			</fieldset>
      </div>
      <div class="modal-footer">
       	<?php echo $this->Form->button('Create Deal',array('class'=>'btn btn-danger','type'=>'submit'));?>
       	<?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	jQuery(document).ready(function($) {

			var p = 1;
			var tp = 0;
			$('[id^=list-t]').each(function() {
				$(this).addClass('p'+p++);
			});
			$('[data-toggle^=tab]').each(function() {
				$(this).attr('id', 'triangle');
				// $(this).addClass('triangle-p'+tp++);
			});


		$(".clickableRow").click(function() {
            window.document.location = $(this).attr("href");
      	});

      	//checkbox checking
		$('#selectall').click(function(event) { 
        if(this.checked) { 
            $('.checkbox1').each(function() {
                this.checked = true;            
            });
        }else{
            $('.checkbox1').each(function() { 
                this.checked = false;                      
            });         
        }
  	  	});
		$('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

      })

		//for tab sorting
		$('#deal-tab li').first().addClass( "active" );
	
		// $('#deal-tab li a div').first().addClass( "nav-tabs.triangle-tabs a textoverride" );
		// $('#deal-tab li a div').first().addClass('text nav-tabs.triangle-tabs a textoverride');
		// $('#deal-tab li a').first().removeClass( "triangle-p0" );
		// $('#deal-tab li a').first().addClass( "triangle-p3" );
		// $('#deal-tab li a').first().removeAttr( "id" );

		$('#deal-tab-content div').first().addClass( "active" );
		
		 $(".select_box").chosen({
		  	width:"100%",
		    no_results_text: "Oops, nothing found!",
		    
		    search_contains:true
	  	});
		 $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
			$(this).prev().focus();
		});


		var colors = [
		'#16A765',
			'#fa573c',
			
			'#f393af',
			'#4986E7',
			'#ffad46',
			
			'#9FE7C6',
			'#8fe1b9',
			'#b3dc6c',
		];

		var i = 0;
		var k = 0;
		function fixSize(item, count_all) {
			var ul = $(item).parents("ul.triangle-tabs:first");
			$(item).parent().css("width", ($( ul ).width()/count_all) + "px");
			var liWidth = parseFloat($(item).parent().css('width'));
			var aWidth  = parseFloat($(item).css('width'));
			console.log("aw="+aWidth+", liw="+liWidth);
			/*
			if ( (aWidth-liWidth)>5) {
				$(item).parent().tooltip({
					'title': $(item).html(),
					'placement': 'bottom'					
				});
			} else {
				$(item).parent().tooltip('destroy');
			}
			*/
		}
		$(function () {
			var count_all = $(".triangle-tabs>li").length;
			$( window ).resize(function() {
				$(".triangle-tabs>li>a").each(function (a, ind) {
				 fixSize(this, count_all);
				});
			});	
			$(".triangle-tabs>li>a").each(function (a, ind) {
				$(this).css("background", colors[i]);
				var prev = (i-1 < 0)?colors.length-1:i-1;
				
				$(this).bind('mouseover', function (e) {
					e.stopPropagation();
					console.log($(e.target));
					var ul = $(e.target).parents("ul.triangle-tabs:first");
					var w = parseFloat($(e.target).css('width'));
					console.log('set width '+w);
					$(e.target).parent().css("width", w + 'px');
					var rest = $( ul ).width() - w;
					console.log("all width "+$( ul ).width()+" set width "+w+" rest width "+rest);
					var target = e.target;
					var width = rest/(count_all-1);
				    $(".triangle-tabs>li").each(function (t, k) {
						if ($(this).is($(target).parent())) {
							console.log('skip current');
						} else {
							console.log("set width "+width);
							$(this).css("width", width + 'px');
						}
					});
				});
				$(this).bind('mouseleave', function (e) {
					e.stopPropagation();
					console.log('fix size');
					$(".triangle-tabs>li>a").each(function (t, k) {
						fixSize(this, count_all);
					});			
				});		
				
				fixSize(this, count_all);
				if (k==0) {
					$(this).css("border-color", colors[i]);
				} else {
					$(this).css("border-color", "transparent transparent "+colors[prev]+" transparent");
					$(this).css("_border-color", "#000 #000 "+colors[prev]+" #000");
					$(this).css("_filter", "progid:DXImageTransform.Microsoft.Chroma(color='#000000')");
					$(this).css("border-color", "transparent transparent transparent "+colors[prev]);		
				}
				if (i >= colors.length-1) { i=0; } else { i++; };
				k++;
			});
		});
			});

</script>