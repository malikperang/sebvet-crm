<div class="page-header">
<h1>
			<?php echo __('Deals');?>
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
			</small>
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> New Deals',array('controller'=>'deals','action'=>'add'),array('class'=>'btn btn-danger ','escape'=>false));?>
		</h1>
	</div>
	<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="tabbable">
		<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#all">		
						<?php echo __('All');?>
						<span class="badge badge-danger"><?php echo $totalDeals;?></span>
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#incoming">
						<?php echo __('New Opportunities');?>
						<span class="badge badge-danger"><?php echo $totalIncoming;?></span>
					</a>
				</li>

				<li>
					<a data-toggle="tab" href="#contact">
						<?php echo __('Follow Up');?>
						<span class="badge badge-danger"><?php echo $totalContacted;?></span>
					</a>
				</li>

				<li class="dropdown">
					<a data-toggle="tab" href="#quote">
						<?php echo __('Appoinment/Demo');?>
						<span class="badge badge-danger"><?php echo $totalQuote;?></span>
					</a>
				</li>
				<li class="dropdown">
					<a data-toggle="tab" href="#salesc" class="won-text">
						<?php echo __('Sales Cycle');?>
						<span class="badge badge-danger"><?php echo $totalSalesCycle;?></span>
					</a>
				</li>
				<li class="dropdown">
					<a data-toggle="tab" href="#won" class="won-text">
						<?php echo __('Invoice (Won)');?>
						<span class="badge badge-danger"><?php echo $totalWon;?></span>
					</a>
				</li>
				<li class="dropdown">
					<a data-toggle="tab" href="#lost" class="">
						<?php echo __('Lost');?>
						<span class="badge badge-danger"><?php echo $totalLost;?></span>
					</a>
				</li>
			</ul>
	<div class="tab-content">
	<div id="all" class="tab-pane in active">							
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-all">
	<thead>
	<tr class="">
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td colspan="7"><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($alldeals as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>

	<div id="incoming" class="tab-pane">							
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-incoming">
	<thead>
	<tr>
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>		
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($incoming as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>		
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>

	<div id="contact" class="tab-pane">
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-contact">
	<thead>
	<tr class="">
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
			
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($contacted as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>		
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>


	<div id="quote" class="tab-pane">
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-quotes">
	<thead>
	<tr class="">
		 	<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td colspan="7"><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($quotes as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>

	<div id="salesc" class="tab-pane">
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="salescycle">
	<thead>
	<tr class="">
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td colspan="7"><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($salescycles as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
		
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>


	<div id="won" class="tab-pane">
	<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-won">
	<thead>
	<tr class="">
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td colspan="7"><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($wons as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
		
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>

	<div id="lost" class="tab-pane">
		<table class="table table-responsive table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="deal-lost">
	<thead>
	<tr class="">
			<th>#</th>
			<th><?php echo __('Deal Name'); ?></th>
			<th><?php echo __('Contact Name'); ?></th>
			<th><?php echo __('Deal Stage'); ?></th>
			<th><?php echo __('Deal Value'); ?></th>
			<th><?php echo __('Estimated Close Date'); ?></th>
			<th><?php echo __('Created at'); ?></th>
			<!-- <th class="actions"><?php echo __('Actions'); ?></th> -->
	</tr>
	</thead>
	<tfoot>
			<tr>
		 		<td colspan="8"><input type="checkbox" id="selectall"/> <?php echo $this->Form->button(__('<i class="ace-icon fa fa-trash-o bigger-125"></i> Delete'),array('type'=>'submit','class'=>'btn btn-white','escape'=>false));?>	</td>
		 	</tr>
	</tfoot>
	<tbody>
	<?php foreach ($lost as $deal): ?>
	<tr class="">
		<td><?php echo $this->Form->checkbox('Deal.' . $deal['Deal']['id'],array('class'=>'checkbox1','value'=>$deal['Deal']['id'],'name'=>'data[Deal][id][]','hiddenField' => false));?></td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>">
			<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id'])); ?>
		</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
		<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id']?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
		<!-- <td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $deal['Deal']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $deal['Deal']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $deal['Deal']['id']), array(), __('Are you sure you want to delete # %s?', $deal['Deal']['id'])); ?>
		</td> -->
	</tr>
		<?php endforeach; ?>
	</tbody>
	</table>
	</div>
	

	</div>
	</div>
	</div>
	</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		  $(".clickableRow").click(function() {
            window.document.location = $(this).attr("href");
      });
		 $('#selectall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
	});
</script>
