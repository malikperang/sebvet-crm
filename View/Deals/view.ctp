<?php 
		// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-timepicker.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');
		

?>
<div class="row">
<div class="col-lg-12">
<div class="page-header">
		<h1 class="page-header-override">
			<?php echo $this->Html->link(__('Deal'),array('controller'=>'deals','action'=>'index'));?>			
				<i class="ace-icon fa fa-angle-double-right"></i>
			<?php echo h($deal['Deal']['name']); ?>
			&nbsp;	
			<div class="btn-group pull-right">
			<?php echo $this->Html->link('Won',array('plugin'=>false,'controller'=>'deals','action'=>'won',$deal['Deal']['id'],$userDetails['id']),array('class'=>'btn btn-primary '));?>
			<?php echo $this->Html->link('Lost',array('plugin'=>false,'controller'=>'deals','action'=>'lost',$deal['Deal']['id'],$userDetails['id']),array('class'=>'btn btn-danger'));?>	
			</div>	
			</div>
		</h1>
	</div>
</div>
<div class="row" >
	<div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
	<div class="row">
	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">
					<strong><?php echo __('Deal Details');?></strong>
				</span>
			</div>

			<div class="panel-body">
					<div class="profile-info-row">
						<div class="profile-info-name"> <?php echo __('Name');?> </div>

						<div class="profile-info-value override-font">
								<?php echo $this->Html->link($deal['Deal']['name'], array(),array('data-type'=>'text','id'=>'','class'=>'dealName')); ?>
&nbsp;

						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> <?php echo __('Value');?> </div>

						<div class="profile-info-value override-font">
							RM	<?php echo $this->Html->link($deal['Deal']['value'], array(),array('data-type'=>'text','id'=>'','class'=>'dealValue')); ?>
&nbsp;
						</div>
					</div>
					<div class="profile-info-row">
						<div class="profile-info-name"> <?php echo __('Stage');?> </div>

						<div class="profile-info-value override-font">
							<?php echo $this->Html->link($deal['DealStage']['name'], array('controller' => 'deal_stages', 'action' => 'view', $deal['DealStage']['id']),array('id'=>$deal['DealStage']['id'],'class'=>'stage')); ?>
&nbsp;
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> <?php echo __('Created at');?> </div>

						<div class="profile-info-value override-font">
							<?php echo date('d M Y H:i:s',strtotime($deal['Deal']['created']));?>
&nbsp;
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name override-font"> <?php echo __('Estimated Closing Date');?> </div>

						<div class="profile-info-value override-font">
						<?php echo date('d M Y H:i:s',strtotime(h($deal['Deal']['estimate_close_date']))); ?>
&nbsp;
&nbsp;
						</div>
					</div>


					<div class="profile-info-row">
						<div class="profile-info-name"> <?php echo __('Owner');?> </div>

						<div class="profile-info-value override-font">
							<?php echo $this->Html->link($deal['Owner']['name'],array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$deal['Owner']['id']));?>
&nbsp;
						</div>
					</div>
				</div>
			</div>
	</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">
					<strong><?php echo __('Associated Contact'); ?></strong>
				</span>
			</div>

			<div class="panel-body">
					<i class="fa fa-user"></i>
					<?php //echo $deal['Contact']['name'];?>
					 <?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'],$userDetails['id'])); ?>
			&nbsp;
				</div>
			</div>
		</div>

	
		</div>
		</div>

		<div class="col-lg-8 col-sm-12 col-md-12 col-xs-12">

			<div class="infobox infobox-red infobox-small infobox-dark">
						<div class="infobox-icon">
						<?php if(!empty($deal['Deal']['status'])):?>
							<?php if($deal['Deal']['status'] == 'won'): ?>
								<i class="ace-icon fa fa-trophy"></i>
							<?php else:?>
								<i class="ace-icon fa fa-thumbs-o-down"></i>
							<?php endif;?>
								<?php else:?>
								<i class="ace-icon fa fa-line-chart"></i>
							<?php endif;?>
							
						</div>
					
						<div class="infobox-data">
							<div class="infobox-content">STAGE</div>
							<div class="infobox-content">
							<?php if(!empty($deal['Deal']['status'])):?>
								<?php echo ucfirst($deal['Deal']['status']);?>
								<?php else:?>
								<?php echo $deal['DealStage']['name'];?>
							<?php endif;?>
							</div>
						</div>
					</div>
				
			<div class="infobox infobox-blue infobox-small infobox-dark">
				
				<div class="infobox-icon">
					<i class="ace-icon fa fa-money"></i>
				</div>

				<div class="infobox-data">
					<div class="infobox-content">VALUE</div>
					<div class="infobox-content"><?php echo $deal['Deal']['value'];?></div>
				</div>
			</div>

		<div class="space-10"></div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="panel-title">
					<i class="menu-icon fa fa-clock-o"></i>
						 Activities
					</span>
					<button type="btn" class="btn btn-sm btn-default pull-right" id="addActBtn">Add</button>
					<button type="btn" class="btn btn-sm btn-default pull-right" id="cancelActBtn">Cancel</button>
				</div>
				</div>

				<div id="actTab">
				<div class="tabbable">
					<ul class="nav nav-tabs" id="myTab">
					<li class="active">
								<a data-toggle="tab" href="#note" class="act-text">		
									<?php echo __('Add new activity');?>
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#attachment" class="act-text">
									<?php echo __('Attachment');?>
								</a>
							</li>
					</ul>
					</div>
						<div class="tab-content">
							<div id="note" class="tab-pane in active">	
						<?php echo $this->Form->create('Activity',array('controller'=>'activities','action'=>'add','type'=>'file','class'=>'','id'=>'activity-form'));?>

						
						<div class="row" >
						<div class="form-group">
						 <p class="title"><?php echo __('Left a note');?></p>
						<?php echo $this->Form->input('notes',array('class'=>'form-control','label'=>false,'div'=>false,'type'=>'textarea'))?>
						</div>
						</div>

							<div class="row">
						
						 <p class="title"><?php echo __('Schedule Activity');?></p>
							<div class="form-group">

							<div class="col-sm-2 ">
							Date
								<?php echo $this->Form->input('start_date',array(
									'class'=>'form-control date-picker','type'=>'text',
									'div'=>false,'label'=>false,'data-date-format'=>'yyyy-mm-dd'
								))?>
							</div>

							<div class="col-sm-2 ">
							Due Date
								<?php echo $this->Form->input('due',array(
									'class'=>'form-control date-picker','type'=>'text',
									'div'=>false,'label'=>false,'data-date-format'=>'yyyy-mm-dd'
								))?>
							</div>

							<div class="col-sm-2 ">
							<?php echo __('Location');?>
								<?php echo $this->Form->input('location',array(
									'class'=>'form-control','type'=>'text',
									'div'=>false,'label'=>false,'placeholder'=>'Optional'
								))?>
							</div>

							<div class="col-sm-2 ">
							<?php echo __('Time');?>
								<?php echo $this->Form->input('event_time',array(
								'class'=>'form-control','type'=>'text',
									'id'=>'timepicker',
									'div'=>false,'label'=>false,'placeholder'=>'Optional'
								))?>
							</div>


								<div class="col-sm-2 ">
								Assigned To
								<?php echo $this->Form->input('user_id',array('class'=>'form-control','label'=>false,'div'=>false,'empty'=>'Optional'))?>
							</div>
								
						</div>
						</div>
					

						<?php echo $this->Form->input('deal_id',array('type'=>'hidden','value'=>$deal['Deal']['id']));?>
						<?php echo $this->Form->input('created_by',array('type'=>'hidden','value'=>$userDetails['id']));?>
						<?php echo $this->Form->button('Submit',array('class'=>'btn btn-primary btn-sm pull-right act-btn','escape'=>false,'data-loading-text'=>'loading..'));?>
					
							</div>

							<div id="attachment" class="tab-pane">	
						
							 <label><?php echo __('Add an Attachment');?></label>
						
							<?php echo $this->Form->input('attachment',array('class'=>'form-control','label'=>false,'type'=>'file','multiple'=>false))?>
							<?php echo $this->Form->button('<i class="fa fa-upload"></i> Submit',array('class'=>'btn btn-sm btn-primary pull-right act-btn','escape'=>false,'data-loading-text'=>'loading..'));?>
							<?php echo $this->Form->end();?>
							</div>
				</div>
				</div>

						<div class="space-10"></div>
						<div class="space-10"></div>

					<div class="panel panel-default">
					  <div class="panel-heading">
						<span class="panel-title">
							<i class="fa fa-list"></i>
							Feed
						</span>
						</div>
						<div class="panel-body">
							<?php foreach($activities as $activity):?>
								<div class="profile-activity clearfix">
										
								<?php echo $this->Html->link($activity['User']['name'],array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$activity['User']['id']),array('class'=>'user'));?>
							
								<br />
								<?php echo $activity['Activity']['notes'];?>
								<div class="time pull-right override-font">
									<i class="ace-icon fa fa-clock-o bigger-110"></i>
									<?php echo date('d/m/Y H:i:s',strtotime($activity['Activity']['created']));?>
								</div>
								<br />

								<?php foreach ($activity['Attachment'] as $attachment) :?>
									<?php if(!empty($attachment['name'])):?>							
									<?php 
										$imageTypes = array('image/gif', 'image/jpeg', 'image/png');
										$fileTypes = array(
												'application/pdf', 
												'application/msword', 
												'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
												'application/vnd.ms-excel',
												'application/vnd.ms-powerpoint');
									?>
									<?php if(in_array($attachment['type'], $imageTypes)):?>
									<?php echo $this->Html->image('../attachments/'.$attachment['url'],array('fullBase'=>true,'class'=>'editable img-responsive'));?>
									<?php elseif(in_array($attachment['type'], $fileTypes)):?>
										<a href="<?php echo $this->webroot;?>attachments/<?php echo $attachment['url'];?>"><?php echo $attachment['name'];?></a>
									<?php endif;?>
								<?php endif;?>
								<?php endforeach;?>
							
								</div>
							<?php endforeach;?>
								</div>
							</div>
							</div>
					</div>
						</div>



						</div>
			<div class="space-20"></div>

			<div class="row">
			<div class="col-lg-6">
			
					</div>
				</div>
<script>
$(function(){
   
});
</script>
 <script type="text/javascript">
jQuery(document).ready(function($) {
	$('[data-rel=tooltip]').tooltip();
	$('#actTab').hide();
	$('#cancelActBtn').hide();
	$('#addActBtn').click(function(event) {
		/* Act on the event */
		$('#cancelActBtn').show();
		$('#actTab').show();
		$(this).hide();

	});
	$('#cancelActBtn').click(function(event) {
		/* Act on the event */
		$('#addActBtn').show();
		$('#cancelActBtn').hide();
		$('#actTab').hide();

	});

	//activities submit
	$("#activity-form").on('submit',function(e) {
		e.preventDefault();
		
		$.ajax({
			url: '<?php echo $this->Html->url(array("controller"=>"activities","action" => "add")); ?>', // Url to which the request is send
			type: 'POST',             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				location.reload();
			}
			})
		});
		
	var stagelist = $.ajax({
          url: "<?php echo $this->webroot;?>deal_stages/api.json",
          dataType:"json",
          async: false
          }).responseText;
	
	stagelist = JSON.parse(stagelist);
	
    $.fn.editable.defaults.mode = 'inline';     
    
  	var value = $('.stage').attr('id');
 	var dealName =  $('.dealName').text();
 	var dealValue =  $('.dealValue').text();
    console.log(dealValue);
    $('.dealName').editable({
    	
        type: 'text',
        title: 'Deal Name',
        placement: 'right',
        value : dealName,
        url : '<?php echo $this->webroot;?>deals/changeDealName',
        pk:'<?php echo $deal["Deal"]["id"];?>',
        success :function(message){
			    	// alert('success');
			    	// console.log(message);
	                location.reload();
        },
      
    });

       //make status editable
    $('.dealValue').editable({
    	
        type: 'text',
        title: 'Deal Value',
        placement: 'right',
        value : dealValue,
        url : '<?php echo $this->webroot;?>deals/changeDealValue',
        pk:'<?php echo $deal["Deal"]["id"];?>',
        success :function(message){
			    	// alert('success');
			    	// console.log(message);
	                location.reload();
        },
      
    });

    //make status editable
    $('.stage').editable({
    	
        type: 'select',
        title: 'Select stage',
        placement: 'right',
        value :value,
        url : '<?php echo $this->webroot;?>deals/changeStage',
        pk:'<?php echo $deal["Deal"]["id"];?>',
        source: stagelist,
        success :function(message){
			    	// alert('success');
			    	// console.log(message);
	                location.reload();
        },
      
    });


    //date picker
    $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
	});


	//time picker		
	$('#timepicker').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

			


	 $(".select_box").chosen({
	  	width:"100%",
	    no_results_text: "Oops, nothing found!",
	    
	    search_contains:true
  	});
	 $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

});
 </script>