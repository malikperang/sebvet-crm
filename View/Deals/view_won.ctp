<?php //debug($this->params);?>
<div class="row">
<div class="col-lg-12">
<div class="page-header">
		<h1>
		<i class="fa fa-trophy"></i>
			<?php echo h('Won Deals'); ?>
			&nbsp;	
		</h1>

	</div>
</div>
<div class="row  no-margin-right">
<div class="col-lg-12">
<?php echo $this->Form->create('Deal',array('url'=>array('plugin'=>false,'controller'=>'deals','action'=>'search'),'type'=>'post'));?>
		<?php echo $this->Form->input('deal_id', array(
			'class'=>'form-control select_box',
			// 'placeholder' => 'Username',
			'label' => false,
			'beforeInput' => '<div class="input-group"><span class="input-group-addon"><i class="ace-icon fa fa-search"></i></span>',
			'onchange'=>'this.form.submit()',
			'afterInput' => '</div>','empty'=>'Find a deal..','options'=>$lists
		)); ?>
	<?php echo $this->Form->end();?>
</div>
</div>
<br />
<div class="row no-margin-right">
<div class="col-lg-12" >

<div class="panel panel-default">

<div class="table-responsive">
	<table class="table table-bordered table-hover table-responsive" >
		<thead>
			<tr>
				<tr class="">
						<th><?php echo __('Deal Name'); ?></th>
						<th><?php echo __('Contact Name'); ?></th>
						<th><?php echo __('Deal Value (RM)'); ?></th>
						<th><?php echo __('Scheduled Activity Date'); ?></th>
						<th><?php echo __('Created at'); ?></th>
						<th><?php echo __('Action'); ?></th>
				</tr>
			</tr>
		</thead>
		<tbody>
		<?php foreach($deals as $deal):?>
			<tr class="">
				<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['Deal']['name']); ?>&nbsp;</td>
				<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id'];?>/<?php echo $userDetails['id'];?>">
					<?php echo $this->Html->link($deal['Contact']['name'], array('controller' => 'contacts', 'action' => 'view', $deal['Contact']['id'],$userDetails['id'])); ?>
				</td>
				<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['Deal']['value']); ?>&nbsp;</td>
				<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['Deal']['estimate_close_date']); ?>&nbsp;</td>
				<td class="clickableRow" href="<?php echo $this->webroot;?>deals/view/<?php echo $deal['Deal']['id'];?>/<?php echo $userDetails['id'];?>"><?php echo h($deal['Deal']['created']); ?>&nbsp;</td>
				<td><?php echo $this->Form->postLink(__('<i class="fa fa-trash"></i>'), array('action' => 'delete', $deal['Deal']['id']), array('class'=>'btn btn-default btn-sm','escape'=>false), __('Are you sure you want to delete this deal?')); ?></td>
			</tr>
		<?php endforeach;?>
		</tbody>
</table>

</div>
 
</div>
<?php if($this->Paginator->counter(array('format' => '%count%')) > 1):?>
<p>
     <?php
     // echo $this->Paginator->counter(array(
     // 'format' => __('<h1>Total deal : {:count} </h1>')
     // ));
     ?>     </p>
     <ul class="pagination pagination pagination-right">
     <li><?php
          echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
     ?>
     </li><li><?php
          echo $this->Paginator->numbers(array('separator' => ''));
     ?>
     </li><li><?php
          echo $this->Paginator->next(__('') . ' >>', array(), null, array('class' => 'next disabled'));
     ?>
     </li>    
     </ul>
 
</div>
<?php endif;?>

</div>
<div class="col-lg-6">
</div>
</div>
<script type="text/javascript">
	
jQuery(document).ready(function($) {
	
	   $(".clickableRow").click(function() {
	   	  window.document.location = $(this).attr("href");
          
      });
	  $(".select_box").chosen({
		  	width:"100%",
		    no_results_text: "Oops, nothing found!",
		    
		    search_contains:true
	  	});
	  $('[data-toggle="tooltip"]').tooltip();	
});
</script>