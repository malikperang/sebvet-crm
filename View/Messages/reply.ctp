<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<?php 
// debug($sender);
?>
<div class="page-header">
    <h1 style="margin-left:3px;">
        <i class="fa fa-envelope"></i> <?php echo __('Reply Message');?>
    </h1>
</div>
<div class="row" >
    <div class="col-xs-12 col-lg-10" style="margin-left:15px;margin-top:-15px;border:1px solid #ddd;padding:10px;border-radius:5px;">
	<?php echo $this->Form->create('Message',array('type'=>'file','action'=>'add')); ?>
	<fieldset>
		<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="title"> Recipient</label>
			<?php echo $this->Form->input('user_id',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
	</div>
	
	<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right no-padding-left"> Subject</label>
	<?php echo $this->Form->input('subject',array('div'=>false,'label'=>false,'class'=>'form-control'));?>
	</div>
 
	<div class="form-group">
	<?php echo $this->Form->input('content',array('type'=>'textarea','rows'=>20,'div'=>false,'label'=>false,'class'=>'form-control','id'=>'editor1'));?>
	</div>
	<div class="form-group  input_fields_wrap">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="attachments"> Attachments</label>
		<?php echo $this->Form->input('file.', array('type' => 'file', 'multiple' => true,'class'=>'form-control'));?>
		<p class="help-block">Can upload more than 1 file. Supported file type : (pdf,xls,doc,ppt)</p>
	</div>
	
	</fieldset>
	
	<div class="text-center">
		<?php echo $this->Form->button('Send',array('class'=>'btn btn-primary','type'=>'submit'));?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		CKEDITOR.replace( 'editor1' );	
	});

</script>