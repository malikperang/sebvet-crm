<div id="inbox">
    <div class="message-container">
        <div id="id-message-new-navbar" class="hide message-navbar clearfix">
            <div class="message-bar">
            </div>

            <div>
                <div class="messagebar-item-left">
                    <a href="#" class="btn-back-message-list">
                        <i class="ace-icon fa fa-arrow-left bigger-110 middle blue"></i>
                        <b class="middle bigger-110">Back</b>
                    </a>
                </div>

                <div class="messagebar-item-right">
                    <span class="inline btn-send-message">
                        <button type="button" class="btn btn-sm btn-primary no-border btn-white btn-round">
                            <span class="bigger-110">Send</span>

                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>

                    
</div><div class="message-content" id="id-message-content">

<div class="message-header clearfix">
<div class="pull-left">
    <span class="blue bigger-125"> </span>

    <div class="space-4"></div>
    &nbsp;
    <?php if(empty($user['User']['profile_picture'])):?>

    <?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'middle','width'=>32)); ?>
    <?php else:echo $this->Html->image('uploads'.DS.$user['Sender']['profile_picture'],array('fullBase'=>true,'class'=>'middle','width'=>32));endif;?>
    &nbsp;
    <i class="ace-icon fa fa-clock-o bigger-110 orange middle"></i>
    <span class="time grey"><?php echo date('d M Y H:i:s',strtotime($message['Message']['created']));?></span>
    
     <p class="sender">From : <?php echo $message['Sender']['name'];?></p>
  <p class="sender">Recipient : <?php echo $message['Recipient']['name'];?></p>

</div>

<div class="pull-right action-buttons">
    <?php echo $this->Html->link('Reply',array('controller'=>'messages','action'=>'reply',$message['Sender']['id']));?>

    <?php echo $this->Form->postLink('<i class="ace-icon fa fa-trash-o red icon-only bigger-130"></i>', array('action' => 'delete', $message['Message']['id']), array('escape'=>false), __('Are you sure you want to delete this message?')); ?>
</div>
</div>
<hr>

<p>Subject : <?php echo $message['Message']['subject'];?></p>
Message: <?php echo $message['Message']['content'];?>
<?php if(!empty($attachments)):?>
<div class="message-attachment clearfix">
<div class="attachment-title">
    <span class="blue bolder bigger-110">Attachments :</span>
    &nbsp;
</div>

&nbsp;

<ul class="attachment-list pull-left list-unstyled">
<?php foreach ($attachments as $attachment):?>
    <li>
        <a href="<?php echo $this->webroot.'attachments/'.$attachment['Attachment']['url'];?>" class="attached-file">
            <i class="ace-icon fa fa-file-o bigger-110"></i>
            <span class="attached-name"><?php echo $attachment['Attachment']['name'];?></span>
        </a>
    </li>
<?php endforeach;?>
</ul>
<?php endif;?>


</div>
</div>
<div class="message-footer clearfix hide">
  
</div>

</div>
</div>