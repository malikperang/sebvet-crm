<?php //debug($messages);?>
<div class="page-header">
	<h1>
		Sentbox
		<?php echo $this->Html->link('Write Message',array('controller'=>'messages','action'=>'add'),array('class'=>'btn btn-primary pull-right'));?>
	</h1>
	<p>Track your sent messages</p>
</div>
<div class="row" style="margin-left:-12px">
    <div class="col-xs-12 col-lg-12" style="margin-top:-30px">
    <div class="panel panel-default">
		<table class="table table-striped table-bordered table-hover message-table">
		<thead class="thin-border-bottom">
		<tr>
			<th>Sender</th>
			<th>To</th>
			<th>Message</th>
			<th>Updated</th>
		</tr>
			
		</thead>
		<tbody>
			<?php foreach ($messages as $message):?>
				<?php //debug($message);?>

				<tr class="clickableRow" href="<?php echo $this->webroot;?>messages/view/<?php echo $message['Message']['id'].DS.$userDetails['id'];?>">
					<td class="col-lg-1"> <?php echo $message['Sender']['name'];?></td>
					<td class="col-lg-1"> <?php echo $message['Recipient']['name'];?></td>
					<td class="col-lg-4" > 
					<div class="crop">
						 <?php echo $message['Message']['content'];?>
					</div>			
					</td>
					<td class="col-lg-2">  <?php echo date('d M Y H:i:s',strtotime($message['Message']['created']));?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		</div>
	</div>
</div>
	<?php if(empty($messages)):?>
			<div class="alert alert-info" role="alert">
		      <strong>Heads up!</strong> No message in your sentbox.
		    </div>
		<?php endif;?>
</div>
<script type="text/javascript">
$(".clickableRow").click(function() {
    window.document.location = $(this).attr("href");
	});
</script>