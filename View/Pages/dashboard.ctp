  <div class="page-header">
    <h1>
      <?php echo __('Sales Performance');?>
      <small>
        <i class="ace-icon fa fa-angle-double-right"></i>
      </small>
    </h1>
  </div>

  <div class="row">
            <div class="col-sm-4">
                  <div class="widget-box">
                    <div class="widget-header">
                      <h4 class="widget-title lighter smaller">
                      
                        <?php echo __('New Opportunity');?>
                      </h4>
                    </div>

                    <div class="widget-body">
                      <div class="widget-main no-padding">
                            <h1 class="center"><?php echo $newDeals;?></h1>
                      </div>
                    </div>
                  </div>
                </div>
                 <div class="col-sm-4">
                  <div class="widget-box">
                    <div class="widget-header">
                      <h4 class="widget-title lighter smaller">
                    
                        Follow Up
                      </h4>
                    </div>

                    <div class="widget-body">
                      <div class="widget-main no-padding">
                       <h1 class="center"><?php echo $followUp;?></h1>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="col-sm-4">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                       <?php echo __('Total Sales');?>
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main no-padding">
                    <h1 class="text-center"><?php echo $totalWonDeal;?></h1>

                    </div>
                  </div>
                </div>
              </div>
                <div class="col-sm-4">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                       <?php echo __('Total Sales');?>
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main no-padding">
                    <h1 class="text-center"><?php echo $totalWonDeal;?></h1>

                    </div>
                  </div>
                </div>
              </div>
                <div class="col-sm-4">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                       <?php echo __('Total Sales');?>
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main no-padding">
                    <h1 class="text-center"><?php echo $totalWonDeal;?></h1>

                    </div>
                  </div>
                </div>
              </div>
                <div class="col-sm-4">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                       <?php echo __('Total Sales');?>
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main no-padding">
                    <h1 class="text-center"><?php echo $totalWonDeal;?></h1>

                    </div>
                  </div>
                </div>
              </div>
                <div class="col-sm-4">
                <div class="widget-box">
                  <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                       <?php echo __('Total Sales');?>
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main no-padding">
                    <h1 class="text-center"><?php echo $totalWonDeal;?></h1>

                    </div>
                  </div>
                </div>
              </div>
  </div>