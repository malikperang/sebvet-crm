<?php
	// <!-- page specific plugin styles -->
		echo $this->Html->css('jquery-ui.custom.min');
		echo $this->Html->css('datepicker');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('bootstrap-timepicker');

		echo $this->Html->script('jquery-ui.custom.min');
		echo $this->Html->script('jquery.ui.touch-punch.min');
		echo $this->Html->script('date-time/moment.min');
		echo $this->Html->script('date-time/bootstrap-timepicker.min');
		echo $this->Html->script('date-time/bootstrap-datepicker.min');
		echo $this->Html->script('date-time/bootstrap-datetimepicker.min');


?>

<div class="page-header">
		<h1>
			<?php echo __('Edit sales target');?>
		</h1>

		<p class="font-14">Set your sales target here to display it on the MTD Chart</p>
	</div>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-body">
<?php echo $this->Form->create('SalePerformance'); ?>
	<fieldset>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="title"> Sales Target</label>
	<?php echo $this->Form->input('target',array('class'=>'form-control','label'=>false,'div'=>false));?>
	</div>
	<?php 
	$month = array(
		'1'=>'January',
		'2'=>'February',
		'3'=>'March',
		'4'=>'April',
		'5'=>'Jun',
		'6'=>'July',
		'7'=>'August',
		'8'=>'September',
		'9'=>'October',
		'10'=>'November',
		'11'=>'December',
		);
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right no-padding-left" for="title"> Set Month & Year </label>
			<?php echo $this->Form->input('datetime',array(
									'class'=>'form-control date-picker','type'=>'text',
									'div'=>false,'label'=>false,'data-date-format'=>'yyyy-mm-dd'
								))?>
	</div>

	</fieldset>
	<?php echo $this->Form->button('Submit',array('class'=>'btn btn-primary'));?>
<?php echo $this->Form->end(); ?>
</div>
</div>
</div>
</div>

			<script type="text/javascript">
			jQuery(function($) {
								
				$('#timepicker').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

				 $('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
	});


				 $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			});
</script>

