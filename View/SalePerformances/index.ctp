	<?php //debug($salePerformances);?>
	<div class="page-header">
		<h1>
			<?php echo __('Sale Performance');?>
			<button class="btn btn-default" type="btn btn-default">
				<?php echo $this->Html->link('New Sales Target',array('controller'=>'sale_performances','action'=>'add'),array('class'=>'dark'));?>
			</button>
			
		</h1>

		<p class="font-14">Sales target history</p>
	</div>
<div class="row">
<div class="col-lg-12 col-sm-12" style="margin-top:-10px;">
<div class="panel panel-default">
	<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover message-table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('target'); ?></th>
			<th><?php echo $this->Paginator->sort('achievement'); ?></th>
			<th><?php echo $this->Paginator->sort('month','Month'); ?></th>
			<th><?php echo $this->Paginator->sort('year','Year'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Updated'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($salePerformances as $salePerformance): ?>
	<tr>
	<?php 
		$monthNum  = $salePerformance['SalePerformance']['month'];
		$monthName = date('F', mktime(0, 0, 0, $monthNum, 10)); // March

	?>
		<td><?php echo h($salePerformance['SalePerformance']['target']); ?>&nbsp;</td>
		<td><?php echo h($salePerformance['SalePerformance']['achievement']); ?>&nbsp;</td>
		<td><?php echo $monthName; ?>&nbsp;</td>
		<td><?php echo date('Y',strtotime(h($salePerformance['SalePerformance']['year']))); ?>&nbsp;</td>
		<td><?php echo h($salePerformance['SalePerformance']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group">
			
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $salePerformance['SalePerformance']['id']),array('class'=>'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $salePerformance['SalePerformance']['id']), array('class'=>'btn btn-default'), __('Are you sure you want to delete # %s?', $salePerformance['SalePerformance']['id'])); ?>
			</div>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

</div>
<p>
     <?php
     echo $this->Paginator->counter(array(
     'format' => __('{:count} record.')
     ));
     ?>     </p>
     <ul class="pagination pagination pagination-right">
     <li><?php
          echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
     ?>
     </li><li><?php
          echo $this->Paginator->numbers(array('separator' => ''));
     ?>
     </li><li><?php
          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
     ?>
     </li>    
     </ul>
</div>