<div class="salePerformances view">
<h2><?php echo __('Sale Performance'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Target'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['target']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Achievement'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['achievement']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['created_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified By'); ?></dt>
		<dd>
			<?php echo h($salePerformance['SalePerformance']['modified_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Performance'), array('action' => 'edit', $salePerformance['SalePerformance']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Performance'), array('action' => 'delete', $salePerformance['SalePerformance']['id']), array(), __('Are you sure you want to delete # %s?', $salePerformance['SalePerformance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Performances'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Performance'), array('action' => 'add')); ?> </li>
	</ul>
</div>
