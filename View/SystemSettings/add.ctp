<div class="page-header">
<h1> <?php echo __('Configuration');?></h1>
	
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
	<div class="panel panel-default">
    <div class="panel-body">
<?php echo $this->Form->create('SystemSetting',array('role'=>'form','type'=>'file')); ?>
	<fieldset>

	<?php echo $this->Form->input('created_by',array('type'=>'hidden','value'=>$userDetails['id']));?>
 	<div class="form-group">
       <label class="control-label"><?php echo __('Name');?></label>
        <div class="control-group">
	<?php echo $this->Form->input('name',array('label'=>false,'class'=>'col-lg-2 form-control','div'=>false));?>
	</div>
	</div>

	<div class="form-group">
     <label class="control-label"><?php echo __('Login Logo');?></label> 
       <div class="control-group"> 
	<?php echo $this->Form->input('login_logo',array('label'=>false,'class'=>'col-lg-2 form-control','div'=>false,'type'=>'file'));?>
	<p class="help-block">Recommended size </p>
	</div>
	</div>

	<br /><br />
	<div class="text-center">
        <?php echo $this->Form->button(__('<i class="fa fa-floppy-o"></i>  Submit'), array('class'=>'btn btn-primary center-block', 'div'=>false,'escape'=>false,'type'=>'submit'));?>     
    </div>
		
		
	</fieldset>

<?php echo $this->Form->end(); ?>
</div>
</div>
</div>