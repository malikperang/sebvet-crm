<div class="systemSettings form">
<?php echo $this->Form->create('SystemSetting'); ?>
	<fieldset>
		<legend><?php echo __('Edit System Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('logo');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SystemSetting.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SystemSetting.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List System Settings'), array('action' => 'index')); ?></li>
	</ul>
</div>
