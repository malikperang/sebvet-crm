<?php 
$options = array();
$myCalendar = array();
foreach ($list as $calendar) {
    $options[$calendar->id] = $calendar->summary;
    $myCalendar[] = $calendar->id;
}

?>


<div class="page-header">
<h1> <?php echo __('Google Calendar Setting');?></h1>
    <p>Here you can connect your Google Calendar to your xRM account.</p>
	
</div>
<div class="row setting-row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
	<div class="panel panel-default">
    <div class="panel-body">

    	<p>Please choose which google calendar you want to use with Sebvet xRM account</p>
        <?php echo $this->Form->create('SystemSetting',array('action'=>'googleCalendar'));?>
       

    	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1">Sync with the following calendar</label>
		<div class="col-sm-8">
    	<?php
    	echo $this->Form->input('calendar_id',array('options'=>$options,'class'=>'form-control','div'=>false,'label'=>false));

    	?>
    	</div>
    	</div>
    	<div class="clearfix">
			<div class="col-md-offset-5 col-md-9">
			<?php echo $this->Form->button('Save',array('class'=>'btn btn-default center-block','type'=>'submit'));?>
			</div>
			</div>
        <?php echo $this->Form->end();?>
    </div>
    </div>
    </div>
    </div>