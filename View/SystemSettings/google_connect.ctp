<div class="page-header">
<h1> <?php echo __('Set Connections');?></h1>
	<p>Here you can connect your Sebvet xRM account with google services for integrations.</p>
</div>
<div class="row setting-row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
	<div class="panel panel-default">
    <div class="panel-body">

    <img src="http://img3.wikia.nocookie.net/__cb20100520131746/logopedia/images/5/5c/Google_logo.png" class="img-responsive">
    <?php if(!empty($google_user)):?>
    <strong>✓ Connected as <?php echo $google_user['GoogleUser']['google_email'];?>.	</strong>
    <p>Now configure the following services</p>
    <ul>
    	<li><?php echo $this->Html->link('Google Contact Sync',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleContact'));?></li>
    	<li><?php echo $this->Html->link('Google Calendar Sync',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleCalendar'));?></li>
    </ul>
    <?php echo $this->Form->create('SystemSetting',array('action'=>'googleConnect'));?>
    <?php echo $this->Form->button('Disconnect',array('class'=>'btn btn-danger','name'=>'disconnect'));?>
    <?php echo $this->Form->end();?>

	<?php else:?>
    <?php echo $this->Form->create('SystemSetting',array('action'=>'googleConnect'));?>
    <?php echo $this->Form->button('Connect',array('class'=>'btn btn-primary','name'=>'connect'));?>
    <?php echo $this->Form->end();?>
	<?php endif;?>
</div>
</div>
</div>
</div>
