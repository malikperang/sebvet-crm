<script type="text/javascript">
    function reload(){
        location.reload();
    }

</script>


<?php
$options = array();
$groups = array();
foreach ($contactsGroup['feed']['entry'] as $option) {
  
    $id = $option['id']['$t'];
    $title =  str_replace('System Group:', '', $option['title']['$t']);
    $options[$id] = $title;
    $groups[$id][] = $title;
}

?>



<div class="page-header">
<h1> <?php echo __('Google Contacts');?></h1>
    <p>Here you can connect your Google Contacts to your xRM account.</p>
	
</div>
<div class="row setting-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
    <div class="panel panel-default">
    <div class="panel-body">
  
        <?php echo $this->Form->create('SystemSetting',array('action'=>'googleContact'));?>
          <?php if (isset($usedGroup)) :
             foreach ($groups as $key => $group):
                if ($key == $usedGroup) :?>
                <p>You already sync your Sebvet xRM Contacts with this group :<strong> <?php echo $group[0]; ?></strong></p>
           <?php endif;
          endforeach;
        endif;
        ?>
    	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1">Sync with the following contacts group</label>
		<div class="col-sm-8">
    	<?php
    	echo $this->Form->input('contact_id',array('options'=>$options,'class'=>'form-control','div'=>false,'label'=>false));
    	?>
    	</div>
    	</div>
    	<div class="clearfix">
			<div class="col-md-offset-5 col-md-9">
			<?php echo $this->Form->button('Save',array('class'=>'btn btn-default','type'=>'submit'));?>
			</div>
			</div>
        <?php echo $this->Form->end();?>
    </div>
    </div>
    </div>
    </div>