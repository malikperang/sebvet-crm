<div class="systemSettings index">
	<h2><?php echo __('System Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('logo'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('created_by'); ?></th>
			<th><?php echo $this->Paginator->sort('modified_by'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($systemSettings as $systemSetting): ?>
	<tr>
		<td><?php echo h($systemSetting['SystemSetting']['id']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['name']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['logo']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['created']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['modified']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['created_by']); ?>&nbsp;</td>
		<td><?php echo h($systemSetting['SystemSetting']['modified_by']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $systemSetting['SystemSetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $systemSetting['SystemSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $systemSetting['SystemSetting']['id']), array(), __('Are you sure you want to delete # %s?', $systemSetting['SystemSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New System Setting'), array('action' => 'add')); ?></li>
	</ul>
</div>
