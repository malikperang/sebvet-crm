<div class="row">
<div class="page-header">
		<h1>
			<?php echo __('Customize Deal Stages');?>
			
			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#addStage">
			  Add Stage
			</button>

		</h1>
	</div>
	<div class="col-sm-12 col-lg-12" style="border:1px solid #ddd;padding:10px;background:#ccc;border-radius:4px;" >
		<div class="tabbable">
		<ul class="nav nav-tabs" id="myTab" >
		<?php foreach($dealStages as $stage):?>
			<li>

					<a  data-toggle="modal" data-target="#editStage" data-stageid="<?php echo $stage['DealStage']['id'];?>" data-stagename="<?php echo $stage['DealStage']['name'];?>" >		
						<?php echo __($stage['DealStage']['name']);?>

					</a>

			</li>

		<?php endforeach;?>
				
		</ul>
	

	</div>
	</div>
	</div>
	</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="addStage" tabindex="-1" role="dialog" aria-labelledby="pipeline" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Stage</h4>
      </div>
      <div class="modal-body">
			<?php echo $this->Form->create('DealStage',array('controller'=>'dealStages','action'=>'add')); ?>
				<fieldset>
				<?php
					echo $this->Form->input('name',array('class'=>'form-control','label'=>'Stage Name'));
					echo $this->Form->input('sort',array('type'=>'hidden'));
					echo $this->Form->input('company_id',array('type'=>'hidden','value'=>$userDetails['company_id']));
					echo $this->Form->input('created_by',array('type'=>'hidden','value'=>$userDetails['id']));
					echo $this->Form->input('modified_by',array('type'=>'hidden','value'=>$userDetails['id']));
				?>
				</fieldset>
			
      </div>
      <div class="modal-footer">
      	<?php echo $this->Form->button('Add',array('class'=>'btn btn-success center-block','type'=>'submit'));?>
     	 <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editStage" tabindex="-1" role="dialog" aria-labelledby="editStageLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editStageLabel"></h4>
      </div>
      <div class="modal-body">
      		<?php echo $this->Form->create('DealStage',array('controller'=>'dealStages','action'=>'edit')); ?>
				<fieldset>
				<?php
					echo $this->Form->input('id',array('class'=>'form-control','id'=>'DealStageId'));
					echo $this->Form->input('name',array('class'=>'form-control','id'=>'DealStageName'));
					echo $this->Form->input('sort',array('type'=>'hidden','id'=>'DealStageSort'));
					echo $this->Form->input('company_id',array('type'=>'hidden','value'=>$userDetails['company_id']));
					echo $this->Form->input('created_by',array('type'=>'hidden','value'=>$userDetails['id']));
					echo $this->Form->input('modified_by',array('type'=>'hidden','value'=>$userDetails['id']));
				?>
				</fieldset>
 
      </div>
      <div class="modal-footer">
       	<?php echo $this->Form->button('Update',array('class'=>'btn btn-success','type'=>'submit'));?>
     	<?php echo $this->Form->end(); ?>
    	<?php echo $this->Form->create('DealStage',array('controller'=>'dealStages','action'=>'delete')); ?>
    	<?php echo $this->Form->input('id',array('class'=>'form-control','id'=>'DeleteStage'));?>
    	<?php echo $this->Form->button('Delete',array('class'=>'btn btn-danger stage-delete','type'=>'submit'));?>
    	<?php echo $this->Form->end(); ?>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#editStage').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget) 
		  var stageName = button.data('stagename')
		  var stageId = button.data('stageid')
		
		  var modal = $(this)
		  modal.find('.modal-title').text('Edit stage : ' + stageName)
		  modal.find('.modal-body #DealStageName').val(stageName)
		  modal.find('.modal-body #DealStageId').val(stageId)
		   modal.find('.modal-footer #DeleteStage').val(stageId)
		})

		$('.stage-delete').click(function(event) {
			alert('Are you sure you want to delete this stage? All deal under this stage will be trash too.');
		});
	});

</script>