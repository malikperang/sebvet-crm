<div class="alert alert-success">
<i class="fa fa-check"></i>
<button type="button" class="close" data-dismiss="alert">&times;</button>
<?php echo $message; ?>
</div>