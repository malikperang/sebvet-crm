<?php

$current_plugin = $this->params['plugin'];
$current_page = $this->params['action'];

?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project Name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">This</a></li>
            <li><a href="#">is Admin</a></li>
            <li><a href="#">Panel</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>