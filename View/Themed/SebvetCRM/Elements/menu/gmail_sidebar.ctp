<style type="text/css">
	.gmail-li a{
		min-height: 70px;
		background-color: #fff !important;
	}
	.gmail-logo{
		height: 60px;
	}
</style>
<div id="sidebar" class="sidebar navbar-collapse collapse  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

			
	<ul class="nav nav-list">
		<li class="gmail-li">
			<a class="text-center">
			<!-- <img src="http://www.userlogos.org/files/logos/sjdvda/gmail2.png" style="" class="gmail-logo"> -->
			<?php echo $this->Html->image('mail_logo_rgb_web.png',array('fullBase'=>true,'class'=>'gmail-logo'));?></a>	
		</li>
		<li class="">
			<?php echo $this->Html->link('<i class="menu-icon fa fa-inbox"></i> Inbox',array('plugin'=>'google_app','controller'=>'gmail','action'=>'index'),array('escape'=>false));?>
			</li>
		<li class="">
			<?php echo $this->Html->link('<i class="menu-icon fa fa-paper-plane"></i> Sent Mail',array('plugin'=>'google_app','controller'=>'gmail','action'=>'viewSendMail'),array('escape'=>false));?>
			</li>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>

	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>