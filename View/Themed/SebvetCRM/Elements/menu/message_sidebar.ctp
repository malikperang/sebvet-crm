<div id="sidebar" class="sidebar navbar-collapse collapse  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

			
	<ul class="nav nav-list">
		<li class="">
			<?php echo $this->Html->link('<i class="menu-icon fa fa-inbox"></i> Inbox',array('plugin'=>false,'controller'=>'messages','action'=>'index'),array('escape'=>false));?>
			</li>
		<li class="">
			<?php echo $this->Html->link('<i class="menu-icon fa fa-paper-plane"></i> Sent',array('plugin'=>false,'controller'=>'messages','action'=>'viewSent'),array('escape'=>false));?>
			</li>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>

	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>