<?php

	$current_page = $this->params['action'];
	//debug($this->params['controller']);

?>
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					
				</div>

				<ul class="nav nav-list" style="top: 0px;">

					<li class="<?php echo $this->params['controller'] == 'deals' ? 'active':null?> hover hsub">
						<?php echo $this->Html->link('<i class="menu-icon fa fa-money"></i>
							<span class="menu-text"> Deals </span>',array('plugin'=>false,'controller'=>'deals','action'=>'index'),array('escape'=>false));?>
							<b class="arrow fa fa-angle-down"></b>
					</li>



				<!-- 	<li class="<?php echo ($this->params['controller'] == 'leads' ? 'active':null); ?> hover hsub">
							<?php echo $this->Html->link('<i class="menu-icon fa fa-heart"></i>
							<span class="menu-text"> Leads </span>',array('plugin'=>false,'controller'=>'leads','action'=>'index'),array('escape'=>false));?>
							<b class="arrow fa fa-angle-down"></b>
					</li>
 -->
				<!-- 	<li class="<?php echo ($this->here == '/my/crm/leads/opportunities' ? 'active':null) ?> hover hsub">
							<?php echo $this->Html->link('<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text"> Opportunities </span>',array('controller'=>'contacts','action'=>'leads','opportunities'),array('escape'=>false));?>
							<b class="arrow fa fa-angle-down"></b>
					</li>

 -->	

 					<li class="<?php echo $this->params['controller'] == 'contacts' ? 'active':null?> hover">
						<?php echo $this->Html->link('<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Contacts </span>',array('plugin'=>false,'controller'=>'contacts','action'=>'index'),array('escape'=>false));?>

						<b class="arrow"></b>
					</li>

				

					 <li class="<?php echo $this->params['controller'] == 'activities' ? 'active':null?> hover">
						<?php echo $this->Html->link('<i class="menu-icon fa fa-clock-o"></i>
							<span class="menu-text"> Activities </span>',array('plugin'=>false,'controller'=>'activities','action'=>'index'),array('escape'=>false));?>

						<b class="arrow"></b>
					</li>


					<li class="<?php echo ($this->here == '/my/crm/pages/dashboard') ? 'active':null;?> hover">
						<?php echo $this->Html->link('<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Sales Performance </span>',array('plugin'=>'acl_management','controller'=>'users','action'=>'dashboard'),array('escape'=>false));?>
						<b class="arrow"></b>
					</li>

				</ul>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>
