<div id="sidebar" class="sidebar navbar-collapse collapse  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

			
	<ul class="nav nav-list">
		<?php if($userDetails['group_id'] == 1):?>
		<li class="">
		 <a href="#" class="dropdown-toggle">
			 <i class="menu-icon fa fa-desktop"></i> 
				<span class="menu-text"><?php echo __('General');?> </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<ul class="submenu">
			<!-- 	<li class="">
				<?php echo $this->Html->link('Config',array('plugin'=>false,'controller'=>'systemSettings','action'=>'add'));?>
				</li> -->

				<li class="">
				<?php echo $this->Html->link('Pipeline',array('plugin'=>false,'controller'=>'systemSettings','action'=>'pipelines'));?>
				</li>
				<li><?php echo $this->Html->link('<i class="menu-icon fa fa-bar-chart"></i> Sale Performances',array('plugin'=>false,'controller'=>'sale_performances','action'=>'index'),array('escape'=>false));?></li>
			</ul>	
		</li>
		<?php endif;?>

		<li class="">
			 <a href="#" class="dropdown-toggle">
			 	<i class="menu-icon fa fa-google-plus-square"></i>
				<span class="menu-text"><?php echo __('Google Connection');?> </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>


			<ul class="submenu">
			<li class="">
				<?php echo $this->Html->link('Set Connections',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleConnect'));?>
				</li>
			<li class="">
			<?php echo $this->Html->link('Contacts',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleContact'));?>
			</li>
			<li class="">
				<?php echo $this->Html->link('Calendar',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleCalendar'));?>
				</li>
			<li class="">
			<?php echo $this->Html->link('Drive',array('plugin'=>false,'controller'=>'system_settings','action'=>'googleDrive'));?>
			</li>
			</ul>
			
		</li>

		<!-- <li class="">
			 <a href="#" class="dropdown-toggle">
			<i class="menu-icon fa fa-facebook-official"></i>
				<span class="menu-text"><?php echo __('Facebook Connection');?> </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>


	
			
		</li>
 -->

		<li class="">
			 <a href="#" class="dropdown-toggle">
			 	<i class="menu-icon fa fa-users"></i>
				<span class="menu-text"><?php echo __('User Management');?> </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>


			<ul class="submenu">
				<li class="">
				
				<?php echo $this->Html->link('Edit My Profile',array('plugin'=>'acl_management','controller'=>'users','action'=>'edit',$userDetails['id']));?>
				</li>

			<?php if($userDetails['group_id'] == 1):?>
				<li class="">
				<?php echo $this->Html->link('List',array('plugin'=>'acl_management','controller'=>'users','action'=>'index'));?>
				</li>
				<li class="">
				<?php echo $this->Html->link('Create New User',array('plugin'=>'acl_management','controller'=>'users','action'=>'add'));?>
				</li>
				<li class="">
				<?php echo $this->Html->link('Create New User Group',array('plugin'=>false,'controller'=>'user_groups','action'=>'add'));?>
				</li>
				<li class="">
				<?php echo $this->Html->link('Create New ACL Group',array('plugin' => 'acl_management', 'controller' => 'groups', 'action' => 'add'));?>
				</li>
				<li class="">
				<?php echo $this->Html->link('ACL Setting',array('plugin' => 'acl_management', 'controller' => 'user_permissions', 'action' => 'index'));?>
				</li>
				<?php endif;?>
				</ul>
			
		</li>

		
	</ul><!-- /.nav-list -->

	<!-- #section:basics/sidebar.layout.minimize -->
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>

	<!-- /section:basics/sidebar.layout.minimize -->
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>