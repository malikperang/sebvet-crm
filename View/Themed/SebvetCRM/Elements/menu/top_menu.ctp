<div id="navbar" class="navbar navbar-default top-nav">
      <script type="text/javascript">
        // try{ace.settings.check('navbar' , 'fixed')}catch(e){}
      </script>

      <div class="navbar-container" id="navbar-container">
      
        <button type="button" class="navbar-toggle menu-toggler pull-left" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu,.nav nav-list">
          <span class="sr-only">Toggle sidebar</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
      
          <a href="#" class="navbar-brand">
              <?php if(!empty($sysSetting['SystemSetting']['name'])):?>
                <small>
              <?php echo __($sysSetting['SystemSetting']['name']);?>
               </small>

              <?php else:
                  echo $this->Html->image('new-logo.jpg',array('class'=>'img-responsive logo-top'));?> S.R.S
              <?php endif;?>          
          </a>
        </div>

    
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">
            <li class="transparent">
             <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="ace-icon fa fa-bell"></i>

                 <?php if($newMessage !== 0):?>
                   <span class="badge badge-primary">
                      <?php echo $newMessage;?>
                   </span>
                 <?php endif;?>
              </a>

              <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                <li>
                    <?php echo $this->Html->link('Messages <span class="badge badge-blue">'.$newMessage.'</span>',array('plugin'=>false,'controller'=>'messages','action'=>'index'),array('escape'=>false));?>
                </li>
 

                <li>
                    <?php echo $this->Html->link('Announcements',array('plugin'=>false,'controller'=>'announcements','action'=>'index'),array('escape'=>false));?>
                </li>

                <li>
                 <?php echo $this->Html->link('<img src="http://cdn-media-1.lifehack.org/wp-content/files/2011/11/gmail-logo-icon.png" class="img-responsive" style="height:20px;"> Gmail Inbox',array('plugin'=>'google_app','controller'=>'gmail','action'=>'index'),array('escape'=>false));?>
                </li>

                
              </ul>
            </li>


            <!-- #section:basics/navbar.user_menu -->
            <li class="transparent">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
              <?php if(empty($userDetails['profile_picture'])):?>
                <?php echo $this->Html->image('default/no-profile-img.gif',array('fullBase'=>true,'class'=>'nav-user-photo'));?>
                  <?php else:?>
                <?php echo $this->Html->image('uploads/'.$userDetails['profile_picture'],array('fullBase'=>true,'class'=>'nav-user-photo'));?>
                <?php endif;?>
                 <span class="user-info">
                  <small>Welcome,</small>
                  <?php echo $userDetails['name'];?>
                </span>

                <i class="ace-icon fa fa-caret-down"></i>
              </a>

              <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
               
              <?php if($userDetails['group_id'] == 1):?>
                <li>
                    <?php echo $this->Html->link(' <i class="purple ace-icon fa fa-cog bigger-125"></i> Settings',array('plugin'=>false,'controller'=>'systemSettings','action'=>'pipelines'),array('escape'=>false));?>
                </li>

              <?php else:?>         
                <li>
                    <?php echo $this->Html->link(' <i class="purple ace-icon fa fa-cog bigger-125"></i> Settings',array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$userDetails['id']),array('escape'=>false));?>
                </li>
              <?php endif;?>

                <li>
                  <?php echo $this->Html->link(' <i class="ace-icon fa fa-user"></i> Profile',array('plugin'=>'acl_management','controller'=>'users','action'=>'view',$userDetails['id']),array('escape'=>false));?>
                </li>

                <li class="divider"></li>

                <li>
                   <?php echo $this->Html->link('  <i class="ace-icon fa fa-power-off"></i> Logout',array('plugin'=>'acl_management','controller'=>'users','action'=>'logout'),array('escape'=>false));?>
                </li>
              </ul>
            </li>

      
          </ul>
        </div>

         <nav role="navigation" class="navbar-menu pull-left collapse navbar-collapse top-menu-nav">
      
          <ul class="nav navbar-nav">

          <li>
            <?php echo $this->Html->link('<i class="fa fa-bar-chart"></i>
              <span class="menu-text"> Company Sales Performance </span>',array('plugin'=>'acl_management','controller'=>'users','action'=>'companyPerformance'),array('escape'=>false));?>
             </li>

          <li>
            <?php if($userDetails['group_id'] == 1):?>
               <?php echo $this->Html->link('<i class="fa fa-user"></i>
              <span class="menu-text"> Personal Sales Performance </span>',array('plugin'=>'acl_management','controller'=>'users','action'=>'personalPerformance'),array('escape'=>false));?>
            <?php else:?>
              <?php echo $this->Html->link('<i class="fa fa-user"></i>
              <span class="menu-text"> Personal Sales Performance </span>',array('plugin'=>'acl_management','controller'=>'users','action'=>'personalPerformance',$userDetails['id']),array('escape'=>false));?>
            <?php endif;?>
          </li>

          <li>
            <?php echo $this->Html->link('<i class="menu-icon fa fa-users"></i>
              <span class="menu-text"> Contacts </span>',array('plugin'=>false,'controller'=>'contacts','action'=>'index'),array('escape'=>false));?>

            <b class="arrow"></b>
           </li>


<!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Overview
        &nbsp;
                <i class="ace-icon fa fa-angle-down bigger-110"></i>
              </a> -->
          <li >
            <a href="<?php echo $this->webroot;?>deals/" class="dropdown-toggle" data-toggle="dropdown">
              <i class="ace-icon fa fa-money"></i>
              <span class="menu-text"> Pipeline </span>
               <!--  <i class="ace-icon fa fa-angle-down bigger-110"></i> -->
              </a>
          

              <ul class="dropdown-menu dropdown-light-blue dropdown-caret">
                 <li>
                  <a href="<?php echo $this->webroot;?>deals/">
                    All Deals
                  </a>
                </li>
                <li>
                  <a href="<?php echo $this->webroot;?>deals/viewWon">
                <!--     <i class="ace-icon fa fa-eye bigger-110 blue"></i>
                    -->
                    Won Deals
                  </a>
                </li>
                <li>
                  <a href="<?php echo $this->webroot;?>deals/viewWon">
                <!--     <i class="ace-icon fa fa-eye bigger-110 blue"></i>
                    -->
                   Lost Deals
                  </a>
                </li>

              </ul>
          </li>

        

           <li>
            <?php echo $this->Html->link('<i class="menu-icon fa fa-calendar"></i>
              <span class="menu-text"> Daily Activities </span>',array('plugin'=>false,'controller'=>'activities','action'=>'index'),array('escape'=>false));?>

            <b class="arrow"></b>
          </li>

<!-- 
          <li>
            <?php echo $this->Html->link('<i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Sales Performance </span>',array('plugin'=>'acl_management','controller'=>'users','action'=>'dashboard'),array('escape'=>false));?>
            <b class="arrow"></b>
          </li> -->

        
          </ul>

        </nav>

    
      </div>
    </div>