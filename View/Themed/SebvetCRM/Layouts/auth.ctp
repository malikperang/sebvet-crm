<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'SRS');
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
	<title>
		<?php echo $cakeDescription ?>:
		<?php //echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta(array("name"=>"viewport","content"=>"width=device-width,  initial-scale=1.0"));
		echo $this->Html->meta('icon');

		//bootstrap & font awesome css
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('font-awesome.min');
		
		//ace fonts 
		echo $this->Html->css('ace-fonts');


		//ace styles
		echo $this->Html->css('uncompressed/ace');

		//bootstrap-social
		echo $this->Html->css('bootstrap-social/bootstrap-social');

		echo $this->Html->css('login');
		
	?>
	<?php
		echo $this->Html->css('ace-skins.min');
		echo $this->Html->css('ace-rtl.min');
		echo $this->fetch('meta');
		echo $this->fetch('css');

	?>

</head>
<body class="login-layout">
	<div class="main-container">
		<div class="main-content">
			<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
			<div class="login-container">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<footer>

	</footer>

	<?php //echo $this->element('sql_dump'); ?>
	<?php
		//bootstrap js
		echo $this->Html->script('jquery.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->fetch('script');
	?>
</body>
</html>
