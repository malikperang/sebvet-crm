<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'SRS');
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta(array("name"=>"viewport","content"=>"width=device-width,  initial-scale=1.0"));
		echo $this->Html->meta('icon');

		//bootstrap & font awesome css
		echo $this->Html->css('uncompressed/bootstrap',array('id'=>'bootstrap-css'));
		echo $this->Html->css('http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		
		// //datatables
		// echo $this->Html->css('http://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css');
		// echo $this->Html->css('http://cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css');

		//bootstrap-social
		echo $this->Html->css('bootstrap-social/bootstrap-social');
		
		//ace fonts 
		// echo $this->Html->css('ace-fonts');
		echo $this->Html->css('chosen');
		//ace styles
		echo $this->Html->css('uncompressed/ace');

		//sweet alert
		echo $this->Html->css('sweetalert');
		echo $this->Html->css('custom');
		
	?>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
	<?php
		echo $this->Html->css('uncompressed/ace-skins');
		echo $this->Html->css('ace-rtl.min');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->Html->script('jquery.min');
	
		echo $this->Html->script('jquery.fittext');
	?>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<style type="text/css">
	.theme-dropdown .dropdown-menu {
  position: static;
  display: block;
  margin-bottom: 20px;
}

.theme-showcase > p > .btn {
  /*margin: 5px 0;
  position:absolute;
  z-index: 1;*/
}

.theme-showcase .navbar .container {
  width: auto;
}

</style>
</head>
<body class="skin-3 no-skin">
<div class="se-pre-con"></div>
	<?php echo $this->element('menu/top_menu'); ?>

		<div class="container theme-showcase" role="main" id="container">
		<div class="page-content">
		<?php //echo $this->element('menu/navbar'); ?>
		<!-- <div class="main-content main-content-default"> -->
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<?php
	
		//bootstrap js
		echo $this->Html->script('jquery.cookie');
		echo $this->Html->script('moment');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('jquery.dataTables.min');
		echo $this->Html->script('sweetalert/sweetalert.min');

		// echo $this->Html->script('http://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js');
		// echo $this->Html->script('dataTables.tableTools');
		// echo $this->Html->script('http://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js');	
		echo $this->Html->script('chosen.jquery');
		// echo $this->Html->script('dt-config');
			//ace setting handler
		echo $this->Html->script('ace-extra.min');
		echo $this->Html->script('ace-elements.min');
		echo $this->Html->script('ace.min');
		echo $this->fetch('script');
	?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
		<script type="text/javascript">
			$(window).load(function() {
				$(".se-pre-con").fadeOut("slow");;
			});
			jQuery(document).ready(function($) {
				   var currentDate = moment().format('YYYY MM DD'); 
			       var currentMonth = moment().format('MMMM'); 
			        $('#current-month').text(currentMonth);	
			});


			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				if( !fixed || ( ace.helper.mobile_view() || ace.helper.collapsible() ) ) {
					$sidebar.removeClass('hide-before');
					//restore original, default marginTop
					ace.helper.removeStyle(sidebar , 'margin-top')
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('hide-before');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('hide-before');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
					$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
		        var currentDate = moment().format('YYYY MM DD'); 
		        var currentMonth = moment().format('MMMM'); 
		        $('#current-month').text(currentMonth);
			
			});
		</script>
</div>
<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
