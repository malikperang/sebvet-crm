jQuery(document).ready(function($) {
	$('#lead-index').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Lead Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Lead Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Lead Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": false },           
               ] ,   
	 });
	// $('#deal-index').DataTable({
	// 	 responsive: true,
	// 	"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
 //       //   tableTools: {   
 //       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	//       //   "aButtons": [
	//       //   	"copy",
	//       //       "print", {
	//       //           "sExtends": "xls",
	//       //           "sTitle": "Deal Report"
	//       //       }, {
	//       //           "sExtends": "pdf",
	//       //           "sTitle": "Deal Report"
	//       //       },{
	//       //       	"sExtends": "csv",
	//       //           "sTitle": "Deal Report"
	//       //       }]
	//       // },
	//     "aoColumns": [
	//     		{ "bSortable": false },
	//     		{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },      
 //               ] ,   
	//  });

	$('#deal-all').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });
	// $('#deal-kuote').DataTable({
	// 	 responsive: true,
	// 	"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
 //       //   tableTools: {   
 //       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	//       //   "aButtons": [
	//       //   	"copy",
	//       //       "print", {
	//       //           "sExtends": "xls",
	//       //           "sTitle": "Deal Report"
	//       //       }, {
	//       //           "sExtends": "pdf",
	//       //           "sTitle": "Deal Report"
	//       //       },{
	//       //       	"sExtends": "csv",
	//       //           "sTitle": "Deal Report"
	//       //       }]
	//       // },
	//     "aoColumns": [
	//     		{ "bSortable": false },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": true },
 //              	{ "bSortable": false },           
 //               ] ,   
	//  });
	$('#deal-quotes').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });
	$('#deal-incoming').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });
	$('#deal-contact').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    	
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });

	$('#salescycle').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });

	$('#deal-won').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });
	$('#deal-lost').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Deal Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Deal Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Deal Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
	    		
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },      
               ] ,   
	 });

	
	$('#contact-index').DataTable({
		 responsive: true,
		"sDom": '<"top"f>rt<"clear">lrtip<"bottom">',
       //   tableTools: {   
       //   	"sSwfPath":"theme/AceTheme/swf/copy_csv_xls_pdf.swf",
	      //   "aButtons": [
	      //   	"copy",
	      //       "print", {
	      //           "sExtends": "xls",
	      //           "sTitle": "Contact Report"
	      //       }, {
	      //           "sExtends": "pdf",
	      //           "sTitle": "Contact Report"
	      //       },{
	      //       	"sExtends": "csv",
	      //           "sTitle": "Contact Report"
	      //       }]
	      // },
	    "aoColumns": [
	    		{ "bSortable": false },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": true },
              	{ "bSortable": false },           
               ] ,   
	 });
	
});