/**
 Required. sebvet's Basic File to Initiliaze Different Parts & Some Variables.
*/
if( !('sebvet' in window) ) window['sebvet'] = {}
if( !('helper' in window['sebvet']) ) window['sebvet'].helper = {}
if( !('vars' in window['sebvet']) ) {
  window['sebvet'].vars = {
	 'icon'	: ' sebvet-icon ',
	'.icon'	: '.sebvet-icon'
  }
}
sebvet.vars['touch']	= 'ontouchstart' in document.documentElement;


jQuery(function($) {
	//sometimes we try to use 'tap' event instead of 'click' if jquery mobile plugin is available
	sebvet.click_event = sebvet.vars['touch'] && $.fn.tap ? 'tap' : 'click';

	//sometimes the only good way to work around browser's pecularities is to detect them using user-agents
	//though it's not accurate
	var agent = navigator.userAgent
	sebvet.vars['webkit'] = !!agent.match(/AppleWebKit/i)
	sebvet.vars['safari'] = !!agent.match(/Safari/i) && !agent.match(/Chrome/i);
	sebvet.vars['android'] = sebvet.vars['safari'] && !!agent.match(/Android/i)
	sebvet.vars['ios_safari'] = !!agent.match(/OS ([4-8])(_\d)+ like Mac OS X/i) && !agent.match(/CriOS/i)
	
	sebvet.vars['non_auto_fixed'] = sebvet.vars['android'] || sebvet.vars['ios_safari'];
	// for android and ios we don't use "top:auto" when breadcrumbs is fixed
	if(sebvet.vars['non_auto_fixed']) {
		$('body').addClass('mob-safari');
	}

	sebvet.vars['transition'] = 'transition' in document.body.style || 'WebkitTransition' in document.body.style || 'MozTransition' in document.body.style || 'OTransition' in document.body.style

	/////////////////////////////

	//a list of available functions with their arguments
	// >>> null means enable
	// >>> false means disable
	// >>> array means function arguments
	var available_functions = {
		'general_vars' : null,//general_vars should come first

		'handle_side_menu' : null,
		'add_touch_drag' : null,
				
		'sidebar_scrollable' : [
							 //true, //enable only if sidebar is fixed , for 2nd approach only
							 true //scroll to selected item? (one time only on page load)
							,true //true = include shortcut buttons in the scrollbars
							,false || sebvet.vars['safari'] || sebvet.vars['ios_safari'] //true = include toggle button in the scrollbars
							,200 //> 0 means smooth_scroll, time in ms, used in first approach only, better to be almost the same amount as submenu transition time
							,false//true && sebvet.vars['touch'] //used in first approach only, true means the scrollbars should be outside of the sidebar
							],

		'sidebar_hoverable' : null,//automatically move up a submenu, if some part of it goes out of window
		
		'general_things' : null,
		
		'widget_boxes' : null,
		'widget_reload_handler' : null,
								   
		'settings_box' : null,//settings box
		'settings_rtl' : null,
		'settings_skin' : null,
		
		'enable_searchbox_autocomplete' : null,
		
		'auto_hide_sidebar' : null,//disable?
		'auto_padding' : null,//disable
		'auto_container' : null//disable
	};
	
	//enable these functions with related params
	for(var func_name in available_functions) {
		if(!(func_name in sebvet)) continue;

		var args = available_functions[func_name];
		if(args === false) continue;//don't run this function

		else if(args == null) args = [jQuery];
		  else if(args instanceof String) args = [jQuery, args];
		    else if(args instanceof Array) args.unshift(jQuery);//prepend jQuery

		sebvet[func_name].apply(null, args);
	}

})



sebvet.general_vars = function($) {
	var minimized_menu_class  = 'menu-min';
	var responsive_min_class  = 'responsive-min';
	var horizontal_menu_class = 'h-sidebar';
	
	var sidebar = $('#sidebar').eq(0);
	//differnet mobile menu styles
	sebvet.vars['mobile_style'] = 1;//default responsive mode with toggle button inside navbar
	if(sidebar.hasClass('responsive') && !$('#menu-toggler').hasClass('navbar-toggle')) sebvet.vars['mobile_style'] = 2;//toggle button behind sidebar
	 else if(sidebar.hasClass(responsive_min_class)) sebvet.vars['mobile_style'] = 3;//minimized menu
	  else if(sidebar.hasClass('navbar-collapse')) sebvet.vars['mobile_style'] = 4;//collapsible (bootstrap style)

	//update some basic variables
	$(window).on('resize.sebvet.vars' , function(){
		sebvet.vars['window'] = {width: parseInt($(this).width()), height: parseInt($(this).height())}
		sebvet.vars['mobile_view'] = sebvet.vars['mobile_style'] < 4 && sebvet.helper.mobile_view();
		sebvet.vars['collapsible'] = !sebvet.vars['mobile_view'] && sebvet.helper.collapsible();
		sebvet.vars['nav_collapse'] = (sebvet.vars['collapsible'] || sebvet.vars['mobile_view']) && $('#navbar').hasClass('navbar-collapse');

		var sidebar = $(document.getElementById('sidebar'));
		sebvet.vars['minimized'] = 
		(!sebvet.vars['collapsible'] && sidebar.hasClass(minimized_menu_class))
		 ||
		(sebvet.vars['mobile_style'] == 3 && sebvet.vars['mobile_view'] && sidebar.hasClass(responsive_min_class))

		sebvet.vars['horizontal'] = !(sebvet.vars['mobile_view'] || sebvet.vars['collapsible']) && sidebar.hasClass(horizontal_menu_class)
	}).triggerHandler('resize.sebvet.vars');
}

//
sebvet.general_things = function($) {
	//add scrollbars for user dropdowns
	var has_scroll = !!$.fn.sebvet_scroll;
	if(has_scroll) $('.dropdown-content').sebvet_scroll({reset: false, mouseWheelLock: true})
	/**
	//add scrollbars to body
	 if(has_scroll) $('body').sebvet_scroll({size: sebvet.helper.winHeight()})
	 $('body').css('position', 'static')
	*/

	//reset scrolls bars on window resize
	$(window).on('resize.reset_scroll', function() {
		/**
		 //reset body scrollbars
		 if(has_scroll) $('body').sebvet_scroll('update', {size : sebvet.helper.winHeight()})
		*/
		if(has_scroll) $('.sebvet-scroll').sebvet_scroll('reset');
	});
	$(document).on('settings.sebvet.reset_scroll', function(e, name) {
		if(name == 'sidebar_collapsed' && has_scroll) $('.sebvet-scroll').sebvet_scroll('reset');
	});


	//change a dropdown to "dropup" depending on its position
	$(document).on('click.dropdown.pos', '.dropdown-toggle[data-position="auto"]', function() {
		var offset = $(this).offset();
		var parent = $(this.parentNode);

		if ( parseInt(offset.top + $(this).height()) + 50 
				>
			(sebvet.helper.scrollTop() + sebvet.helper.winHeight() - parent.find('.dropdown-menu').eq(0).height()) 
			) parent.addClass('dropup');
		else parent.removeClass('dropup');
	});
	
	
	//prevent dropdowns from hiding when a tab is selected
	$(document).on('click', '.dropdown-navbar .nav-tabs', function(e){
		e.stopPropagation();
		var $this , href
		var that = e.target
		if( ($this = $(e.target).closest('[data-toggle=tab]')) && $this.length > 0) {
			$this.tab('show');
			e.preventDefault();
		}
	});
	
	//prevent dropdowns from hiding when a from is clicked
	/**$(document).on('click', '.dropdown-navbar form', function(e){
		e.stopPropagation();		
	});*/


	//disable navbar icon animation upon click
	$('.sebvet-nav [class*="icon-animated-"]').closest('a').one('click', function(){
		var icon = $(this).find('[class*="icon-animated-"]').eq(0);
		var $match = icon.attr('class').match(/icon\-animated\-([\d\w]+)/);
		icon.removeClass($match[0]);
	});


	//tooltip in sidebar items
	$('.sidebar .nav-list .badge[title],.sidebar .nav-list .badge[title]').each(function() {
		var tooltip_class = $(this).attr('class').match(/tooltip\-(?:\w+)/);
		tooltip_class = tooltip_class ? tooltip_class[0] : 'tooltip-error';
		$(this).tooltip({
			'plsebvetment': function (context, source) {
				var offset = $(source).offset();

				if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
				return 'left';
			},
			container: 'body',
			template: '<div class="tooltip '+tooltip_class+'"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
		});
	});
	
	//or something like this if items are dynamically inserted
	/**$('.sidebar').tooltip({
		'plsebvetment': function (context, source) {
			var offset = $(source).offset();

			if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
			return 'left';
		},
		selector: '.nav-list .badge[title],.nav-list .label[title]',
		container: 'body',
		template: '<div class="tooltip tooltip-error"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	});*/
	
	


	//the scroll to top button
	var scroll_btn = $('.btn-scroll-up');
	if(scroll_btn.length > 0) {
		var is_visible = false;
		$(window).on('scroll.scroll_btn', function() {
			if(sebvet.helper.scrollTop() > parseInt(sebvet.helper.winHeight() / 4)) {
				if(!is_visible) {
					scroll_btn.addClass('display');
					is_visible = true;
				}
			} else {
				if(is_visible) {
					scroll_btn.removeClass('display');
					is_visible = false;
				}
			}
		}).triggerHandler('scroll.scroll_btn');

		scroll_btn.on(sebvet.click_event, function(){
			var duration = Math.min(500, Math.max(100, parseInt(sebvet.helper.scrollTop() / 3)));
			$('html,body').animate({scrollTop: 0}, duration);
			return false;
		});
	}


	//chrome and webkit have a problem here when resizing from 460px to more
	//we should force them redraw the navbar!
	if( sebvet.vars['webkit'] ) {
		var sebvet_nav = $('.sebvet-nav').get(0);
		if( sebvet_nav ) $(window).on('resize.webkit' , function(){
			sebvet.helper.redraw(sebvet_nav);
		});
	}

}




//some functions
sebvet.helper.collapsible = function() {
	var toggle
	return (document.querySelector('#sidebar.navbar-collapse') != null)
	&& ((toggle = document.querySelector('.navbar-toggle[data-target*=".sidebar"]')) != null)
	&&  toggle.scrollHeight > 0
	//sidebar is collapsible and collapse button is visible?
}
sebvet.helper.mobile_view = function() {
	var toggle
	return ((toggle = document.getElementById('menu-toggler')) != null	&& toggle.scrollHeight > 0)
}

sebvet.helper.redraw = function(elem) {
	var saved_val = elem.style['display'];
	elem.style.display = 'none';
	elem.offsetHeight;
	elem.style.display = saved_val;
}

sebvet.helper.scrollTop = function() {
	return document.scrollTop || document.documentElement.scrollTop || document.body.scrollTop
	//return $(window).scrollTop();
}
sebvet.helper.winHeight = function() {
	return window.innerHeight || document.documentElement.clientHeight;
	//return $(window).innerHeight();
}
sebvet.helper.camelCase = function(str) {
	return str.replsebvet(/-([\da-z])/gi, function(match, chr) {
	  return chr ? chr.toUpperCase() : '';
	});
}
sebvet.helper.removeStyle = 
  'removeProperty' in document.body.style
  ?
  function(elem, prop) { elem.style.removeProperty(prop) }
  :
  function(elem, prop) { elem.style[sebvet.helper.camelCase(prop)] = '' }


sebvet.helper.hasClass = 
  'classList' in document.documentElement
  ?
  function(elem, className) { return elem.classList.contains(className); }
  :
  function(elem, className) { return elem.className.indexOf(className) > -1; }