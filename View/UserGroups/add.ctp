<div class="page-header">
            <h1>
              <?php echo __('User Group');?>
            </h1>
	<p>User group is a group to define type of team in a company.E.g: Manager,Sales Advisor,Marketing .</p>
        </div>
<div class="row setting-row">
<div class="col-xs-12 col-sm-12 col-lg-3">
		<div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
           	Create new group
            </h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
                     <?php echo $this->Form->create('UserGroup'); ?>
	<fieldset>
		<div class="form-group">
		<label for="name"> User Group Name</label>
	<?php echo $this->Form->input('name',array('class'=>'form-control','label'=>false));?>
	</fieldset>
	<?php echo $this->Form->button('Create Group',array('type'=>'submit','class'=>'btn btn-default center-block'));?>
			&nbsp; &nbsp; &nbsp;
<?php echo $this->Form->end(); ?>
            </div>
          </div>
        </div>

 </div>
<div class="col-lg-8">
<div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
            Available User Group
            </h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
                 <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
					<thead>
					<tr>
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('created','Created at'); ?></th>
							<th><?php echo $this->Paginator->sort('modified','Updated at'); ?></th>
							<th><?php echo $this->Paginator->sort('created_by','Created by'); ?></th>
							<th><?php echo $this->Paginator->sort('modified_by','Updated by'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($userGroups as $userGroup): ?>
					<tr>
						<td><?php echo h($userGroup['UserGroup']['name']); ?>&nbsp;</td>
						<td><?php echo h($userGroup['UserGroup']['created']); ?>&nbsp;</td>
						<td><?php echo h($userGroup['UserGroup']['modified']); ?>&nbsp;</td>
						<td><?php echo h($userGroup['Created_by']['name']); ?>&nbsp;</td>
						<td><?php echo h($userGroup['Modified_by']['name']); ?>&nbsp;</td>
						<td class="actions">
						<div class="btn-group">
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userGroup['UserGroup']['id']),array('class'=>'btn btn-default','escape'=>false)); ?>
							<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userGroup['UserGroup']['id']), array('class'=>'btn btn-default','escape'=>false), __('Are you sure you want to delete # %s?', $userGroup['UserGroup']['id'])); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
					</table>

					<p class="text-center">
					     <?php
					     echo $this->Paginator->counter(array(
					     'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					     ));
					     ?>     </p>
					     <div class="text-center">
					     <ul class="pagination pagination pagination-right">
					     <li><?php
					          echo $this->Paginator->prev('<<',array(), null, array('class' => 'prev disabled'));
					     ?>
					     </li><li><?php
					          echo $this->Paginator->numbers(array('separator' => ''));
					     ?>
					     </li><li><?php
					          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					     ?>
					     </li>    
					     </ul>
					     </div>
              
            </div>
          </div>
        </div>
</div>

</div>