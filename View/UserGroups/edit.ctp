<div class="page-header">
            <h1>
              <?php echo __('User Group');?>
              <?php echo $this->Html->link(__('<i class="fa fa-plus-circle"></i>Create new group'), array('action' => 'add'),array('class'=>'btn btn-default','escape'=>false)); ?>
            </h1>
			<p>User group is a group to define type of team in a company.E.g: Manager,Sales Advisor,Marketing .</p>
        </div>
<div class="row setting-row">
<div class="col-xs-12 col-sm-12 col-lg-3">
		<div class="widget-box">
          <div class="widget-header">
            <h4 class="widget-title lighter smaller">
           	Update User Group 
            </h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
              <?php echo $this->Form->create('UserGroup',array('action'=>'edit')); ?>
	<fieldset>
		<div class="form-group">
		<label for="name"> User Group Name</label>
	<?php echo $this->Form->input('name',array('class'=>'form-control','label'=>false));?>
	</fieldset>
	<?php echo $this->Form->button('Update',array('type'=>'submit','class'=>'btn btn-default center-block'));?>
			&nbsp; &nbsp; &nbsp;
<?php echo $this->Form->end(); ?>
            </div>
          </div>
        </div>

 </div>

</div>

</div>